install:
	docker exec -it frontend-repo bash -c "yarn install"

bootstrap:
	docker exec -it frontend-repo bash -c "yarn bootstrap"

storybook.build:
	docker exec -it frontend-repo bash -c "cd packages/core-web && yarn build"

app.web.build:
	docker exec -it frontend-repo bash -c "cd packages/app-web && yarn build"

init: install bootstrap storybook.build