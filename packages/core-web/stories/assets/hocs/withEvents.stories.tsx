import withEvents from '@betpad/helpers/hocs/withEvents'
import { IWithEvents } from '@betpad/types/hocs/IWithEvents'
import { action } from '@storybook/addon-actions'
import * as React from 'react'
import createHOCTitle from '../../helpers/createHOCTitle'
import createStories from '../../helpers/createStories'

const WithEventsStoryWrapper = (props: IWithEvents) => {

    const Wrapped = withEvents(props)(
        () => <div>
            Here's <b>click</b> and <b>mouseout</b> event example only for document.<br/><br/>
            See sources to extend.<br/><br/>
            You can click anywhere and see result in actions-addon panel.
        </div>,
    ) as any

    return <Wrapped/>
}

const WithEventsStoryRender = () => (
    <WithEventsStoryWrapper
        target={document}
        targetKey={'document'}
        events={['click', 'mouseout']}
        handlers={() => ({
            onDocumentClick: action('onDocumentClick') ,
            onDocumentMouseout: action('onDocumentMouseout'),
        })}
    />
)

createStories({
    title: createHOCTitle('withEvents'),
    module,
    stories: [
        {
            name: 'default',
            render: WithEventsStoryRender,
        },
    ],
})