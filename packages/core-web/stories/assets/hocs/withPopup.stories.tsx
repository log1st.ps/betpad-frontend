import withForwardedRef from '@betpad/helpers/hocs/withForwardedRef'
import withPopup from '@betpad/helpers/hocs/withPopup'
import {
    IWithPopupProps,
    POPUP_ALIGN_AUTO,
    POPUP_ALIGN_END,
    POPUP_ALIGN_START,
    POPUP_POSITION_BOTTOM,
    POPUP_POSITION_LEFT,
    POPUP_POSITION_RIGHT,
    POPUP_POSITION_TOP,
} from '@betpad/types/hocs/IWithPopup'
import { action } from '@storybook/addon-actions'
import {
    boolean,
    number,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import createHOCTitle from '../../helpers/createHOCTitle'
import createStories from '../../helpers/createStories'

const groupID = 'withPopup'

const WithPopupWrappedComponent = withForwardedRef(({forwardedRef}: any) => (
    <div
        ref={forwardedRef}
        style={{
            width: 300,
            height: 300,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
            background: 'blue',
            color: 'white',
        }}
    >
        {text('custom: wrapped component text', 'Some example target', groupID)}
    </div>
))

const WithPopupPopupComponent = ({zIndex}: {zIndex: number}) => (
    <div style={{zIndex}}>
        Popup Component with index {zIndex}<br/>
        {text('custom: renderContent text', 'Custom content', groupID)}
    </div>
)

const WithPopupStoryRender = (props: IWithPopupProps) => withPopup({
    ...props,
})(WithPopupWrappedComponent)()

createStories({
    module,
    title: createHOCTitle('withPopup'),
    stories: [
        {
            name: 'default',
            render: () => (
                <WithPopupStoryRender
                    onCreate={action('onCreate')}
                    onUpdate={action('onUpdate')}
                    renderContent={WithPopupPopupComponent}
                    zStartIndex={
                        number(
                            'zStartIndex',
                            0,
                            {min: 0, step: 1, range: false, max: 10000000000000000000},
                            groupID,
                        )
                    }
                    zPopupOffset={
                        number(
                            'zPopupOffset',
                            0,
                            {min: 0, step: 1, range: false, max: 10000000000000000000},
                            groupID,
                        )
                    }
                    offsetPrimary={
                        number(
                            'offsetPrimary',
                            0,
                            {min: 0, step: 1, range: false, max: 10000000000000000000},
                            groupID,
                        )
                    }
                    offsetSecondary={
                        number(
                            'offsetSecondary',
                            0,
                            {min: 0, step: 1, range: false, max: 10000000000000000000},
                            groupID,
                        )
                    }
                    onArrowPositionUpdate={action('onArrowPositionUpdate')}
                    isFixed={boolean('isFixed', false, groupID)}
                    position={
                        select(
                            'position', [
                                POPUP_POSITION_LEFT,
                                POPUP_POSITION_RIGHT,
                                POPUP_POSITION_TOP,
                                POPUP_POSITION_BOTTOM,
                                POPUP_ALIGN_AUTO,
                            ] as ReadonlyArray<string>,
                            String(POPUP_POSITION_RIGHT),
                            groupID,
                        ) as IWithPopupProps['position']
                    }
                    align={
                        select(
                            'align', [
                                POPUP_ALIGN_START,
                                POPUP_ALIGN_END,
                                POPUP_ALIGN_AUTO,
                            ] as ReadonlyArray<string>,
                            String(POPUP_ALIGN_AUTO),
                            groupID,
                        ) as IWithPopupProps['align']
                    }
                />
            ),
        },
    ],
})