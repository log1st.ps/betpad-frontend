import { action } from '@storybook/addon-actions'
import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import { BUTTON_SIZE_XS } from '../../../../types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '../../../../types/assets/elements/button/buttonStatesConstants'
import { PrimarySubHeading } from '../../../components/TypographyComponent'
import ButtonsList from '../../../src/assets/components/ButtonsList/ButtonsListComponent'
import Form from '../../../src/assets/components/Form/FormComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'Form'

const FormStoryRender = () => () => (
    <Form
        onSubmit={() => {
            event && event.preventDefault()
            action('onSubmit')()
        }}
    >
        <div>
            <PrimarySubHeading
                content={text('children in subHeading', 'Example children in subheading', groupID)}
            />
            <br/>
            <ButtonsList
                buttons={[
                    {
                        key: 'submit',
                        text: 'abstract example submit button',
                        state: BUTTON_STATE_PRIMARY,
                        size: BUTTON_SIZE_XS,
                    },
                ]}
            />
        </div>
    </Form>
)

createStories({
    title: createComponentTitle('Form'),
    module,
    stories: [
        {
            name: 'form',
            render: FormStoryRender(),
        },
    ],
})