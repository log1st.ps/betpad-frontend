import {
    boolean,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as cardTypes from '../../../../types/assets/components/card/cardTypesConstants'
import * as icons from '../../../../types/assets/elements/icon/iconsConstants'
import * as colors from '../../../../types/core/colors'
import Card from '../../../src/assets/components/Card/CardComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'Card'

const defaultLogo = require('./1xbetLogo.png')
const defaultActiveLogo = require('./1xbetLogoActive.png')

const CardStoryRender = () => () => (
    <Card
        type={
            select(
                'type',
                Object.values(cardTypes) as any,
                cardTypes.CARD_TYPE_ICON as any,
                groupID,
            )
        }
        value={text('value', '1000 usd.', groupID)}
        label={text('label', 'Some Label', groupID)}
        logo={
            select(
                'logo',
                {
                    'EXAMPLE: defaultLogo': defaultLogo,
                    'EXAMPLE: defaultActiveLogo': defaultActiveLogo,
                } as any,
                defaultLogo as any,
                groupID,
            )}
        logoBackground={
            select(
                'logoBackground',
                Object.values(colors) as any,
                colors.COLOR_BLUE_DEEP,
                groupID,
            )
        }
        isActive={boolean('isActive', false, groupID)}
        isDisabled={boolean('isDisabled', false, groupID)}
        icon={
            select(
                'icon',
                Object.values(icons) as any,
                icons.ICON_SPORT_VOLLEYBALL as any,
            )
        }
    />
)

createStories({
    title: createComponentTitle('Card'),
    module,
    stories: [
        {
            name: 'default',
            render: CardStoryRender(),
        },
    ],
})