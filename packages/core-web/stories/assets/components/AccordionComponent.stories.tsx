import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Accordion from '../../../components/AccordionComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'Accordion'

const AccordionStoryRender = () => () => (
    <Accordion
        label={text('label', 'Some label', groupID)}
        isActive={boolean('isActive', false, groupID)}
        onClick={action('onClick')}
    >
        {text('children', 'Some content', groupID)}
    </Accordion>
)

createStories({
    title: createComponentTitle('Accordion'),
    module,
    stories: [
        {
            name: 'default',
            render: AccordionStoryRender(),
        },
    ],
})