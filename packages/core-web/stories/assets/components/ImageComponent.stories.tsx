import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as imageBackgroundTypes from '../../../../types/assets/components/image/imageBackgroundTypesConstants'
import * as imageBounds from '../../../../types/assets/components/image/imageBoundsConstants'
import * as imageTypes from '../../../../types/assets/components/image/imageTypesConstants'
import Image from '../../../components/ImageComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const defaultImage: string = require('./defaultImage.jpg')

const groupID = 'Image'

const imageBackgroundTypesArray
    : ReadonlyArray<string> | any
    = Object.keys(imageBackgroundTypes).map(key => imageBackgroundTypes[key])

const imageBoundsArray
    : ReadonlyArray<string> | any
    = [
        ...Array(11).fill(null).map((item, i) => `${i * 30}px`),
        ...Array(11).fill(null).map((item, i) => `${i * 10}%`),
        ...Object.keys(imageBounds).map(key => imageBounds[key]),
    ]

const imageTypesArray
    : ReadonlyArray<string> | any
    = Object.keys(imageTypes).map(key => imageTypes[key])

createStories({
    title: createComponentTitle('Image'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <Image
                    url={text('url', defaultImage, groupID)}
                    alt={text('alt', 'Some alt', groupID)}
                    type={select('type', imageTypesArray, imageTypes.IMAGE_TYPE_BLOCK, groupID)}
                    backgroundType={
                        select(
                            'backgroundType',
                            imageBackgroundTypesArray,
                            imageBackgroundTypes.IMAGE_BACKGROUND_TYPE_COVER,
                            groupID,
                        )
                    }
                    height={
                        select(
                            'height',
                            imageBoundsArray,
                            imageBounds.IMAGE_BOUND_STRETCH,
                            groupID,
                        )
                    }
                    width={
                        select(
                            'width',
                            imageBoundsArray,
                            imageBounds.IMAGE_BOUND_STRETCH,
                            groupID,
                        )
                    }
                />
            ),
        },
    ],
})