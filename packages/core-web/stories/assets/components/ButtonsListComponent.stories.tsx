import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as buttonAligns from '../../../../types/assets/components/buttonsList/buttonsListAlignsConstants'
import * as buttonSizes from '../../../../types/assets/elements/button/buttonSizesConstants'
import * as buttonStates from '../../../../types/assets/elements/button/buttonStatesConstants'
import ButtonsList from '../../../components/ButtonsListComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'


const groupID = 'ButtonsList'

const ButtonsListStoryRender = () => () => {
    const buttonSize = select(
        'buttons[].size',
        Object.values(buttonSizes) as any,
        buttonSizes.BUTTON_SIZE_XS as any,
        groupID,
    )

    return (
        <ButtonsList
            align={
                select(
                    'align',
                    Object.values(buttonAligns) as any,
                    buttonAligns.BUTTONS_LIST_JUSTIFY_START as any,
                    groupID,
                )
            }
            buttons={[
                {
                    key: 'button0',
                    text: text('buttons[0].text', 'First button', groupID),
                    state: select(
                        'buttons[0].state',
                        Object.values(buttonStates) as any,
                        buttonStates.BUTTON_STATE_PRIMARY,
                        groupID,
                    ),
                    size: buttonSize,
                },
                {
                    key: 'button1',
                    text: text('buttons[1].text', 'Second button', groupID),
                    state: select(
                        'buttons[1].state',
                        Object.values(buttonStates) as any,
                        buttonStates.BUTTON_STATE_SECONDARY,
                        groupID,
                    ),
                    size: buttonSize,
                },
                {
                    key: 'button2',
                    text: text('buttons[2].text', 'Third button', groupID),
                    state: select(
                        'buttons[2].state',
                        Object.values(buttonStates) as any,
                        buttonStates.BUTTON_STATE_OUTLINE,
                        groupID,
                    ),
                    size: buttonSize,
                },
                {
                    key: 'button3',
                    text: text('buttons[3].text', 'Fourth button', groupID),
                    state: select(
                        'buttons[3].state',
                        Object.values(buttonStates) as any,
                        buttonStates.BUTTON_STATE_GRADIENT,
                        groupID,
                    ),
                    size: buttonSize,
                },
            ]}
        />
    )
}

createStories({
    title: createComponentTitle('ButtonsList'),
    module,
    stories: [
        {
            name: 'default',
            render: ButtonsListStoryRender(),
        },
    ],
})