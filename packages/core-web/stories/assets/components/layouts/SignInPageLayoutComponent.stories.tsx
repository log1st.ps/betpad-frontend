import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import SignInPageLayout from '../../../../components/layouts/SignInPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'SignInPageLayout'

createStories({
    title: createComponentTitle('layouts/SignInPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <SignInPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderForm={
                        text('renderForm', 'Form', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                    renderSubActions={
                        text('renderSubActions', 'SubActions', groupID)
                    }
                />
            ),
        },
    ],
})