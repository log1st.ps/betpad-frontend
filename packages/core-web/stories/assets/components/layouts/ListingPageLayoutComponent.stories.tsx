import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import ListingPageLayout from '../../../../components/layouts/ListingPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'ListingPageLayout'

const ListingPageLayoutStoryRender = () => () => (
    <ListingPageLayout
        renderTabs={text('renderTabs', 'Tabs', groupID)}
        renderTable={text('renderTable', 'Table', groupID)}
        renderMode={text('renderMode', 'Mode', groupID)}
        renderFilters={text('renderFilters', 'Filters', groupID)}
        renderDatepicker={text('renderDatepicker', 'Datepicker', groupID)}
        renderCategories={text('renderCategories', 'Categories', groupID)}
    />
)

createStories({
    title: createComponentTitle('layouts/ListingPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: ListingPageLayoutStoryRender(),
        },
    ],
})