import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import CCFormLayout from '../../../../components/layouts/CCFormLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'CCFormLayout'

const CCFormLayoutStoryRender = () => () => (
    <CCFormLayout
        renderCVCField={text('renderCVCField', 'cvc', groupID)}
        renderExpirationMonthField={text('renderExpirationMonthField', 'month', groupID)}
        renderExpirationYearField={text('renderExpirationYearField', 'year', groupID)}
        renderNameField={text('renderNameField', 'name', groupID)}
        renderNumberField={text('renderNumberField', 'number', groupID)}
    />
)

createStories({
    title: createComponentTitle('layouts/CCFormLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: CCFormLayoutStoryRender(),
        },
    ],
})