import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import BalancesSettingPageLayout
    from '../../../../src/assets/components/layouts/BalancesSettingPageLayout/BalancesSettingPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'BalancesSettingPageLayout'

createStories({
    title: createComponentTitle('layouts/BalancesSettingPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <BalancesSettingPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderValues={
                        text('renderValues', 'Values', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                    renderStep={
                        text('renderStep', 'Step', groupID)
                    }
                />
            ),
        },
    ],
})