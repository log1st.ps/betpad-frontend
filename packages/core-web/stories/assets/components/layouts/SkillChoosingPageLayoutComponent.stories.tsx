import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import SkillChoosingPageLayout
    from '../../../../src/assets/components/layouts/SkillChoosingPageLayout/SkillChoosingPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'SkillChoosingPageLayout'

createStories({
    title: createComponentTitle('layouts/SkillChoosingPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <SkillChoosingPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderValues={
                        text('renderValues', 'Values', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                    renderStep={
                        text('renderStep', 'Step', groupID)
                    }
                />
            ),
        },
    ],
})