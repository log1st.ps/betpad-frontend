import {
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import MainLayout from '../../../../components/layouts/MainLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'


const groupID = 'MainLayout'

createStories({
    title: createComponentTitle('layouts/MainLayout'),
    module,
    hasTheme: true,
    stories: [
        {
            name: 'default',
            render: () => (
                <MainLayout
                    renderHeader={
                        text('renderHeader', 'Some header', groupID)
                    }
                >
                    {text('children', 'Some children text', groupID)}
                </MainLayout>
            ),
        },
    ],
})