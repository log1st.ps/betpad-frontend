import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import TariffsPageLayout from '../../../../components/layouts/TariffsPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'TariffsPageLayout'

createStories({
    title: createComponentTitle('layouts/TariffsPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <TariffsPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderTariffs={
                        text('renderTariffs', 'Tariffs', groupID)
                    }
                    renderForm={
                        text('renderForm', 'Form', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                />
            ),
        },
    ],
})