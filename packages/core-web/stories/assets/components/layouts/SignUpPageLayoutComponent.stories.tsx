import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import SignUpPageLayout from '../../../../components/layouts/SignUpPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'SignUpPageLayout'

createStories({
    title: createComponentTitle('layouts/SignUpPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <SignUpPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderForm={
                        text('renderForm', 'Form', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                    renderSubActions={
                        text('renderSubActions', 'SubActions', groupID)
                    }
                />
            ),
        },
    ],
})