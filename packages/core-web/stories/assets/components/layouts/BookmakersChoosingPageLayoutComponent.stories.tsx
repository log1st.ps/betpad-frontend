import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'
import BookmakersChoosingPageLayout
    from '../../../../src/assets/components/layouts/BookmakersChoosingPageLayout/BookmakersChoosingPageLayoutComponent'

const groupID = 'BookmakersChoosingPageLayout'

createStories({
    title: createComponentTitle('layouts/BookmakersChoosingPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <BookmakersChoosingPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderBookmakers={
                        text('renderBookmakers', 'Categories', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                    renderStep={
                        text('renderStep', 'Step', groupID)
                    }
                />
            ),
        },
    ],
})