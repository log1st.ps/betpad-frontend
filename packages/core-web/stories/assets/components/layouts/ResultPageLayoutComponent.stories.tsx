import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import ResultPageLayout from '../../../../components/layouts/ResultPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'ResultPageLayout'

createStories({
    title: createComponentTitle('layouts/ResultPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <ResultPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                />
            ),
        },
    ],
})