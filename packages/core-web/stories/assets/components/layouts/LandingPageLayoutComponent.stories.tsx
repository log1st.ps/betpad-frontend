import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import LandingPageLayout from '../../../../src/assets/components/layouts/LandingPageLayout/LandingPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'LandingPageLayout'

createStories({
    title: createComponentTitle('layouts/LandingPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <LandingPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderCategories={
                        text('renderCategories', 'Categories', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                />
            ),
        },
    ],
})