import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import CCPageLayout from '../../../../components/layouts/CCPageLayoutComponent'
import createComponentTitle from '../../../helpers/createComponentTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'CCPageLayout'

createStories({
    title: createComponentTitle('layouts/CCPageLayout'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <CCPageLayout
                    renderTitle={
                        text('renderTitle', 'Title', groupID)
                    }
                    renderSubTitle={
                        text('renderSubTitle', 'SubTitle', groupID)
                    }
                    renderForm={
                        text('renderForm', 'Form', groupID)
                    }
                    renderCTA={
                        text('renderCTA', 'CTA', groupID)
                    }
                />
            ),
        },
    ],
})