import { action } from '@storybook/addon-actions'
import {
    boolean,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as aligns from '../../../../types/assets/components/header/headerMenuAlignsConstants'
import * as buttonSizes from '../../../../types/assets/elements/button/buttonSizesConstants'
import * as buttonStates from '../../../../types/assets/elements/button/buttonStatesConstants'
import Header from '../../../components/HeaderComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'
import { defaultAvatar } from '../elements/AvatarElement.stories'


const groupID = 'Header'

const HeaderStoryRender = () => () => (
    <Header
        onLogoClick={action('onLogoClick')}
        menuAlign={
            select(
                'menuAlign',
                Object.values(aligns) as any,
                aligns.HEADER_MENU_ALIGN_RIGHT as any,
                groupID,
            )
        }
        primaryButton={
            boolean('custom: with primary button', false, groupID)
                ? {
                    state: select(
                        'primaryButton.state',
                        Object.values(buttonStates) as any,
                        buttonStates.BUTTON_STATE_OUTLINE as any,
                        groupID,
                    ),
                    size: select(
                        'primaryButton.size',
                        Object.values(buttonSizes) as any,
                        buttonSizes.BUTTON_SIZE_XS as any,
                        groupID,
                    ),
                    text: text('primaryButton.text', 'Sign In', groupID),
                    onClick: action('onPrimaryButtonClick'),
                }
                : undefined
        }
        userInfo={
            boolean('custom: with user', false, groupID)
                ? {
                    name: text('userInfo.name', 'John Doe', groupID),
                    url: text('userInfo.url', '#username', groupID),
                    onClick: action('userInfo.onClick'),
                    avatar: text('userInfo.avatar', defaultAvatar, groupID),
                }
                : undefined
        }
        menuItems={
            boolean('custom: with menu', false, groupID)
                ? [
                    {
                        key: 'menu1',
                        url: text('menuItems[0].url', '#menu1', groupID),
                        text: text('menuItems[0].text', 'Functionality', groupID),
                        isActive: boolean('menuItems[0].isActive', false, groupID),
                        isDisabled: boolean('menuItems[0].isDisabled', false, groupID),
                        onClick: action('menuItems[0].onClick'),
                        isBig: boolean('menuItems[0].isBig', false, groupID),
                    },
                    {
                        key: 'menu2',
                        url: text('menuItems[1].url', '#menu2', groupID),
                        text: text('menuItems[1].text', 'Tariffs', groupID),
                        isActive: boolean('menuItems[1].isActive', false, groupID),
                        isDisabled: boolean('menuItems[1].isDisabled', false, groupID),
                        onClick: action('menuItems[1].onClick'),
                        isBig: boolean('menuItems[1].isBig', false, groupID),
                    },
                    {
                        key: 'menu3',
                        url: text('menuItems[2].url', '#menu3', groupID),
                        text: text('menuItems[2].text', 'Blog', groupID),
                        isActive: boolean('menuItems[2].isActive', false, groupID),
                        isDisabled: boolean('menuItems[2].isDisabled', false, groupID),
                        onClick: action('menuItems[2].onClick'),
                        isBig: boolean('menuItems[2].isBig', false, groupID),
                    },
                    {
                        key: 'string',
                        url: text('menuItems[3].url', '#menu4', groupID),
                        text: text('menuItems[3].text', 'Sign Up', groupID),
                        isActive: boolean('menuItems[3].isActive', false, groupID),
                        isDisabled: boolean('menuItems[3].isDisabled', false, groupID),
                        onClick: action('menuItems[3].onClick'),
                        isBig: boolean('menuItems[3].isBig', false, groupID),
                    },
                ]
                : undefined
        }
    />
)

createStories({
    title: createComponentTitle('Header'),
    module,
    stories: [
        {
            name: 'Header',
            render: HeaderStoryRender(),
        },
    ],
})