import {
    boolean,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as cardTypes from '../../../../types/assets/components/card/cardTypesConstants'
import * as aligns from '../../../../types/assets/components/cardsList/cardsListAlignsConstants'
import * as icons from '../../../../types/assets/elements/icon/iconsConstants'
import * as colors from '../../../../types/core/colors'
import CardsList from '../../../components/CardsListComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const defaultLogo = require('./1xbetLogo.png')
const defaultActiveLogo = require('./1xbetLogoActive.png')

const groupID = 'CardsList'

const CardsListStoryRender = () => () => (
    <CardsList
        align={
            select(
                'align',
                Object.values(aligns) as any,
                aligns.CARDS_LIST_ALIGN_CENTER,
                groupID,
            )
        }
        cards={[
            {
                key: 'card0',
                type: select('cards[0].type', Object.values(cardTypes) as any, cardTypes.CARD_TYPE_ICON, groupID),
                label: text('cards[0].label', 'Some label', groupID),
                icon: select('cards[0].icon', Object.values(icons) as any, icons.ICON_SPORT_VOLLEYBALL, groupID),
                isActive: boolean('cards[0].isActive', false, groupID),
                isDisabled: boolean('cards[0].isDisabled', false, groupID),
                value: text('cards[0].value', 'card0', groupID),
                logo: select(
                    'logo',
                    {
                        'EXAMPLE: defaultLogo': defaultLogo,
                        'EXAMPLE: defaultActiveLogo': defaultActiveLogo,
                    } as any,
                    defaultLogo as any,
                    groupID,
                ),
                logoBackground: select(
                    'logoBackground',
                    Object.values(colors) as any,
                    colors.COLOR_BLUE_DEEP,
                    groupID,
                ),
            },
            {
                key: 'card1',
                type: select('cards[1].type', Object.values(cardTypes) as any, cardTypes.CARD_TYPE_ICON, groupID),
                label: text('cards[1].label', 'Some label', groupID),
                icon: select('cards[1].icon', Object.values(icons) as any, icons.ICON_SPORT_VOLLEYBALL, groupID),
                isActive: boolean('cards[1].isActive', false, groupID),
                isDisabled: boolean('cards[1].isDisabled', false, groupID),
                value: text('cards[1].value', 'card1', groupID),
                logo: select(
                    'logo',
                    {
                        'EXAMPLE: defaultLogo': defaultLogo,
                        'EXAMPLE: defaultActiveLogo': defaultActiveLogo,
                    } as any,
                    defaultLogo as any,
                    groupID,
                ),
                logoBackground: select(
                    'logoBackground',
                    Object.values(colors) as any,
                    colors.COLOR_BLUE_DEEP,
                    groupID,
                ),
            },
            {
                key: 'card2',
                type: select('cards[2].type', Object.values(cardTypes) as any, cardTypes.CARD_TYPE_ICON, groupID),
                label: text('cards[2].label', 'Some label', groupID),
                icon: select('cards[2].icon', Object.values(icons) as any, icons.ICON_SPORT_VOLLEYBALL, groupID),
                isActive: boolean('cards[2].isActive', false, groupID),
                isDisabled: boolean('cards[2].isDisabled', false, groupID),
                value: text('cards[2].value', 'card2', groupID),
                logo: select(
                    'logo',
                    {
                        'EXAMPLE: defaultLogo': defaultLogo,
                        'EXAMPLE: defaultActiveLogo': defaultActiveLogo,
                    } as any,
                    defaultLogo as any,
                    groupID,
                ),
                logoBackground: select(
                    'logoBackground',
                    Object.values(colors) as any,
                    colors.COLOR_BLUE_DEEP,
                    groupID,
                ),
            },
        ]}
    />
)

createStories({
    title: createComponentTitle('CardsList'),
    module,
    stories: [
        {
            name: 'default',
            render: CardsListStoryRender(),
        },
    ],
})