import { IMAGE_BACKGROUND_TYPE_COVER } from '@betpad/types/assets/components/image/imageBackgroundTypesConstants'
import {
    IMAGE_TYPE_BLOCK,
} from '@betpad/types/assets/components/image/imageTypesConstants'
import { ITableItem } from '@betpad/types/assets/components/table/ITableComponent'
import {
    TABLE_CELL_ALIGN_CENTER,
} from '@betpad/types/assets/components/table/tableCellAlignsConstants'
import { BUTTON_ALIGN_START } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import {
    BUTTON_SIZE_LT,
    BUTTON_SIZE_SM,
} from '@betpad/types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_30,
    BUTTON_STATE_INACTIVE_OPACITY_70,
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_VALUE,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_800,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import * as icons from '../../../../types/assets/elements/icon/iconsConstants'
import Image from '../../../components/ImageComponent'
import {
    QuinarySubHeading,
    SenarySubHeading,
    SeptenarySubHeading,
} from '../../../components/TypographyComponent'
import Table from '../../../src/assets/components/Table/TableComponent'
import Button from '../../../src/assets/elements/Button/ButtonElement'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const miniFonbetLogoImage = require('./miniFonbetIcon.svg')
const mini1xBetLogoImage = require('./mini1xBetIcon.svg')
const miniLigaStavokLogoImage = require('./miniLigaStavokIcon.svg')

const groupID = 'Table'

const TableStoryRender = () => () => {
    const width1 = text('titles[0].width | rows[].items[0].width', '28%', groupID)
    const width2 = text('titles[1].width | rows[].items[1].width', '16.5%', groupID)
    const width3 = text('titles[2].width | rows[].items[2].width', '16.5%', groupID)
    const width4 = text('titles[3].width | rows[].items[3].width', '16.5%', groupID)
    const width5 = text('titles[4].width | rows[].items[4].width', '16.5%', groupID)
    const width6 = text('titles[4].width | rows[].items[4].width', '5%', groupID)

    return (
        <Table
            titles={[
                {
                    key: text('titles[0].key', 'key0', groupID),
                    title: text('titles[0].label', 'Event', groupID),
                    width: width1,
                },
                {
                    key: text('titles[1].key', 'key1', groupID),
                    title: text('titles[1].label', 'Bet', groupID),
                    width: width2,
                },
                {
                    key: text('titles[2].key', 'key2', groupID),
                    title: text('titles[2].label', 'Popularity', groupID),
                    width: width3,
                },
                {
                    key: text('titles[3].key', 'key3', groupID),
                    title: text('titles[3].label', 'Reliability', groupID),
                    width: width4,
                },
                {
                    key: text('titles[4].key', 'key4', groupID),
                    title: text('titles[4].label', 'Coefficient', groupID),
                    width: width5,
                },
                {
                    key: text('titles[5].key', 'key5', groupID),
                    title: text('titles[5].label', '', groupID),
                    width: width6,
                },
            ]}
            rows={[
                {
                    key: '1',
                    items: [
                        ({
                            key: 'event1',
                            render: () => {
                                const Event = compose(
                                    withProps({
                                        renderLeft: select(
                                            'rows[0].items[0].render.renderLeft',
                                            Object.values(icons) as any,
                                            icons.ICON_SPORT_FOOTBALL as any,
                                            groupID,
                                        ),
                                        size: BUTTON_SIZE_SM,
                                        textAlign: BUTTON_ALIGN_START,
                                        align: BUTTON_ALIGN_START,
                                        text: () => (
                                            <React.Fragment>
                                                <QuinarySubHeading
                                                    content={
                                                        text(
                                                            'rows[0].items[0].render.text',
                                                            'Chicago - New York',
                                                            groupID,
                                                        )
                                                    }
                                                    color={COLOR_GRAY_900}
                                                />
                                                <SeptenarySubHeading
                                                    content={
                                                        text(
                                                            'rows[0].items[0].render.hint',
                                                            '12:00 22.01.2019',
                                                            groupID,
                                                        )
                                                    }
                                                    color={COLOR_GRAY_700}
                                                />
                                            </React.Fragment>
                                        ),
                                        state: BUTTON_STATE_INACTIVE_OPACITY_70,
                                    }),
                                )(Button) as any

                                return (
                                    <Event/>
                                )
                            },
                            hasBackground: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'bet1',
                            render: () => (
                                <QuinarySubHeading
                                    content={text('rows[0].items[1].render', 'Win (1)', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                        } as ITableItem),
                        ({
                            key: 'popularity1',
                        } as ITableItem),
                        ({
                            key: 'reliability1',
                        } as ITableItem),
                        ({
                            key: 'coefficient1',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[0].items[4].render', '4.9 - 5.7', groupID)}
                                    color={COLOR_GRAY_800}
                                />
                            ),
                        } as ITableItem),
                        ({
                            key: 'arrow1',
                            render: () => (
                                <Button
                                    renderLeft={icons.ICON_ANGLE}
                                    size={BUTTON_SIZE_LT}
                                    isSquare={true}
                                    color={COLOR_GRAY_900}
                                    state={BUTTON_STATE_INACTIVE_OPACITY_30}
                                />
                            ),
                            align: TABLE_CELL_ALIGN_CENTER,
                            hasHorizontalPadding: false,
                            hasBottomPadding: false,
                            hasTopPadding: false,
                        } as ITableItem),
                    ],
                },
                {
                    key: '2',
                    items: [
                        ({
                            key: 'event2',
                            hasBackground: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'bet2',
                            render: () => (
                                <QuinarySubHeading
                                    content={text('rows[1].items[1].render', 'Win (2)', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                        } as ITableItem),
                        ({
                            key: 'popularity2',
                        } as ITableItem),
                        ({
                            key: 'reliability2',
                        } as ITableItem),
                        ({
                            key: 'coefficient2',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[1].items[4].render', '4.9 - 5.7', groupID)}
                                    color={COLOR_GRAY_800}
                                />
                            ),
                        } as ITableItem),
                        ({
                            key: 'arrow2',
                            render: () => (
                                <Button
                                    renderLeft={icons.ICON_ANGLE}
                                    size={BUTTON_SIZE_LT}
                                    isSquare={true}
                                    color={COLOR_GRAY_900}
                                    state={BUTTON_STATE_INACTIVE_OPACITY_30}
                                />
                            ),
                            align: TABLE_CELL_ALIGN_CENTER,
                            hasHorizontalPadding: false,
                            hasBottomPadding: false,
                            hasTopPadding: false,
                        } as ITableItem),
                    ],
                },
                {
                    key: '3',
                    items: [
                        ({
                            key: 'event3',
                            hasBackground: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'bet3',
                            render: () => (
                                <QuinarySubHeading
                                    content={text('rows[2].items[1].render', 'Win (2)', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'popularity3',
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'reliability3',
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'coefficient3',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[2].items[4].render', '4.9 - 5.7', groupID)}
                                    color={COLOR_GRAY_800}
                                />
                            ),
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'arrow3',
                            render: () => (
                                <Button
                                    renderLeft={icons.ICON_ANGLE_BOTTOM}
                                    size={BUTTON_SIZE_LT}
                                    isSquare={true}
                                    color={COLOR_GRAY_900}
                                    state={BUTTON_STATE_INACTIVE_OPACITY_30}
                                />
                            ),
                            align: TABLE_CELL_ALIGN_CENTER,
                            hasHorizontalPadding: false,
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                    ],
                },
                {
                    key: '4',
                    items: [
                        ({
                            key: 'event3_1',
                            hasBackground: false,
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                        ({
                            key: 'bet3_1',
                            render: () => (
                                <Button
                                    renderLeft={() => (
                                        <Image
                                            url={text(
                                                'rows[3_1].items[1].image', miniFonbetLogoImage, groupID,
                                            )}
                                            type={IMAGE_TYPE_BLOCK}
                                            backgroundType={IMAGE_BACKGROUND_TYPE_COVER}
                                            height={16}
                                            width={16}
                                        />
                                    )}
                                    text={() => (
                                        <SenarySubHeading
                                            content={text('rows[3_1].items[1].render', 'Fonbet', groupID)}
                                            color={COLOR_GRAY_900}
                                        />
                                    )}
                                />
                            ),
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                        ({
                            key: 'popularity3_1',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_1].items[2].render', '16', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                        ({
                            key: 'reliability3_1',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_1].items[3].render', '75%', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                        ({
                            key: 'coefficient3_1',
                            render: () => (
                                <Button
                                    state={[BUTTON_STATE_VALUE, BUTTON_STATE_PRIMARY]}
                                    text={text('rows[3_1].items[4].render', '1.00', groupID)}
                                    size={BUTTON_SIZE_LT}
                                />
                            ),
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                        ({
                            key: 'arrow3_1',
                            hasBorder: false,
                            hasBottomPadding: false,
                        } as ITableItem),
                    ],
                },
                {
                    key: '5',
                    items: [
                        ({
                            key: 'event3_2',
                            hasBackground: false,
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'bet3_2',
                            render: () => (
                                <Button
                                    renderLeft={() => (
                                        <Image
                                            url={text(
                                                'rows[3_2].items[1].image', mini1xBetLogoImage, groupID,
                                            )}
                                            type={IMAGE_TYPE_BLOCK}
                                            backgroundType={IMAGE_BACKGROUND_TYPE_COVER}
                                            height={16}
                                            width={16}
                                        />
                                    )}
                                    text={() => (
                                        <SenarySubHeading
                                            content={text('rows[3_2].items[1].render', '1XBET', groupID)}
                                            color={COLOR_GRAY_900}
                                        />
                                    )}
                                />
                            ),
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'popularity3_2',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_2].items[2].render', '16', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'reliability3_2',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_2].items[3].render', '75%', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'coefficient3_2',
                            render: () => (
                                <Button
                                    state={[BUTTON_STATE_VALUE, BUTTON_STATE_PRIMARY]}
                                    text={text('rows[3_2].items[4].render', '1.00', groupID)}
                                    size={BUTTON_SIZE_LT}
                                />
                            ),
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                        ({
                            key: 'arrow3_2',
                            hasBottomPadding: false,
                            hasTopPadding: false,
                            hasBorder: false,
                        } as ITableItem),
                    ],
                },
                {
                    key: '6',
                    items: [
                        ({
                            key: 'event3_3',
                            hasBackground: false,
                            hasBottomPadding: false,
                            hasTopPadding: false,
                        } as ITableItem),
                        ({
                            key: 'bet3_3',
                            render: () => (
                                <Button
                                    renderLeft={() => (
                                        <Image
                                            url={text(
                                                'rows[3_3].items[1].image', miniLigaStavokLogoImage, groupID,
                                            )}
                                            type={IMAGE_TYPE_BLOCK}
                                            backgroundType={IMAGE_BACKGROUND_TYPE_COVER}
                                            height={16}
                                            width={16}
                                        />
                                    )}
                                    text={() => (
                                        <SenarySubHeading
                                            content={text('rows[3_3].items[1].render', 'Liga Stavok', groupID)}
                                            color={COLOR_GRAY_900}
                                        />
                                    )}
                                />
                            ),
                            hasTopPadding: false,
                        } as ITableItem),
                        ({
                            key: 'popularity3_3',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_3].items[2].render', '16', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasTopPadding: false,
                        } as ITableItem),
                        ({
                            key: 'reliability3_3',
                            render: () => (
                                <SenarySubHeading
                                    content={text('rows[3_3].items[3].render', '75%', groupID)}
                                    color={COLOR_GRAY_900}
                                />
                            ),
                            hasTopPadding: false,
                        } as ITableItem),
                        ({
                            key: 'coefficient3_3',
                            render: () => (
                                <Button
                                    state={[BUTTON_STATE_VALUE, BUTTON_STATE_PRIMARY]}
                                    text={text('rows[3_3].items[4].render', '1.00', groupID)}
                                    size={BUTTON_SIZE_LT}
                                />
                            ),
                            hasTopPadding: false,
                        } as ITableItem),
                        ({
                            key: 'arrow3_2',
                            hasTopPadding: false,
                        } as ITableItem),
                    ],
                },
            ]}
        />
    )
}

createStories({
    title: createComponentTitle('Table'),
    module,
    stories: [
        {
            name: 'default',
            render: TableStoryRender(),
        },
    ],
})