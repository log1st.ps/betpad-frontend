import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import TabsList from '../../../src/assets/components/TabsList/TabsListComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'TabsList'

const TabsListStoryRender = () => () => (
    <TabsList
        value={text('value', 'value2', groupID)}
        items={[
            {
                value: text('items[0].value', 'value0', groupID),
                label: text('items[0].label', 'Tab 1', groupID),
                isDisabled: boolean('items[0].isDisabled', false, groupID),
            },
            {
                value: text('items[1].value', 'value1', groupID),
                label: text('items[1].label', 'Tab 2', groupID),
                isDisabled: boolean('items[1].isDisabled', false, groupID),
            },
            {
                value: text('items[2].value', 'value2', groupID),
                label: text('items[2].label', 'Tab 3', groupID),
                isDisabled: boolean('items[2].isDisabled', false, groupID),
            },
            {
                value: text('items[3].value', 'value3', groupID),
                label: text('items[3].label', 'Tab 4', groupID),
                isDisabled: boolean('items[3].isDisabled', true, groupID),
            },
            {
                value: text('items[4].value', 'value4', groupID),
                label: text('items[4].label', 'Tab 5', groupID),
                isDisabled: boolean('items[4].isDisabled', false, groupID),
            },
        ]}
        onClick={action('onClick')}
    />
)

createStories({
    title: createComponentTitle('TabsList'),
    module,
    stories: [
        {
            name: 'default',
            render: TabsListStoryRender(),
        },
    ],
})