import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import withProps from 'recompose/withProps'
import * as colors from '../../../../types/core/colors'
import * as components from '../../../components/TypographyComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'
import StorybookGridLayout, { GRID_BUTTON_ALIGN_LEFT } from '../../helpers/layouts/gridLayout/StorybookGridLayout'

const groupID = 'Typography'

const getAbstractTypographyStory = (cmps: any) => () => (
    <StorybookGridLayout
        items={
            cmps.map(({key, component}: any) => ({
                key,
                isBlock: true,
                render: withProps({
                    content: text('content', 'Some content for Typography', groupID),
                    color: select('color', [null, ...Object.values(colors)] as any, null as any, groupID),
                })(component),
                buttonAlign: GRID_BUTTON_ALIGN_LEFT,
            }))
        }
    />
)

createStories({
    title: createComponentTitle('Typography'),
    module,
    hasTheme: true,
    stories: [
        {
            name: 'Heading',
            render: getAbstractTypographyStory([
                {key: 'PrimaryHeading', component: components.PrimaryHeading},
                {key: 'SecondaryHeading', component: components.SecondaryHeading},
                {key: 'TertiaryHeading', component: components.TertiaryHeading},
                {key: 'QuatenaryHeading', component: components.QuaternaryHeading},
                {key: 'QuinaryHeading', component: components.QuinaryHeading},
                {key: 'SenaryHeading', component: components.SenaryHeading},
            ]),
        },
        {
            name: 'SubHeading',
            render: getAbstractTypographyStory([
                {key: 'PrimarySubHeading', component: components.PrimarySubHeading},
                {key: 'SecondarySubHeading', component: components.SecondarySubHeading},
                {key: 'TertiarySubHeading', component: components.TertiarySubHeading},
                {key: 'QuatenarySubHeading', component: components.QuaternarySubHeading},
                {key: 'QuinarySubHeading', component: components.QuinarySubHeading},
                {key: 'SenarySubHeading', component: components.SenarySubHeading},
                {key: 'SeptenarySubHeading', component: components.SeptenarySubHeading},
            ]),
        },
        {
            name: 'Caption',
            render: getAbstractTypographyStory([
                {key: 'PrimaryCaption', component: components.PrimaryCaption},
                {key: 'SecondaryCaption', component: components.SecondaryCaption},
            ]),
        },
        {
            name: 'Text',
            render: getAbstractTypographyStory([
                {key: 'Paragraph', component: components.Paragraph},
                {key: 'SubText', component: components.SubText},
                {key: 'Hint', component: components.Hint},
            ]),
        },
    ],
})