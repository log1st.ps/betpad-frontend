import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Pagination from '../../../src/assets/components/Pagination/PaginationComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'Pagination'

const PaginationStoryRender = () => () => (
    <Pagination
        value={text('value', 'value2', groupID)}
        onClick={action('onClick')}
        items={[
            {
                value: text('items[0].value', 'value0', groupID),
                label: text('items[0].label', 'Back', groupID),
                isDisabled: boolean('items[0].isDisabled', false, groupID),
            },
            {
                value: text('items[1].value', 'value1', groupID),
                label: text('items[1].label', '1', groupID),
                isDisabled: boolean('items[0].isDisabled', false, groupID),
            },
            {
                value: text('items[2].value', 'value2', groupID),
                label: text('items[2].label', '2', groupID),
                isDisabled: boolean('items[0].isDisabled', false, groupID),
            },
            {
                value: text('items[2].value', 'value3', groupID),
                label: text('items[2].label', '3', groupID),
                isDisabled: boolean('items[0].isDisabled', false, groupID),
            },
            {
                value: text('items[2].value', 'value4', groupID),
                label: text('items[2].label', 'Next', groupID),
                isDisabled: boolean('items[0].isDisabled', true, groupID),
            },
        ]}
    />
)

createStories({
    title: createComponentTitle('Pagination'),
    module,
    stories: [
        {
            name: 'default',
            render: PaginationStoryRender(),
        },
    ],
})