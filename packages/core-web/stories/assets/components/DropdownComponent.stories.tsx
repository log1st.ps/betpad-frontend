import { action } from '@storybook/addon-actions'
import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Dropdown from '../../../components/DropdownComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'
import { iconsArray } from '../elements/ButtonElement.stories'

const groupID = 'Dropdown'

const DropdownRender = () => (
    <Dropdown
        onOutsideClick={action('onOutsideClick')}
        items={[
            {
                key: 'i0',
                url: text('items.0.url', '', groupID),
                onClick: action('onClick.0'),
                renderLeft: select('items.0.renderLeft', [null, ...iconsArray] as any, null as any, groupID),
                renderRight: select('items.0.renderRight', [null, ...iconsArray] as any, null as any, groupID),
                text: text('items.0.text', 'Item 0', groupID),
            },
            {
                key: 'i1',
                url: text('items.1.url', '', groupID),
                onClick: action('onClick.1'),
                renderLeft: select('items.1.renderLeft', [null, ...iconsArray] as any, null as any, groupID),
                renderRight: select('items.1.renderRight', [null, ...iconsArray] as any, null as any, groupID),
                text: text('items.1.text', 'Item 1', groupID),
            },
            {
                key: 'i2',
                url: text('items.2.url', '', groupID),
                onClick: action('onClick.2'),
                renderLeft: select('items.2.renderLeft', [null, ...iconsArray] as any, null as any, groupID),
                renderRight: select('items.2.renderRight', [null, ...iconsArray] as any, null as any, groupID),
                text: text('items.2.text', 'Item 2', groupID),
            },
        ]}
    />
)

createStories({
    title: createComponentTitle('Dropdown'),
    module,
    stories: [
        {
            name: 'default',
            render: DropdownRender,
        },
    ],
})