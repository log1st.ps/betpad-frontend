import { action } from '@storybook/addon-actions'
import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as icons from '../../../../types/assets/elements/icon/iconsConstants'
import TagsList from '../../../components/TagsListComponent'
import createComponentTitle from '../../helpers/createComponentTitle'
import createStories from '../../helpers/createStories'

const groupID = 'TagsList'

const TagsListStoryRender = () => () => (
    <TagsList
        onClick={action('onClick')}
        items={[
            {
                value: text('items[0].key', 'volleyball', groupID),
                icon: select('items[0].icon', Object.values(icons) as any, icons.ICON_SPORT_VOLLEYBALL, groupID),
                label: text('items[0].label', 'Volleyball', groupID),
            },
            {
                value: text('items[1].key', 'basketball', groupID),
                icon: select('items[1].icon', Object.values(icons) as any, icons.ICON_SPORT_BASKETBALL, groupID),
                label: text('items[1].label', 'Basketball', groupID),
            },
            {
                value: text('items[2].key', 'football', groupID),
                icon: select('items[2].icon', Object.values(icons) as any, icons.ICON_SPORT_FOOTBALL, groupID),
                label: text('items[2].label', 'Football', groupID),
            },
        ]}
    />
)

createStories({
    title: createComponentTitle('TagsList'),
    module,
    stories: [
        {
            name: 'default',
            render: TagsListStoryRender(),
        },
    ],
})