import * as avatarSizes from '@betpad/types/assets/elements/avatar/avatarSizesConstants'
import {
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Avatar from '../../../src/assets/elements/Avatar/AvatarElement'
import createElementTitle from '../../helpers/createElementTitle'
import createStories from '../../helpers/createStories'

const groupID = 'Avatar'

const avatarSizesArray = Object.values(avatarSizes) as any

export const defaultAvatar = require('./avatar.jpg') as string

export const AvatarStoryRender = () => (
    <Avatar
        name={text('name', 'John Doe', groupID)}
        size={select('size', avatarSizesArray as any, avatarSizes.AVATAR_SIZE_SM, groupID)}
        url={text('url', defaultAvatar, groupID)}
    />
)

createStories({
    title: createElementTitle('Avatar'),
    module,
    stories: [
        {
            name: 'default',
            render: AvatarStoryRender,
        },
    ],
})