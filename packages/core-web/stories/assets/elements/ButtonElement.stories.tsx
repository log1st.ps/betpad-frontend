import { action } from '@storybook/addon-actions'
import {
    boolean,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import * as buttonSizes from '../../../../types/assets/elements/button/buttonSizesConstants'
import * as buttonStates from '../../../../types/assets/elements/button/buttonStatesConstants'
import {
    BUTTON_STATE_GRADIENT,
    BUTTON_STATE_VALUE,
} from '../../../../types/assets/elements/button/buttonStatesConstants'
import * as buttonTypes from '../../../../types/assets/elements/button/buttonTypesConstants'
import { IButtonElement } from '../../../../types/assets/elements/button/IButtonElement'
import * as icons from '../../../../types/assets/elements/icon/iconsConstants'
import { IIconElement } from '../../../../types/assets/elements/icon/IIconElement'
import Button from '../../../src/assets/elements/Button/ButtonElement'
import createElementTitle from '../../helpers/createElementTitle'
import createStories from '../../helpers/createStories'
import StorybookGridLayout from '../../helpers/layouts/gridLayout/StorybookGridLayout'

const buttonStatesArray: ReadonlyArray<string> | any = Object.keys(buttonStates).map(key => buttonStates[key])
const buttonSizesArray: ReadonlyArray<string> | any = Object.keys(buttonSizes).map(key => buttonSizes[key])
const buttonTypesArray: ReadonlyArray<string> | any = Object.keys(buttonTypes).map(key => buttonTypes[key])
export const iconsArray: ReadonlyArray<string> | any = Object.keys(icons).map(key => icons[key])

const groupID = 'Button'

const ButtonStoryRender = ({
    state,
    isBlock,
    renderLeft,
    renderRight,
    isCircle,
    text: buttonText,
}: {
    state: IButtonElement['state'],
    isBlock?: IButtonElement['isBlock'],
    renderLeft?: IIconElement['icon'],
    renderRight?: IIconElement['icon'],
    isCircle?: IButtonElement['isCircle'],
    text?: IButtonElement['text'],
}) => () => (
    <Button
        onClick={action('onClick')}
        onFocus={action('onFocus')}
        onBlur={action('onBlur')}
        state={state}
        size={
            select(
                'size',
                buttonSizesArray,
                buttonSizes.BUTTON_SIZE_LT,
                groupID,
            )
        }
        type={
            select(
                'type',
                buttonTypesArray,
                buttonTypes.BUTTON_TYPE_BUTTON,
                groupID,
            )
        }
        url={text('url', `#button_${state}`, groupID)}
        isBlock={typeof isBlock === 'undefined' ? boolean('isBlock', false, groupID) : isBlock}
        isSquare={boolean('isSquare', false, groupID)}
        isDisabled={boolean('isDisabled', false, groupID)}
        isCircle={
            typeof isCircle === 'undefined'
                ? boolean('isCircle', false, groupID)
                : isCircle
        }
        renderLeft={
            typeof renderLeft === 'undefined'
                ? select('renderLeft', [null, ...iconsArray] as any, null as any, groupID)
                : renderLeft
        }
        renderLeftInfo={text('renderLeftInfo', '', groupID)}
        text={
            typeof buttonText === 'undefined'
                ? text('text', 'Some text', groupID)
                : buttonText
        }
        renderRight={
            typeof renderRight === 'undefined'
                ? select('renderRight', [null, ...iconsArray] as any, null as any, groupID)
                : renderRight
        }
        renderRightInfo={text('renderRightInfo', '', groupID)}
        hasStackFromTop={boolean('hasStackFromTop', false, groupID)}
        hasStackFromRight={boolean('hasStackFromRight', false, groupID)}
        hasStackFromBottom={boolean('hasStackFromBottom', false, groupID)}
        hasStackFromLeft={boolean('hasStackFromLeft', false, groupID)}
    />
)

createStories({
    title: createElementTitle('Button'),
    module,
    stories: [
        {
            name: 'default',
            render: () => (
                <StorybookGridLayout
                    items={
                        buttonStatesArray.map((state: IButtonElement['state']) => {
                            const isBlock = boolean('isBlock', false, groupID)

                            return {
                                key: state,
                                render: ButtonStoryRender({state}),
                                isBlock,
                            }
                        })
                    }
                />
            ),
        },
        {
            name: 'multiple states',
            render: ButtonStoryRender({
                state: [BUTTON_STATE_GRADIENT, BUTTON_STATE_VALUE],
            }),
        },
    ],
})