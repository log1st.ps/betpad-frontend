import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Input from '../../../../src/assets/elements/controls/Input/InputElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Input'

const InputStoryRender = () => () => (
    <Input
        value={text('value', '', groupID)}
        placeholder={text('placeholder', '', groupID)}
        isDisabled={boolean('isDisabled', false, groupID)}
        prefix={text('prefix', '', groupID)}
        suffix={text('suffix', '', groupID)}
        onChange={action('onChange')}
        hasInputAfter={boolean('hasInputAfter', false, groupID)}
        hasInputBefore={boolean('hasInputBefore', false, groupID)}
    />
)

createStories({
    title: createElementTitle('controls/Input'),
    module,
    stories: [
        {
            name: 'default',
            render: InputStoryRender(),
        },
    ],
})