import { action } from '@storybook/addon-actions'
import { select } from '@storybook/addon-knobs'
import * as React from 'react'
import Switch from '../../../../src/assets/elements/controls/Switch/SwitchElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Switch'

const SwitchStoryRender = (values: any) => () => (
    <Switch
        values={values}
        value={select('value', values.map(({key}: any) => key), 'one', groupID)}
        onChange={action('onChange')}
    />
)

createStories({
    title: createElementTitle('controls/Switch'),
    module,
    stories: [
        {
            name: 'default',
            render: SwitchStoryRender([
                {
                    key: 'one',
                    text: 'One',
                },
                {
                    key: 'two',
                    text: 'Two',
                },
            ]),
        },
        {
            name: 'more than two elements',
            render: SwitchStoryRender([
                {
                    key: 'one',
                    text: 'One',
                },
                {
                    key: 'two',
                    text: 'Two',
                },
                {
                    key: 'three',
                    text: 'Three',
                },
            ]),
        },
    ],
})