import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Select from '../../../../src/assets/elements/controls/Select/SelectElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Select'

const SelectStoryRender = () => () => (
    <Select
        value={text('value', 'value0', groupID)}
        values={[
            {
                value: text('values[0].value', 'value0', groupID),
                text: text('values[0].text', 'Value 1', groupID),
            },
            {
                value: text('values[1].value', 'value1', groupID),
                text: text('values[1].text', 'Value 2', groupID),
            },
            {
                value: text('values[2].value', 'value2', groupID),
                text: text('values[2].text', 'Value 3', groupID),
            },
            {
                value: text('values[3].value', 'value3', groupID),
                text: text('values[3].text', 'Value 4', groupID),
            },
        ]}
        placeholder={text('placeholder', 'Choose...', groupID)}
        hasInputAfter={boolean('hasInputAfter', false, groupID)}
        hasInputBefore={boolean('hasInputBefore', false, groupID)}
        onChange={action('onChange')}
        isDisabled={boolean('isDisabled', false, groupID)}
        texts={{
            noData: 'No data',
        }}
    />
)

createStories({
    title: createElementTitle('controls/Select'),
    module,
    stories: [
        {
            name: 'default',
            render: SelectStoryRender(),
        },
    ],
})