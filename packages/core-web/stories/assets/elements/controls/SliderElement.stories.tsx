import { action } from '@storybook/addon-actions'
import {
    boolean,
    number,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Slider from '../../../../src/assets/elements/controls/Slider/SliderElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Slider'

const SliderStoryRender = (isRange: boolean | undefined = false) => () => (
    <Slider
        isRange={typeof isRange === 'undefined' ? boolean(
            'isRange',
            false,
            groupID,
        ) : isRange}
        step={+number('step', 1, {}, groupID)}
        min={+number('min', 0, {}, groupID)}
        max={+number('max', 100, {}, groupID)}
        value={
            isRange ? [
                +number('value[0]', 25, {}, groupID),
                +number('value[1]', 75, {}, groupID),
            ] : +number('value', 50, {}, groupID)
        }
        onChange={action('onChange')}
    />
)

createStories({
    title: createElementTitle('controls/Slider'),
    module,
    stories: [
        {
            name: 'default',
            render: SliderStoryRender(false),
        },
        {
            name: 'range',
            render: SliderStoryRender(true),
        },
    ],
})