import { text } from '@storybook/addon-knobs'
import * as React from 'react'
import Field from '../../../../src/assets/elements/controls/Field/FieldElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Field'

const FieldStoryRender = () => () => (
    <Field label={text('label', 'Some label', groupID)}>
        {text('children', 'Some children', groupID)}
    </Field>
)

createStories({
    title: createElementTitle('controls/Field'),
    module,
    stories: [
        {
            name: 'default',
            render: FieldStoryRender(),
        },
    ],
})