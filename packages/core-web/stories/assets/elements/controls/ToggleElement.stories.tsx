import { action } from '@storybook/addon-actions'
import {
    boolean,
    select,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import { IToggleElement } from '../../../../../types/assets/elements/controls/toggle/IToggleElement'
import * as labelPositions from '../../../../../types/assets/elements/controls/toggle/toggleLabelPositionsConstants'
import * as toggleTypes from '../../../../../types/assets/elements/controls/toggle/toggleTypesConstants'
import {
    TOGGLE_TYPE_CHECKBOX,
    TOGGLE_TYPE_RADIOBUTTON,
    TOGGLE_TYPE_SWITCH,
} from '../../../../../types/assets/elements/controls/toggle/toggleTypesConstants'
import Toggle from '../../../../src/assets/elements/controls/Toggle/ToggleElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Toggle'

const ToggleStoryRender = (type: IToggleElement['type'] | undefined = undefined) => () => (
    <Toggle
        type={typeof type === 'undefined' ? select(
            'type',
            Object.values(toggleTypes) as any,
            toggleTypes.TOGGLE_TYPE_CHECKBOX,
            groupID,
        ) : type}
        trueValue={text('trueValue', '1', groupID)}
        falseValue={text('falseValue', '0', groupID)}
        onChange={action('onChange')}
        label={text('label', 'Something', groupID)}
        labelPosition={select(
            'labelPosition',
            Object.values(labelPositions) as any,
            labelPositions.TOGGLE_LABEL_POSITION_AFTER,
            groupID,
        )}
        isDisabled={boolean('isDisabled', false, groupID)}
        isChecked={boolean('isChecked', false, groupID)}
    />
)

createStories({
    title: createElementTitle('controls/Toggle'),
    module,
    stories: [
        {
            name: TOGGLE_TYPE_CHECKBOX,
            render: ToggleStoryRender(TOGGLE_TYPE_CHECKBOX),
        },
        {
            name: TOGGLE_TYPE_RADIOBUTTON,
            render: ToggleStoryRender(TOGGLE_TYPE_RADIOBUTTON),
        },
        {
            name: TOGGLE_TYPE_SWITCH,
            render: ToggleStoryRender(TOGGLE_TYPE_SWITCH),
        },
    ],
})