import { action } from '@storybook/addon-actions'
import {
    boolean,
    text,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Datepicker from '../../../../src/assets/elements/controls/Datepicker/DatepickerElement'
import createElementTitle from '../../../helpers/createElementTitle'
import createStories from '../../../helpers/createStories'

const groupID = 'Datepicker'

const DatepickerStoryRender = (value: number | [number, number], isRange = false) => () => {
    let values: (number | number[]) = +text(
        isRange ? 'value[0]' : 'value',
        String(isRange ? value[0] : value),
        groupID,
    )
    
    if (isRange) {
        values = [
            values,
            +text('value[1]', String(value[1]), groupID),
        ]
    }

    return (
        <Datepicker
            value={values}
            isRange={isRange}
            format={text('format', 'DD/MM/YY', groupID)}
            isDisabled={boolean('isDisabled', false, groupID)}
            onChange={action('onChange')}
            hasInputBefore={boolean('hasInputBefore', false, groupID)}
            hasInputAfter={boolean('hasInputAfter', false, groupID)}
        />
    )
}

createStories({
    title: createElementTitle('controls/Datepicker'),
    module,
    stories: [
        {
            name: 'default',
            render: DatepickerStoryRender(1548720000),
        },
        {
            name: 'range',
            render: DatepickerStoryRender([1548720000, 1549065600], true),
        },
    ],
})