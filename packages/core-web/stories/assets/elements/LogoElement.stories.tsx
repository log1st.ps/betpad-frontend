import * as React from 'react'
import Logo from '../../../src/assets/elements/Logo/LogoElement'
import createElementTitle from '../../helpers/createElementTitle'
import createStories from '../../helpers/createStories'

export const LogoStoryRender = () => (
    <Logo/>
)

createStories({
    title: createElementTitle('Logo'),
    module,
    stories: [
        {
            name: 'Logo',
            render: LogoStoryRender,
        },
    ],
})