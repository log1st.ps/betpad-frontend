import * as iconsConstants from '@betpad/types/assets/elements/icon/iconsConstants'
import * as iconSizesConstants from '@betpad/types/assets/elements/icon/iconSizesConstants'
import * as iconStatesConstants from '@betpad/types/assets/elements/icon/iconStatesConstants'
import {
    boolean,
    select,
} from '@storybook/addon-knobs'
import * as React from 'react'
import Icon from '../../../src/assets/elements/Icon/IconElement'
import createElementTitle from '../../helpers/createElementTitle'
import createStories from '../../helpers/createStories'
import StorybookGridLayout from '../../helpers/layouts/gridLayout/StorybookGridLayout'

const groupID = 'Icon'

const iconsConstantsArray: ReadonlyArray<string> | any = Object.keys(iconsConstants).map(key => iconsConstants[key])

const iconSizesConstantsArray:
    ReadonlyArray<string> | any =
    Object.keys(iconSizesConstants).map(key => iconSizesConstants[key])

const iconStatesConstantsArray:
    ReadonlyArray<string> | any =
    Object.keys(iconStatesConstants).map(key => iconStatesConstants[key])

createStories({
    title: createElementTitle('Icon'),
    module,
    hasTheme: true,
    stories: [
        {
            name: 'default',
            render: () => (
                <StorybookGridLayout
                    items={
                        iconsConstantsArray.map((icon: any) => ({
                            key: icon,
                            render: (
                                <Icon
                                    icon={icon}
                                    size={
                                        select(
                                            'size',
                                            iconSizesConstantsArray,
                                            iconSizesConstants.ICON_SIZE_LT,
                                            groupID,
                                        )
                                    }
                                    state={
                                        select(
                                            'state',
                                            iconStatesConstantsArray,
                                            iconStatesConstants.ICON_STATE_PURE,
                                            groupID,
                                        )
                                    }
                                    isSquare={boolean('isSquare', false, groupID)}
                                />
                            ),
                        }))
                    }
                />
            ),
        },
    ],
})