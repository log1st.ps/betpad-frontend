import { IRenderable } from '@betpad/types/core/IRenderable'
import * as React from 'react'
import MainLayout from '../../../../components/layouts/MainLayoutComponent'
import { IStorybookMainLayoutScss } from './storybookMainLayout.scss'

const styles: IStorybookMainLayoutScss = require('./storybookMainLayout.scss')

interface IStorybookMainLayout {
    children: IRenderable,
}

const StorybookMainLayout = ({
    children,
}: IStorybookMainLayout) => (
    <MainLayout>
        <div className={styles.container}>
            {children}
        </div>
    </MainLayout>
)

export default StorybookMainLayout