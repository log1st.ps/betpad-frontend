import { IRenderable } from '@betpad/types/core/IRenderable'
import cn from 'classnames'
import * as React from 'react'
import { createElement } from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { IStorybookGridLayoutScss } from './storybookGridLayout.scss'

const styles: IStorybookGridLayoutScss = require('./storybookGridLayout.scss')

export const GRID_BUTTON_ALIGN_LEFT = 'left'
export const GRID_BUTTON_ALIGN_CENTER = 'center'
export const GRID_BUTTON_ALIGN_RIGHT = 'right'

type availableAligns =
    typeof GRID_BUTTON_ALIGN_LEFT
    | typeof GRID_BUTTON_ALIGN_CENTER
    | typeof GRID_BUTTON_ALIGN_RIGHT

interface IStorybookGridLayout {
    items: Array<{
        key: string,
        render: IRenderable,
        isBlock?: boolean,
        buttonAlign?: availableAligns,
    }>,
}

interface IConnectedStorybookGridLayout extends IStorybookGridLayout {
    onLabelClick?(item: string): void
}

const StorybookGridLayoutComponent = ({
    items,
    onLabelClick,
}: IConnectedStorybookGridLayout) => (
    <div className={styles.container}>
        {items.map(({ key, render, isBlock, buttonAlign = GRID_BUTTON_ALIGN_CENTER }) => (
            <div
                key={key}
                className={cn(
                    styles.item,
                    {
                        [styles.itemIsBlock]: isBlock,
                    },
                )}
            >
                <div
                    className={cn(
                        styles.itemContent,
                        {
                            [styles.itemContentIsBlock]: isBlock,
                        },
                    )}
                >
                    {typeof render === 'function' ? createElement(render as any) : render}
                </div>
                <button
                    title='copy?'
                    onClick={() => onLabelClick && onLabelClick(key)}
                    className={cn(
                        styles.itemTitle,
                        styles[`${buttonAlign}TitleAlign`],
                    )}
                >
                    {key}
                </button>
            </div>
        ))}
    </div>
)

const StorybookGridLayout: React.ComponentClass<IStorybookGridLayout> = compose<any, any>(
    withProps({
        onLabelClick: (item: string) => {
            const el = document.createElement('textarea')
            el.value = item
            document.body.appendChild(el)
            el.select()
            document.execCommand('copy')
            document.body.removeChild(el)
            alert('copied')
        },
    }),
)(StorybookGridLayoutComponent)

export default StorybookGridLayout