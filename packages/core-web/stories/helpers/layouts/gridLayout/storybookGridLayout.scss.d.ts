export interface IStorybookGridLayoutScss {
  'container': string;
  'item': string;
  'itemContent': string;
  'itemTitle': string;
  'leftTitleAlign': string;
  'centerTitleAlign': string;
  'rightTitleAlign': string;
  'itemIsBlock': string;
  'itemContentIsBlock': string;
}

export const locals: IStorybookGridLayoutScss;
