import { IThemeProviderComponent } from '@betpad/types/assets/providers/themeProvider/IThemeProviderComponent'
import {
    THEME_LIGHT,
} from '@betpad/types/assets/providers/themeProvider/themeConstants'
import { ICreateStories } from '@betpad/types/stories/ICreateStories'
import {
    boolean,
    select,
    withKnobs,
} from '@storybook/addon-knobs'
import { storiesOf } from '@storybook/react'
import * as React from 'react'
import * as themes from '../../../types/assets/providers/themeProvider/themeConstants'
import ThemeProvider from '../../components/providers/ThemeProviderComponent'
import StorybookMainLayout from './layouts/mainLayout/StorybookMainLayout'

const themesArray: ReadonlyArray<string> | any = Object.keys(themes).map(key => themes[key])

const withTheme = ({ defaultTheme, hasTheme }: {
    defaultTheme: IThemeProviderComponent['theme'],
    hasTheme?: boolean,
}) => (story: any) => (
    <ThemeProvider
        theme={hasTheme ? select('theme', themesArray, defaultTheme, 'Theme') : (name || THEME_LIGHT)}
        isMobile={hasTheme ? boolean('isMobile', false, 'Theme') : false}
    >
        <StorybookMainLayout>
            {story()}
        </StorybookMainLayout>
    </ThemeProvider>
)

export default ({
    title,
    stories,
    hasTheme = false,
    defaultTheme = THEME_LIGHT,
    module,
}: ICreateStories) => {
    const config = storiesOf(title, module)
        .addDecorator(withKnobs)
        .addDecorator(withTheme({
            defaultTheme,
            hasTheme,
        }))
    stories.map(({ name, render, parameters }) => {
        config
            .add(name, render, parameters)
    })
}