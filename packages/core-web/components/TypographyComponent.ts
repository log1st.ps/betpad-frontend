import {
    Hint,
    Paragraph,
    PrimaryCaption,
    PrimaryHeading,
    PrimarySubHeading,
    QuaternaryHeading,
    QuaternarySubHeading,
    QuinaryHeading,
    QuinarySubHeading,
    SecondaryCaption,
    SecondaryHeading,
    SecondarySubHeading,
    SenaryHeading,
    SenarySubHeading,
    SeptenarySubHeading,
    SubText,
    TertiaryHeading,
    TertiarySubHeading,
} from '../src/assets/components/Typography/TypographyComponent'

export {PrimaryHeading}
export {PrimarySubHeading}
export {SecondaryHeading}
export {SecondarySubHeading}
export {TertiaryHeading}
export {TertiarySubHeading}
export {QuaternaryHeading}
export {QuaternarySubHeading}
export {QuinaryHeading}
export {QuinarySubHeading}
export {SenaryHeading}
export {SenarySubHeading}
export {SeptenarySubHeading}
export {PrimaryCaption}
export {SecondaryCaption}
export {Paragraph}
export {SubText}
export {Hint}
