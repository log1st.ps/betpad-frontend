import Form from '../src/assets/components/Form/FormComponent'
import Datepicker from '../src/assets/elements/controls/Datepicker/DatepickerElement'
import Field from '../src/assets/elements/controls/Field/FieldElement'
import Input from '../src/assets/elements/controls/Input/InputElement'
import Select from '../src/assets/elements/controls/Select/SelectElement'
import Slider from '../src/assets/elements/controls/Slider/SliderElement'
import Switch from '../src/assets/elements/controls/Switch/SwitchElement'
import Toggle from '../src/assets/elements/controls/Toggle/ToggleElement'

export {
    Datepicker,
    Field,
    Input,
    Select,
    Slider,
    Switch,
    Toggle,
}

export default Form