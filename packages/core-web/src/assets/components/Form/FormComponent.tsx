import * as React from 'react'
import { createElement } from 'react'
import compose from 'recompose/compose'
import { IFormComponent } from '../../../../../types/assets/components/form/IFormComponent'

const FormComponent = ({
    forwardedRef,
    children,
    onSubmit,
}: IFormComponent) => (
    <form
        ref={forwardedRef}
        onSubmit={onSubmit}
    >
        {typeof children === 'function' ? createElement(children as any) : children}
    </form>
)

const Form: React.ComponentClass<IFormComponent> = compose<any, any>(

)(FormComponent)

export default Form