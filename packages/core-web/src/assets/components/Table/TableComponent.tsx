import cn from 'classnames'
import * as React from 'react'
import { createElement } from 'react'
import compose from 'recompose/compose'

import {
    ITableComponent,
    ITableItem,
    ITableRow,
} from '../../../../../types/assets/components/table/ITableComponent'
import { Hint } from '../Typography/TypographyComponent'
import { ITableScss } from './table.scss'

const styles: ITableScss = require('./table.scss')

const TableCell = ({
    render,
    width,
    hasHorizontalPadding,
    hasTopPadding,
    hasBottomPadding,
    align,
    hasBorder,
    hasBackground,
}: ITableItem) => {
    const Tag = 'div' as any

    return (
        <Tag
            className={cn(
                styles.cell,
                {
                    [styles.cellWithBorder]: hasBorder,
                    [styles.cellWithBackground]: hasBackground,
                },
            )}
            style={{
                width,
            }}
        >
            <div
                className={cn(
                    styles.cellContent,
                    styles[`${align}CellAlign`],
                    {
                        [styles.cellWithAlign]: align,
                        [styles.cellContentWithHorizontalPadding]: hasHorizontalPadding,
                        [styles.cellContentWithTopPadding]: hasTopPadding,
                        [styles.cellContentWithBottomPadding]: hasBottomPadding,
                    },
                )}
            >
                {typeof render === 'function' ? createElement(render as any) : render}
            </div>
        </Tag>
    )
}

TableCell.defaultProps = {
    hasTopPadding: true,
    hasBottomPadding: true,
    hasHorizontalPadding: true,
    hasBorder: true,
    hasBackground: true,
}

const TableRow = ({
    items,
}: ITableRow) => (
    <div className={styles.row}>
        {items.map(({key, ...item}) => (
            <TableCell key={key} {...item}/>
        ))}
    </div>
)

const TableComponent = ({
    forwardedRef,
    rows,
    titles,
}: ITableComponent) => (
    <div
        ref={forwardedRef}
        className={styles.table}
    >
        <div className={styles.header}>
            {titles.map(({key, title, width}) => (
                <div
                    key={key}
                    className={styles.title}
                    style={{
                        width,
                    }}
                >
                    <Hint content={title}/>
                </div>
            ))}
        </div>
        <div className={styles.body}>
            {rows.map(({key, ...row}) => (
                <TableRow key={key} {...row}/>
            ))}
        </div>
    </div>
)

const Table: React.ComponentClass<ITableComponent> = compose<any, any>(

)(TableComponent)

export default Table