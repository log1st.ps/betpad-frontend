export interface ITableScss {
  'table': string;
  'header': string;
  'title': string;
  'body': string;
  'row': string;
  'cell': string;
  'cellWithBorder': string;
  'cellWithBackground': string;
  'cellContent': string;
  'cellContentWithTopPadding': string;
  'cellContentWithBottomPadding': string;
  'cellContentWithHorizontalPadding': string;
  'cellWithAlign': string;
  'startCellAlign': string;
  'centerCellAlign': string;
  'endCellAlign': string;
}

export const locals: ITableScss;
