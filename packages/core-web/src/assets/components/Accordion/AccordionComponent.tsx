import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import { IAccordionComponent } from '../../../../../types/assets/components/accordeon/IAccordionComponent'
import {
    ICON_ANGLE,
} from '../../../../../types/assets/elements/icon/iconsConstants'
import { ICON_SIZE_LT } from '../../../../../types/assets/elements/icon/iconSizesConstants'
import { ICON_STATE_INHERIT } from '../../../../../types/assets/elements/icon/iconStatesConstants'
import Icon from '../../elements/Icon/IconElement'
import { QuinarySubHeading } from '../Typography/TypographyComponent'
import { IAccordionScss } from './accordion.scss'

const styles: IAccordionScss = require('./accordion.scss')

const AccordionComponent = ({
    label,
    isActive,
    forwardedRef,
    onClick,
    children,
}: IAccordionComponent) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles.isActive]: isActive,
            },
        )}
    >
        {label && (
            <button
                onClick={onClick}
                className={styles.label}
            >
                <div className={styles.title}>
                    <QuinarySubHeading content={label}/>
                </div>
                <div className={styles.icon}>
                    <Icon
                        icon={ICON_ANGLE}
                        size={ICON_SIZE_LT}
                        state={ICON_STATE_INHERIT}
                        isSquare={true}
                    />
                </div>
            </button>
        )}
        <div
            className={cn(
                styles.content,
                {
                    [styles.contentActive]: isActive,
                },
            )}
        >
            {children}
        </div>
    </div>
)

const Accordion: React.ComponentClass<IAccordionComponent> = compose<any, any>(
)(AccordionComponent)

export default Accordion