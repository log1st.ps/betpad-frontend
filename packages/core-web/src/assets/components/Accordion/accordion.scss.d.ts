export interface IAccordionScss {
  'container': string;
  'label': string;
  'title': string;
  'icon': string;
  'isActive': string;
  'content': string;
  'contentActive': string;
}

export const locals: IAccordionScss;
