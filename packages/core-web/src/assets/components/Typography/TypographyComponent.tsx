import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'

import {
    IHintComponent,
    IParagraphComponent,
    IPrimaryCaption,
    IPrimaryHeadingComponent,
    IPrimarySubHeadingComponent,
    IQuaternaryHeadingComponent,
    IQuaternarySubHeadingComponent,
    IQuinaryHeadingComponent,
    IQuinarySubHeadingComponent,
    ISecondaryCaption,
    ISecondaryHeadingComponent,
    ISecondarySubHeadingComponent,
    ISenaryHeadingComponent,
    ISenarySubHeadingComponent,
    ISeptenarySubHeadingComponent,
    ISubTextComponent,
    ITertiaryHeadingComponent,
    ITertiarySubHeadingComponent,
    ITypographyComponent,
} from '../../../../../types/assets/components/typography/ITypographyComponent'
import { ITypographyScss } from './typography.scss'

const styles: ITypographyScss = require('./typography.scss')

interface IConnectedTypographyComponent extends ITypographyComponent {
    Tag: any,
    className: string | string[],
}

const AbstractPureTypographyComponent = ({
    Tag,
    content,
    className,
    forwardedRef,
    color,
    textAlign,
}: IConnectedTypographyComponent) => (
    <Tag
        ref={forwardedRef}
        className={
            cn(
                styles.typography,
                className,
                styles[`${color}Color`],
                styles[`${textAlign}TextAlign`],
            )
        }
    >
        {content}
    </Tag>
)

const AbstractTypographyComponent
    :React.ComponentClass<IConnectedTypographyComponent>
    = compose<any, any>(

    )(AbstractPureTypographyComponent)

export const PrimaryHeading
    : React.ComponentClass<IPrimaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h1',
            className: [styles.heading, styles.primary],
        }),
    )(AbstractTypographyComponent)

export const SecondaryHeading
    : React.ComponentClass<ISecondaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h2',
            className: [styles.heading, styles.secondary],
        }),
    )(AbstractTypographyComponent)

export const TertiaryHeading
    : React.ComponentClass<ITertiaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h3',
            className: [styles.heading, styles.tertiary],
        }),
    )(AbstractTypographyComponent)

export const QuaternaryHeading
    : React.ComponentClass<IQuaternaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h4',
            className: [styles.heading, styles.quatenary],
        }),
    )(AbstractTypographyComponent)

export const QuinaryHeading
    : React.ComponentClass<IQuinaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h5',
            className: [styles.heading, styles.quinary],
        }),
    )(AbstractTypographyComponent)

export const SenaryHeading
    : React.ComponentClass<ISenaryHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'h6',
            className: [styles.heading, styles.senary],
        }),
    )(AbstractTypographyComponent)

export const PrimarySubHeading
    : React.ComponentClass<IPrimarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.primary],
        }),
    )(AbstractTypographyComponent)

export const SecondarySubHeading
    : React.ComponentClass<ISecondarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.secondary],
        }),
    )(AbstractTypographyComponent)

export const TertiarySubHeading
    : React.ComponentClass<ITertiarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.tertiary],
        }),
    )(AbstractTypographyComponent)

export const QuaternarySubHeading
    : React.ComponentClass<IQuaternarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.quatenary],
        }),
    )(AbstractTypographyComponent)

export const QuinarySubHeading
    : React.ComponentClass<IQuinarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.quinary],
        }),
    )(AbstractTypographyComponent)

export const SenarySubHeading
    : React.ComponentClass<ISenarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.senary],
        }),
    )(AbstractTypographyComponent)

export const SeptenarySubHeading
    : React.ComponentClass<ISeptenarySubHeadingComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.subHeading, styles.septenary],
        }),
    )(AbstractTypographyComponent)

export const PrimaryCaption
    : React.ComponentClass<IPrimaryCaption> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.caption, styles.primary],
        }),
    )(AbstractTypographyComponent)

export const SecondaryCaption
    : React.ComponentClass<ISecondaryCaption> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.caption, styles.secondary],
        }),
    )(AbstractTypographyComponent)

export const Paragraph
    : React.ComponentClass<IParagraphComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.text, styles.paragraph],
        }),
    )(AbstractTypographyComponent)

export const SubText
    : React.ComponentClass<ISubTextComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.text, styles.subText],
        }),
    )(AbstractTypographyComponent)

export const Hint
    : React.ComponentClass<IHintComponent> = compose<any, any>(
        withProps({
            Tag: 'div',
            className: [styles.text, styles.hint],
        }),
    )(AbstractTypographyComponent)