import {
    CARD_TYPE_ICON,
    CARD_TYPE_LOGO,
    CARD_TYPE_TEXT,
} from '@betpad/types/assets/components/card/cardTypesConstants'
import { ICardComponent } from '@betpad/types/assets/components/card/ICardComponent'
import { ICON_SIZE_LG } from '@betpad/types/assets/elements/icon/iconSizesConstants'
import {
    ICON_STATE_INHERIT,
} from '@betpad/types/assets/elements/icon/iconStatesConstants'
import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import Icon from '../../elements/Icon/IconElement'
import {
    PrimarySubHeading,
    SeptenarySubHeading,
} from '../Typography/TypographyComponent'
import { ICardScss } from './card.scss'

const styles: ICardScss = require('./card.scss')

interface IConnectedCardComponent extends ICardComponent{
    onKeyPress(): void,
}

const CardComponent = ({
    forwardedRef,
    type,
    label,
    logo,
    logoBackground,
    value,
    icon,
    isActive,
    isDisabled,
    onClick,
    onKeyPress,
}: IConnectedCardComponent) => (
    <div
        ref={forwardedRef}
        onClick={onClick}
        onKeyPress={onKeyPress}
        tabIndex={0}
        className={
            cn(
                styles.card,
                {
                    [styles.isActive]: isActive,
                    [styles.isDisabled]: isDisabled,
                    [styles[`${logoBackground}Background`]]: type === CARD_TYPE_LOGO,
                },
            )
        }
    >
        <div className={styles.content}>
            {type === CARD_TYPE_ICON && (
                <div className={styles.icon}>
                    <Icon
                        icon={icon as any}
                        state={ICON_STATE_INHERIT}
                        size={ICON_SIZE_LG}
                    />
                </div>
            )}
            {type === CARD_TYPE_LOGO && (
                <div
                    style={{
                        backgroundImage: `url(${logo})`,
                    }}
                    className={styles.logo}
                />
            )}
            {
                [CARD_TYPE_TEXT, CARD_TYPE_ICON].indexOf(type) > -1
                && label
                && (
                    <div className={styles.label}>
                        <SeptenarySubHeading content={label}/>
                    </div>
                )
            }
            {type === CARD_TYPE_TEXT && (
                <div className={styles.text}>
                    <PrimarySubHeading content={value}/>
                </div>
            )}
        </div>
    </div>
)

const Card: React.ComponentClass<ICardComponent> = compose<any, any>(
    withProps(({onClick, isDisabled}) => ({
        onKeyPress(e: KeyboardEvent) {
            if (!isDisabled && onClick && [' ', 'Enter'].indexOf(e.key) > -1) {
                e.preventDefault()
                onClick()
            }
        },
    })),
)(CardComponent)

export default Card