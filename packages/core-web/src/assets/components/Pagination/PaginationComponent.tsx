import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { IPaginationComponent } from '../../../../../types/assets/components/pagination/IPaginationComponent'
import {
    BUTTON_SIZE_XS,
} from '../../../../../types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_STATE_ACTIVE,
    BUTTON_STATE_NAVIGATE,
} from '../../../../../types/assets/elements/button/buttonStatesConstants'
import Button from '../../elements/Button/ButtonElement'
import { IPaginationScss } from './pagination.scss'

const styles: IPaginationScss = require('./pagination.scss')

interface IConnectedPaginationComponent extends IPaginationComponent {
    onItemClick(value: string): () => void,
}

const PaginationComponent = ({
    items,
    forwardedRef,
    onItemClick,
    value,
}: IConnectedPaginationComponent) => (
    <div
        ref={forwardedRef}
        className={styles.container}
    >
        {items.map(({label, value: itemValue, isDisabled}, i) => (
            <div
                key={itemValue}
                className={styles.item}
            >
                <Button
                    size={BUTTON_SIZE_XS}
                    state={
                        itemValue !== value
                            ? BUTTON_STATE_NAVIGATE
                            : [BUTTON_STATE_NAVIGATE, BUTTON_STATE_ACTIVE]
                    }
                    isDisabled={isDisabled}
                    text={label}
                    onClick={onItemClick(value as string)}
                    hasStackFromLeft={i > 0}
                    hasStackFromRight={i < items.length - 1}
                />
            </div>
        ))}
    </div>
)

const Pagination: React.ComponentClass<IPaginationComponent> = compose<any, any>(
    withProps(({onClick}: IPaginationComponent) => ({
        onItemClick: (value: string) => () => {
            if (onClick) {
                onClick(value)
            }
        },
    })),
)(PaginationComponent)

export default Pagination