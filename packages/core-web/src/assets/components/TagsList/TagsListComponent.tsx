import {
    ITagsListComponent,
    ITagsListElement,
} from '@betpad/types/assets/components/tagsList/ITagsListComponent'
import { BUTTON_SIZE_LT } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_TAG } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { ICON_CLOSE } from '@betpad/types/assets/elements/icon/iconsConstants'
import * as React from 'react'
import { withProps } from 'recompose'
import compose from 'recompose/compose'
import Button from '../../elements/Button/ButtonElement'
import { ITagsListScss } from './tagsList.scss'


const styles: ITagsListScss = require('./tagsList.scss')

interface IConnectedTagsListComponent extends ITagsListComponent {
    onTagClick(value: string): () => void,
}

interface IConnectedTagsListElement extends ITagsListElement {
    onClick?(): void,
}

const TagsListElement = ({
    icon,
    onClick,
    label,
}: IConnectedTagsListElement) => (
    <Button
        isCircle={true}
        state={BUTTON_STATE_TAG}
        renderLeft={icon}
        renderRight={ICON_CLOSE}
        onClick={onClick}
        text={label}
        size={BUTTON_SIZE_LT}
    />
)

const TagsListComponent = ({
    items,
    forwardedRef,
    onTagClick,
}: IConnectedTagsListComponent) => (
    <div
        ref={forwardedRef}
        className={styles.container}
    >
        {items.map(({icon, value, label}) => (
            <div
                key={value}
                className={styles.item}
            >
                <TagsListElement
                    value={value}
                    icon={icon}
                    onClick={onTagClick(value)}
                    label={label}
                />
            </div>
        ))}
    </div>
)

const TagsList: React.ComponentClass<ITagsListComponent> = compose<any, any>(
    withProps(({onClick}: ITagsListComponent) => ({
        onTagClick: (value: string) => () => {
            if (onClick) {
                onClick(value)
            }
        },
    })),

)(TagsListComponent)

export default TagsList