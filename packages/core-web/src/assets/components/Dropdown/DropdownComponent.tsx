import { IDropdownComponent } from '@betpad/types/assets/components/dropdown/IDropdownComponent'
import { BUTTON_ALIGN_START } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XS } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_70,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    BUTTON_TYPE_BUTTON,
    BUTTON_TYPE_LINK,
} from '@betpad/types/assets/elements/button/buttonTypesConstants'
import {
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withEvents from '../../../../../helpers/hocs/withEvents'
import Button from '../../elements/Button/ButtonElement'

interface IConnectedDropdownComponent extends IDropdownComponent {
    containerRef: React.RefObject<HTMLDivElement>
}

const styles = require('./dropdown.scss')


const DropdownComponent = ({
    items,
    forwardedRef,
    containerRef,
}: IConnectedDropdownComponent) => (
    <div
        ref={forwardedRef}
    >
        <div
            ref={containerRef}
            className={styles.container}
        >
            {items.map(item => (
                <Button
                    key={item.key}
                    size={BUTTON_SIZE_XS}
                    type={item.url ? BUTTON_TYPE_LINK : BUTTON_TYPE_BUTTON}
                    isBlock={true}
                    state={[BUTTON_STATE_INACTIVE_OPACITY_70]}
                    color={COLOR_GRAY_900}
                    textAlign={BUTTON_ALIGN_START}
                    {...item}
                />
            ))}
        </div>
    </div>
)

const Dropdown: React.ComponentClass<IDropdownComponent> = compose<any, any>(
    withProps(() => ({
        containerRef: React.createRef(),
    })),
    withEvents({
        target: document,
        targetKey: 'document',
        events: ['click'],
        handlers(getProps: () => any): { [p: string]: (e: Event) => void } {
            const {
                onOutsideClick,
                containerRef,
            } = getProps()

            const container = containerRef.current

            return {
                onDocumentClick: (({target}:any) => {
                    if (
                        onOutsideClick
                        && container
                        && !container.contains(target)
                    ) {
                        onOutsideClick()
                    }
                }),
            }
        },
    }),
)(DropdownComponent)

export default Dropdown