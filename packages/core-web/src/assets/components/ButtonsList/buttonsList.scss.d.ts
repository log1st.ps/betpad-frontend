export interface IButtonsListScss {
  'container': string;
  'startAlign': string;
  'endAlign': string;
  'centerAlign': string;
  'betweenAlign': string;
  'evenlyAlign': string;
  'aroundAlign': string;
  'button': string;
  'buttonIsBlock': string;
}

export const locals: IButtonsListScss;
