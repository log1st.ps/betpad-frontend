import {
    BUTTONS_LIST_JUSTIFY_AROUND,
    BUTTONS_LIST_JUSTIFY_BETWEEN,
    BUTTONS_LIST_JUSTIFY_EVENLY,
} from '@betpad/types/assets/components/buttonsList/buttonsListAlignsConstants'
import { IButtonsListComponent } from '@betpad/types/assets/components/buttonsList/IButtonsListComponent'
import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import Button from '../../elements/Button/ButtonElement'
import { IButtonsListScss } from './buttonsList.scss'

const styles: IButtonsListScss = require('./buttonsList.scss')

const ButtonsListComponent = ({
    buttons,
    forwardedRef,
    align,
}: IButtonsListComponent) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles[`${align}Align`]]: align,
            },
        )}
    >
        {buttons.map(({key, ...button}: any, i: number) => (
            <div
                key={key}
                className={cn(
                    styles.button,
                    {
                        [styles.buttonIsBlock]: button.isBlock || false,
                    },
                )}
            >
                <Button
                    hasStackFromRight={
                        (!align
                            || [BUTTONS_LIST_JUSTIFY_AROUND, BUTTONS_LIST_JUSTIFY_EVENLY, BUTTONS_LIST_JUSTIFY_BETWEEN]
                                .indexOf(align) === -1)
                        && i < buttons.length - 1
                    }
                    hasStackFromLeft={
                        (!align ||
                            [BUTTONS_LIST_JUSTIFY_AROUND, BUTTONS_LIST_JUSTIFY_EVENLY, BUTTONS_LIST_JUSTIFY_BETWEEN]
                                .indexOf(align) === -1)
                        && i > 0
                    }
                    {...button}
                />
            </div>
        ))}
    </div>
)

const ButtonsList: React.ComponentClass<IButtonsListComponent> = compose<any, any>(

)(ButtonsListComponent)

export default ButtonsList