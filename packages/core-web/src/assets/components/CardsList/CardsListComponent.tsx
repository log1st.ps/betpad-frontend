import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import { ICardsListComponent } from '@betpad/types/assets/components/cardsList/ICardsListComponent'
import Card from '../Card/CardComponent'
import { ICardsListScss } from './cardsList.scss'

const styles: ICardsListScss = require('./cardsList.scss')

const CardsListComponent = ({
    cards,
    align,
    forwardedRef,
}: ICardsListComponent) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            styles[`${align}Align`],
        )}
    >
        {cards.map(({key, width, ...card}) => (
            <div
                key={key}
                style={{
                    minWidth: width,
                }}
                className={styles.card}
            >
                <Card {...card}/>
            </div>
        ))}
    </div>
)

const CardsList: React.ComponentClass<ICardsListComponent> = compose<any, any>(
    
)(CardsListComponent)

export default CardsList