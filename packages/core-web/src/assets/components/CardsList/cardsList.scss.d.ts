export interface ICardsListScss {
  'container': string;
  'leftAlign': string;
  'centerAlign': string;
  'rightAlign': string;
  'card': string;
}

export const locals: ICardsListScss;
