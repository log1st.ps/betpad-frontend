import * as React from 'react'
import { withProps } from 'recompose'
import compose from 'recompose/compose'

import { ITabsListComponent } from '@betpad/types/assets/components/tabsList/ITabsListComponent'
import {
    BUTTON_STATE_ACTIVE,
    BUTTON_STATE_TAB,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import Button from '../../elements/Button/ButtonElement'
import { ITabsListScss } from './tabsList.scss'

const styles: ITabsListScss = require('./tabsList.scss')

interface IConnectedTabsListComponent extends ITabsListComponent {
    onTabClick(value: string): () => void,
}

const TabsListComponent = ({
    items,
    onTabClick,
    forwardedRef,
    value,
}: IConnectedTabsListComponent) => (
    <div
        className={styles.container}
    >
        {items.map(({value: tabValue, label, isDisabled}: any) => (
            <div
                className={styles.item}
                key={tabValue}
            >
                <Button
                    text={label}
                    onClick={onTabClick(tabValue as string)}
                    state={
                        value === tabValue
                            ? [BUTTON_STATE_ACTIVE, BUTTON_STATE_TAB]
                            : BUTTON_STATE_TAB
                    }
                    isDisabled={isDisabled}
                />
            </div>
        ))}
    </div>
)

const TabsList: React.ComponentClass<ITabsListComponent> = compose<any, any>(
    withProps(({onClick}) => ({
        onTabClick: (value: string) => () => {
            if (onClick) {
                onClick(value)
            }
        },
    })),

)(TabsListComponent)

export default TabsList