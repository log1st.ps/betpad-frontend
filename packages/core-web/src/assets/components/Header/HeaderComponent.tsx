import { HEADER_MENU_ALIGN_LEFT } from '@betpad/types/assets/components/header/headerMenuAlignsConstants'
import {
    IHeaderComponent,
    IHeaderMenuItem,
} from '@betpad/types/assets/components/header/IHeaderComponent'
import {
    AVATAR_SIZE_XS,
} from '@betpad/types/assets/elements/avatar/avatarSizesConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_60,
    BUTTON_STATE_INACTIVE_OPACITY_80,
    BUTTON_STATE_PURE,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    ICON_CARET_DOWN,
} from '@betpad/types/assets/elements/icon/iconsConstants'
import {
    ICON_SIZE_LT,
} from '@betpad/types/assets/elements/icon/iconSizesConstants'
import { ICON_STATE_INHERIT } from '@betpad/types/assets/elements/icon/iconStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
    COLOR_GREEN,
} from '@betpad/types/core/colors'
import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import Avatar from '../../elements/Avatar/AvatarElement'
import Button from '../../elements/Button/ButtonElement'
import Icon from '../../elements/Icon/IconElement'
import Logo from '../../elements/Logo/LogoElement'
import ButtonsList from '../ButtonsList/ButtonsListComponent'
import {
    SecondarySubHeading,
    SenarySubHeading,
} from '../Typography/TypographyComponent'
import {
    BUTTON_TYPE_BUTTON,
    BUTTON_TYPE_LINK,
} from '@betpad/types/assets/elements/button/buttonTypesConstants'
import { BUTTON_SIZE_SM } from '@betpad/types/assets/elements/button/buttonSizesConstants'

const styles = require('./header.scss')

const HeaderComponent = ({
    menuItems,
    userInfo,
    menuAlign,
    primaryButton,
    onLogoClick,
    forwardedRef,
}: IHeaderComponent) => (
    <div
        ref={forwardedRef}
        className={styles.header}
    >
        <div className={styles.logo}>
            <Button
                onClick={onLogoClick}
                text={(
                    <Logo/>
                )}
            />
        </div>
        {menuItems && (
            <div
                className={cn(
                    styles.menu,
                    styles[`${menuAlign}MenuAlign`],
                )}
            >
                <ButtonsList
                    buttons={menuItems.map(({
                        key,
                        url,
                        text,
                        isDisabled,
                        onClick,
                        isActive,
                        isBig,
                    }: IHeaderMenuItem) => ({
                        key,
                        url,
                        type: (url ? BUTTON_TYPE_LINK : BUTTON_TYPE_BUTTON) as any,
                        text: () => (
                            <div className={styles.menuItem}>
                                {isBig
                                    ? (<SecondarySubHeading content={text}/>)
                                    : (<SenarySubHeading content={text}/>)
                                }
                            </div>
                        ),
                        isDisabled,
                        onClick,
                        color: (isActive ? COLOR_GREEN : COLOR_GRAY_900) as any,
                        state: !isActive ? BUTTON_STATE_INACTIVE_OPACITY_60 as any : BUTTON_STATE_PURE,
                        size: BUTTON_SIZE_SM as any,
                    }))}
                />
            </div>
        )}
        {primaryButton && (
            <div className={styles.primaryButton}>
                <Button {...primaryButton}/>
            </div>
        )}
        {userInfo && (
            <div className={styles.userInfo}>
                <Button
                    color={COLOR_GRAY_700}
                    renderLeft={
                        userInfo.avatar
                            ? (
                                <Avatar
                                    name={userInfo.name as string}
                                    url={userInfo.avatar as string}
                                    size={AVATAR_SIZE_XS}
                                />
                            )
                            : undefined
                    }
                    text={
                        <SenarySubHeading color={COLOR_GRAY_900} content={userInfo.name}/>
                    }
                    renderRight={
                        <Icon
                            icon={ICON_CARET_DOWN}
                            size={ICON_SIZE_LT}
                            isSquare={true}
                            state={ICON_STATE_INHERIT}
                        />
                    }
                    state={BUTTON_STATE_INACTIVE_OPACITY_80}
                />
            </div>
        )}
    </div>
)

HeaderComponent.defaultProps = {
    menuAlign: HEADER_MENU_ALIGN_LEFT,
}

const Header: React.ComponentClass<IHeaderComponent> = compose<any, any>(

)(HeaderComponent)

export default Header