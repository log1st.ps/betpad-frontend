export interface IHeaderScss {
  'header': string;
  'logo': string;
  'primaryButton': string;
  'userInfo': string;
  'menu': string;
  'leftMenuAlign': string;
  'centerMenuAlign': string;
  'rightMenuAlign': string;
  'menuItem': string;
}

export const locals: IHeaderScss;
