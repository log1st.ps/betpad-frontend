export interface IResultPageLayoutScss {
  'layout': string;
  'container': string;
  'content': string;
  'title': string;
  'subTitle': string;
  'cta': string;
}

export const locals: IResultPageLayoutScss;
