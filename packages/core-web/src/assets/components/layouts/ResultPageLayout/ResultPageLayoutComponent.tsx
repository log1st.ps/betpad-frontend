import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { IResultPageLayoutComponent } from '@betpad/types/assets/components/layouts/IResultPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { IResultPageLayoutScss } from './resultPageLayout.scss'

const styles: IResultPageLayoutScss = require('./resultPageLayout.scss')

const ResultPageLayoutComponent = ({
    renderCTA,
    renderSubTitle,
    renderTitle,
    forwardedRef,
}: IResultPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {(renderTitle || renderSubTitle) && (
                <div className={styles.content}>
                    {renderTitle && (
                        <div className={styles.title}>
                            {renderIRenderable(renderTitle)}
                        </div>
                    )}
                    {renderSubTitle && (
                        <div className={styles.subTitle}>
                            {renderIRenderable(renderSubTitle)}
                        </div>
                    )}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
        </div>
    </div>
)

const ResultPageLayout: React.ComponentClass<IResultPageLayoutComponent> = compose<any, any>(

)(ResultPageLayoutComponent)

export default ResultPageLayout