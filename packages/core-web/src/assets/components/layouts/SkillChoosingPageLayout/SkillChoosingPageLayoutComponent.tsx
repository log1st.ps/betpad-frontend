import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { ISkillChoosingPageLayoutComponent } from '@betpad/types/assets/components/layouts/ISkillChoosingPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { ISkillChoosingPageLayoutScss } from './skillChoosingPageLayout.scss'

const styles: ISkillChoosingPageLayoutScss = require('./skillChoosingPageLayout.scss')


const SkillChoosingPageLayoutComponent = ({
    renderValues,
    renderCTA,
    renderSubTitle,
    renderTitle,
    renderStep,
    forwardedRef,
}: ISkillChoosingPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderValues && (
                <div className={styles.values}>
                    {renderIRenderable(renderValues)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
            {renderStep && (
                <div className={styles.step}>
                    {renderIRenderable(renderStep)}
                </div>
            )}
        </div>
    </div>
)

const SkillChoosingPageLayout: React.ComponentClass<ISkillChoosingPageLayoutComponent> = compose<any, any>(

)(SkillChoosingPageLayoutComponent)

export default SkillChoosingPageLayout