export interface ISkillChoosingPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'values': string;
  'cta': string;
  'step': string;
}

export const locals: ISkillChoosingPageLayoutScss;
