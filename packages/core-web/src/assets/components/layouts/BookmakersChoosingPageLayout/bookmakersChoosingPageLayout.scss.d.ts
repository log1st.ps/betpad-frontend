export interface IBookmakersChoosingPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'bookmakers': string;
  'cta': string;
  'step': string;
}

export const locals: IBookmakersChoosingPageLayoutScss;
