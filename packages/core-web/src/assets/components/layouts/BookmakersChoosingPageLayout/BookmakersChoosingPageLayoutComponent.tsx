import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { IBookmakersChoosingPageLayoutComponent } from '@betpad/types/assets/components/layouts/IBookmakersChoosingPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { IBookmakersChoosingPageLayoutScss } from './bookmakersChoosingPageLayout.scss'

const styles: IBookmakersChoosingPageLayoutScss = require('./bookmakersChoosingPageLayout.scss')


const BookmakersChoosingPageLayoutComponent = ({
    renderBookmakers,
    renderCTA,
    renderSubTitle,
    renderTitle,
    renderStep,
    forwardedRef,
}: IBookmakersChoosingPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderBookmakers && (
                <div className={styles.bookmakers}>
                    {renderIRenderable(renderBookmakers)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
            {renderStep && (
                <div className={styles.step}>
                    {renderIRenderable(renderStep)}
                </div>
            )}
        </div>
    </div>
)

const BookmakersChoosingPageLayout: React.ComponentClass<IBookmakersChoosingPageLayoutComponent> = compose<any, any>(

)(BookmakersChoosingPageLayoutComponent)

export default BookmakersChoosingPageLayout