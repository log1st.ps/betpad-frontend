import { IMainLayoutComponent } from '@betpad/types/assets/components/layouts/IMainLayoutComponent'
import cn from 'classnames'
import * as React from 'react'
import { createElement } from 'react'
import compose from 'recompose/compose'
import withTheme from '../../../providers/ThemeProvider/withTheme'
import { IMainLayoutScss } from './mainLayout.scss'

const styles : IMainLayoutScss = require('./mainLayout.scss')

const MainLayoutComponent = ({
    children,
    theme,
    forwardedRef,
    isHeaderShadowed,
    renderHeader,
} : IMainLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.layout,
            styles[`${theme}Layout`],
            {
                [styles.isHeaderShadowed]: isHeaderShadowed,
            },
        )}
    >
        {renderHeader && (
            <div
                className={styles.header}
            >
                <div className={styles.headerContent}>
                    {typeof renderHeader === 'function' ? createElement(renderHeader as any) : renderHeader}
                </div>
            </div>
        )}
        <div className={styles.contentWrapper}>
            <div className={styles.content}>
                <div className={styles.contentScroll}>
                    {typeof children === 'function' ? createElement(children as any) : children}
                </div>
            </div>
        </div>
    </div>
)

const MainLayout: React.ComponentClass<IMainLayoutComponent> = compose<any, any>(
    withTheme,
)(MainLayoutComponent)

export default MainLayout