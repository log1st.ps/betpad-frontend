export interface IMainLayoutScss {
  'layout': string;
  'lightLayout': string;
  'darkLayout': string;
  'header': string;
  'contentWrapper': string;
  'contentScroll': string;
  'isHeaderShadowed': string;
  'headerContent': string;
  'content': string;
}

export const locals: IMainLayoutScss;
