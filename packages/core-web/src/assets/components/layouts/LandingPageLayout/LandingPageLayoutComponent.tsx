import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { ILandingPageLayoutComponent } from '@betpad/types/assets/components/layouts/ILandingPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { ILandingPageLayoutScss } from './landingPageLayout.scss'

const styles: ILandingPageLayoutScss = require('./landingPageLayout.scss')

const LandingPageLayoutComponent = ({
    renderCategories,
    renderCTA,
    renderSubTitle,
    renderTitle,
    forwardedRef,
}: ILandingPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderCategories && (
                <div className={styles.categories}>
                    {renderIRenderable(renderCategories)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
        </div>
    </div>
)

const LandingPageLayout: React.ComponentClass<ILandingPageLayoutComponent> = compose<any, any>(

)(LandingPageLayoutComponent)

export default LandingPageLayout