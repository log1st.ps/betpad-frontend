export interface ILandingPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'categories': string;
  'cta': string;
}

export const locals: ILandingPageLayoutScss;
