export interface ISignInPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'form': string;
  'cta': string;
  'subActions': string;
}

export const locals: ISignInPageLayoutScss;
