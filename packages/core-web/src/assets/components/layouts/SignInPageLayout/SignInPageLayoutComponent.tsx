import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { ISignInPageLayoutComponent } from '@betpad/types/assets/components/layouts/ISignInPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { ISignInPageLayoutScss } from './signInPageLayout.scss'

const styles: ISignInPageLayoutScss = require('./signInPageLayout.scss')

const SignInPageLayoutComponent = ({
    renderCTA,
    renderForm,
    renderTitle,
    renderSubActions,
    forwardedRef,
}: ISignInPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderForm && (
                <div className={styles.form}>
                    {renderIRenderable(renderForm)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
            {renderSubActions && (
                <div className={styles.subActions}>
                    {renderIRenderable(renderSubActions)}
                </div>
            )}
        </div>
    </div>
)

const SignInPageLayout: React.ComponentClass<ISignInPageLayoutComponent> = compose<any, any>(

)(SignInPageLayoutComponent)

export default SignInPageLayout