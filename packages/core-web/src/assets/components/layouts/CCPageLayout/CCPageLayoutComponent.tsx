import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import * as React from 'react'
import compose from 'recompose/compose'
import { ICCPageLayoutComponent } from '@betpad/types/assets/components/layouts/ICCPageLayoutComponent'
import { ICCPageLayoutScss } from './cCPageLayout.scss'

const styles: ICCPageLayoutScss = require('./cCPageLayout.scss')

const CCPageLayoutComponent = ({
    renderForm,
    renderCTA,
    renderSubTitle,
    renderTitle,
    forwardedRef,
}: ICCPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderForm && (
                <div className={styles.form}>
                    {renderIRenderable(renderForm)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
        </div>
    </div>
)

const CCPageLayout: React.ComponentClass<ICCPageLayoutComponent> = compose<any, any>(

)(CCPageLayoutComponent)

export default CCPageLayout