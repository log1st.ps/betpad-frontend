export interface ICCPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'form': string;
  'cta': string;
}

export const locals: ICCPageLayoutScss;
