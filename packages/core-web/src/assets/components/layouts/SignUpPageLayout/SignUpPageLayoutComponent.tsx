import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { ISignUpPageLayoutComponent } from '@betpad/types/assets/components/layouts/ISignUpPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { ISignUpPageLayoutScss } from './signUpPageLayout.scss'

const styles: ISignUpPageLayoutScss = require('./signUpPageLayout.scss')

const SignUpPageLayoutComponent = ({
    renderCTA,
    renderForm,
    renderTitle,
    renderSubActions,
    forwardedRef,
}: ISignUpPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderForm && (
                <div className={styles.form}>
                    {renderIRenderable(renderForm)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
            {renderSubActions && (
                <div className={styles.subActions}>
                    {renderIRenderable(renderSubActions)}
                </div>
            )}
        </div>
    </div>
)

const SignUpPageLayout: React.ComponentClass<ISignUpPageLayoutComponent> = compose<any, any>(

)(SignUpPageLayoutComponent)

export default SignUpPageLayout