import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import * as React from 'react'
import compose from 'recompose/compose'
import { ITariffsPageLayoutComponent } from '../../../../../../types/assets/components/layouts/ITariffsPageLayoutComponent'
import { ITariffsPageLayoutScss } from './tariffsPageLayout.scss'

const styles: ITariffsPageLayoutScss = require('./tariffsPageLayout.scss')

const TariffsPageLayoutComponent = ({
    renderForm,
    renderCTA,
    renderSubTitle,
    renderTitle,
    renderTariffs,
    forwardedRef,
}: ITariffsPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderTariffs && (
                <div className={styles.tariffs}>
                    {renderIRenderable(renderTariffs)}
                </div>
            )}
            {renderForm && (
                <div className={styles.form}>
                    {renderIRenderable(renderForm)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
        </div>
    </div>
)

const TariffsPageLayout: React.ComponentClass<ITariffsPageLayoutComponent> = compose<any, any>(

)(TariffsPageLayoutComponent)

export default TariffsPageLayout