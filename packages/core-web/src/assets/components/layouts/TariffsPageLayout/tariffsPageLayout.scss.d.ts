export interface ITariffsPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'tariffs': string;
  'form': string;
  'cta': string;
}

export const locals: ITariffsPageLayoutScss;
