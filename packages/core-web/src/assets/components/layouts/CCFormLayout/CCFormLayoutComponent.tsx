import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { ICCFormLayoutComponent } from '@betpad/types/assets/components/layouts/ICCFormLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import Field from '../../../elements/controls/Field/FieldElement'
import { ICCFormLayoutScss } from './cCFormLayout.scss'

const styles: ICCFormLayoutScss = require('./cCFormLayout.scss')

const CCFormLayoutComponent = ({
    forwardedRef,
    renderCVCField,
    expirationLabel,
    renderExpirationMonthField,
    renderExpirationYearField,
    renderNameField,
    renderNumberField,
}: ICCFormLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.container}
    >
        {renderNameField && (
            <div className={styles.name}>
                {renderIRenderable(renderNameField)}
            </div>
        )}
        {renderNumberField && (
            <div className={styles.number}>
                {renderIRenderable(renderNumberField)}
            </div>
        )}
        {(renderExpirationMonthField || renderExpirationYearField) && (
            <div className={styles.expirationWrapper}>
                <Field label={expirationLabel}>
                    <div className={styles.expiration}>
                        {renderExpirationMonthField && (
                            <div className={styles.month}>
                                {renderIRenderable(renderExpirationMonthField)}
                            </div>
                        )}
                        {renderExpirationYearField && (
                            <div className={styles.year}>
                                {renderIRenderable(renderExpirationYearField)}
                            </div>
                        )}
                    </div>
                </Field>
            </div>
        )}
        {renderCVCField && (
            <div className={styles.cvc}>
                {renderIRenderable(renderCVCField)}
            </div>
        )}
    </div>
)

const CCFormLayout: React.ComponentClass<ICCFormLayoutComponent> = compose<any, any>(

)(CCFormLayoutComponent)

export default CCFormLayout