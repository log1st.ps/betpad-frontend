export interface ICCFormLayoutScss {
  'container': string;
  'name': string;
  'number': string;
  'expirationWrapper': string;
  'cvc': string;
  'expiration': string;
  'month': string;
  'year': string;
}

export const locals: ICCFormLayoutScss;
