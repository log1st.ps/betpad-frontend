export interface IBalancesSettingPageLayoutScss {
  'layout': string;
  'container': string;
  'title': string;
  'subTitle': string;
  'values': string;
  'cta': string;
  'step': string;
}

export const locals: IBalancesSettingPageLayoutScss;
