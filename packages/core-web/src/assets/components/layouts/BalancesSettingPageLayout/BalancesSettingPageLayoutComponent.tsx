import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { IBalancesSettingPageLayoutComponent } from '@betpad/types/assets/components/layouts/IBalancesSettingPageLayoutComponent'
import * as React from 'react'
import compose from 'recompose/compose'
import { IBalancesSettingPageLayoutScss } from './balancesSettingPageLayout.scss'

const styles: IBalancesSettingPageLayoutScss = require('./balancesSettingPageLayout.scss')


const BalancesSettingPageLayoutComponent = ({
    renderValues,
    renderCTA,
    renderSubTitle,
    renderTitle,
    renderStep,
    forwardedRef,
}: IBalancesSettingPageLayoutComponent) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {renderTitle && (
                <div className={styles.title}>
                    {renderIRenderable(renderTitle)}
                </div>
            )}
            {renderSubTitle && (
                <div className={styles.subTitle}>
                    {renderIRenderable(renderSubTitle)}
                </div>
            )}
            {renderValues && (
                <div className={styles.values}>
                    {renderIRenderable(renderValues)}
                </div>
            )}
            {renderCTA && (
                <div className={styles.cta}>
                    {renderIRenderable(renderCTA)}
                </div>
            )}
            {renderStep && (
                <div className={styles.step}>
                    {renderIRenderable(renderStep)}
                </div>
            )}
        </div>
    </div>
)

const BalancesSettingPageLayout: React.ComponentClass<IBalancesSettingPageLayoutComponent> = compose<any, any>(

)(BalancesSettingPageLayoutComponent)

export default BalancesSettingPageLayout