export interface IListingPageLayoutScss {
  'layout': string;
  'container': string;
  'top': string;
  'mode': string;
  'prefilters': string;
  'datepicker': string;
  'categories': string;
  'main': string;
  'filter': string;
  'content': string;
  'tabs': string;
  'table': string;
}

export const locals: IListingPageLayoutScss;
