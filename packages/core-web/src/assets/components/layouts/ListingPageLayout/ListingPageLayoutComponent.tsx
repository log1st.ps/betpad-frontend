import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { IListingPageLayout } from '@betpad/types/assets/components/layouts/IListingPageLayout'
import * as React from 'react'
import compose from 'recompose/compose'
import { IListingPageLayoutScss } from './listingPageLayout.scss'

const styles: IListingPageLayoutScss = require('./listingPageLayout.scss')

const ListingPageLayoutComponent = ({
    renderCategories,
    renderDatepicker,
    renderFilters,
    renderMode,
    renderTable,
    renderTabs,
    forwardedRef,
}: IListingPageLayout) => (
    <div
        ref={forwardedRef}
        className={styles.layout}
    >
        <div className={styles.container}>
            {(renderMode || renderDatepicker || renderCategories) && (
                <div className={styles.top}>
                    {renderMode && (
                        <div className={styles.mode}>
                            {renderIRenderable(renderMode)}
                        </div>
                    )}
                    {(renderDatepicker || renderCategories) && (
                        <div className={styles.prefilters}>
                            {renderDatepicker && (
                                <div className={styles.datepicker}>
                                    {renderIRenderable(renderDatepicker)}
                                </div>
                            )}
                            {renderCategories && (
                                <div className={styles.categories}>
                                    {renderIRenderable(renderCategories)}
                                </div>
                            )}
                        </div>
                    )}
                </div>
            )}
            {(renderFilters || renderTable || renderTabs) && (
                <div className={styles.main}>
                    {renderFilters && (
                        <div className={styles.filter}>
                            {renderIRenderable(renderFilters)}
                        </div>
                    )}
                    {(renderTabs || renderTable) && (
                        <div className={styles.content}>
                            {renderTabs && (
                                <div className={styles.tabs}>
                                    {renderIRenderable(renderTabs)}
                                </div>
                            )}
                            {renderTable && (
                                <div className={styles.table}>
                                    {renderIRenderable(renderTable)}
                                </div>
                            )}
                        </div>
                    )}
                </div>
            )}
        </div>
    </div>
)

const ListingPageLayout: React.ComponentClass<IListingPageLayout> = compose<any, any>(

)(ListingPageLayoutComponent)

export default ListingPageLayout