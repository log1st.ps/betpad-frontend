export interface IImageScss {
  'divType': string;
  'initialBackgroundType': string;
  'stretchBackgroundType': string;
  'coverBackgroundType': string;
  'containBackgroundType': string;
}

export const locals: IImageScss;
