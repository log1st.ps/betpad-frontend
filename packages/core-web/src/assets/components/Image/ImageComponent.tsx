import { IImageComponent } from '@betpad/types/assets/components/image/IImageComponent'
import {
    IMAGE_TYPE_BLOCK,
    IMAGE_TYPE_IMG,
} from '@betpad/types/assets/components/image/imageTypesConstants'
import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import { IImageScss } from './image.scss'

const styles: IImageScss = require('./image.scss')

const ImageComponent = ({
    url,
    alt,
    type,
    backgroundType,
    height,
    width,
    forwardedRef,
}: IImageComponent) => {
    const Tag = type as any

    return (
        <Tag
            ref={forwardedRef}
            className={cn(
                styles[`${type}Type`],
                styles[`${backgroundType}BackgroundType`],
            )}
            src={type === IMAGE_TYPE_IMG ? url : undefined}
            style={{
                height,
                width,
                backgroundImage: type === IMAGE_TYPE_BLOCK ? `url(${url})` : undefined,
            }}
            alt={type === IMAGE_TYPE_IMG ? alt : undefined}
        />
    )
}

const Image: React.ComponentClass<IImageComponent> = compose<any, any>(
)(ImageComponent)

Image.defaultProps = {
    type: IMAGE_TYPE_IMG,
}

export default Image