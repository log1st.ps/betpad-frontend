import * as React from 'react'
import compose from 'recompose/compose'

import { ILogoElement } from '../../../../../types/assets/elements/logo/ILogoElement'
import { ILogoScss } from './logo.scss'

const styles: ILogoScss = require('./logo.scss')

const LogoSvg = require('./logo.inline.svg')

const LogoElement = ({
    forwardedRef,
}: ILogoElement) => (
    <LogoSvg
        ref={forwardedRef}
        className={styles.logo}
    />
)

const Logo: React.ComponentClass<ILogoElement> = compose<any, any>(

)(LogoElement)

export default Logo