export interface IAvatarScss {
  'avatar': string;
  'ltSize': string;
  'xsSize': string;
  'smSize': string;
  'mdSize': string;
  'lgSize': string;
  'xlSize': string;
}

export const locals: IAvatarScss;
