import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'

import { IMAGE_BACKGROUND_TYPE_COVER } from '../../../../../types/assets/components/image/imageBackgroundTypesConstants'
import { IMAGE_BOUND_STRETCH } from '../../../../../types/assets/components/image/imageBoundsConstants'
import { IMAGE_TYPE_BLOCK } from '../../../../../types/assets/components/image/imageTypesConstants'
import {
    AVATAR_SIZE_SM,
} from '../../../../../types/assets/elements/avatar/avatarSizesConstants'
import { IAvatarElement } from '../../../../../types/assets/elements/avatar/IAvatarElement'
import Image from '../../components/Image/ImageComponent'
import { IAvatarScss } from './avatar.scss'

const styles: IAvatarScss = require('./avatar.scss')

interface IConnectedAvatarElement extends IAvatarElement {
    computedSize: string,
}

const AvatarElement = ({
    name,
    url,
    size,
    forwardedRef,
}: IConnectedAvatarElement) => (
    <div
        ref={forwardedRef}
        className={
            cn(
                styles.avatar,
                styles[`${size}Size`],
            )
        }
    >
        <Image
            url={url as any}
            alt={name}
            height={IMAGE_BOUND_STRETCH}
            width={IMAGE_BOUND_STRETCH}
            type={IMAGE_TYPE_BLOCK}
            backgroundType={IMAGE_BACKGROUND_TYPE_COVER}
        />
    </div>
)

AvatarElement.defaultProps = {
    size: AVATAR_SIZE_SM,
}

const Avatar: React.ComponentClass<IAvatarElement> = compose<any, any>(

)(AvatarElement)

export default Avatar