import { IIconElement } from '@betpad/types/assets/elements/icon/IIconElement'
import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import { IIconScss } from './icon.scss'
const styles : IIconScss = require('./icon.scss')

const getIconSource = (type : string) => require(`./iconFiles/${type}.inline.svg`)

const IconElement = ({
    icon,
    size,
    state,
    isSquare,
    forwardedRef,
}: IIconElement) => {
    const IconSource = getIconSource(icon)

    return (
        <div
            ref={forwardedRef}
            className={cn(
                styles.icon,
                styles[`${size}Size`],
                styles[`${state}State`],
                {
                    [styles.isSquare]: isSquare,
                },
            )}
        >
            <IconSource
                className={styles.source}
                {...{...IconSource.defaultProps}}
            />
        </div>
    )
}

const Icon: React.ComponentClass<IIconElement> = compose<any, any>(

)(IconElement)

Icon.defaultProps = {
    isSquare: false,
}

export default Icon
