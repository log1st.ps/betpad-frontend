export interface IIconScss {
  'icon': string;
  'source': string;
  'isSquare': string;
  'stretchSize': string;
  'ltSize': string;
  'xsSize': string;
  'smSize': string;
  'mdSize': string;
  'lgSize': string;
  'xlSize': string;
  'inheritState': string;
}

export const locals: IIconScss;
