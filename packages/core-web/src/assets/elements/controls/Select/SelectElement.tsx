import { BUTTON_STATE_INACTIVE_OPACITY_60 } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { ISelectElement } from '@betpad/types/assets/elements/controls/select/ISelectElement'
import {
    ICON_ANGLE_BOTTOM,
} from '@betpad/types/assets/elements/icon/iconsConstants'
import { ICON_SIZE_LT } from '@betpad/types/assets/elements/icon/iconSizesConstants'
import { ICON_STATE_INHERIT } from '@betpad/types/assets/elements/icon/iconStatesConstants'
import {
    COLOR_BLUE,
} from '@betpad/types/core/colors'
import { IRenderable } from '@betpad/types/core/IRenderable'
import {
    POPUP_ALIGN_START,
    POPUP_POSITION_BOTTOM,
} from '@betpad/types/hocs/IWithPopup'
import cn from 'classnames'
import * as React from 'react'
import {
    createRef,
    RefObject,
} from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import withForwardedRef from '../../../../../../helpers/hocs/withForwardedRef'
import withPopup from '../../../../../../helpers/hocs/withPopup'
import Button from '../../Button/ButtonElement'
import Icon from '../../Icon/IconElement'
import { ISelectScss } from './select.scss'
import { BUTTON_ALIGN_START } from '@betpad/types/assets/elements/button/buttonAlignsConstants'

const styles: ISelectScss = require('./select.scss')

interface IConnectedSelectElement extends ISelectElement {
    isFocused?: boolean,
    containerRef: RefObject<any>,
    valueText: string,
    setIsFocused(value: boolean): void,
    onOptionClick(value: string): () => void,
    onFocus(): void,
    onClick(): void,
    onBlur(): void,
}

const SelectElement = ({
    value,
    values,
    hasInputAfter,
    hasInputBefore,
    isDisabled,
    forwardedRef,
    isFocused,
    onChange,
    placeholder,
    onFocus,
    containerRef,
    onClick,
    onBlur,
    valueText,
}: IConnectedSelectElement) => (
    <div
        ref={forwardedRef}
    >
        <div
            ref={containerRef}
            className={cn(
                styles.select,
                {
                    [styles.isDisabled]: isDisabled,
                    [styles.isFocused]: isFocused,
                    [styles.hasInputAfter]: hasInputAfter,
                    [styles.hasInputBefore]: hasInputBefore,
                },
            )}
            tabIndex={0}
            onFocus={onFocus}
            onClick={onClick}
            onBlur={onBlur}
        >
            <div className={styles.content}>
                <div
                    className={cn(
                        styles.value,
                    )}
                >
                    {value && valueText}
                </div>
                {placeholder && !value && (
                    <div className={styles.placeholder}>
                        {placeholder}
                    </div>
                )}
            </div>
            <div
                className={cn(
                    styles.icon,
                    {
                        [styles.iconIsRotated]: isFocused,
                    },
                )}
            >
                <Icon
                    icon={ICON_ANGLE_BOTTOM}
                    size={ICON_SIZE_LT}
                    state={ICON_STATE_INHERIT}
                    isSquare={true}
                />
            </div>
        </div>
    </div>
)

const handlePopupWidth = (data: any) => {
    setTimeout(
        () => {
            const {
                instance: {
                    popper,
                },
                offsets: {
                    reference: {
                        width,
                    },
                },
            } = data

            if (popper) {
                popper.style.width = `${width}px`
            }
        },
        10,
    )
}

const Select: React.ComponentClass<ISelectElement> = compose<any, any>(
    withState('isFocused', 'setIsFocused', false),
    withProps(() => ({
        containerRef: createRef(),
    })),
    withProps(({onChange, setIsFocused, isDisabled}) => ({
        onOptionClick: (value: string) => () => {
            if (onChange && !isDisabled) {
                onChange(value)
            }

            setIsFocused(false)
        },
    })),
    withPopup({
        onUpdate: handlePopupWidth,
        onCreate: handlePopupWidth,
        renderContent({ zIndex, ...props }: any): IRenderable {
            const {
                values,
                isFocused,
                value,
                texts: {
                    noData: noDataText,
                },
                onOptionClick,
            } = props as IConnectedSelectElement
            
            return (
                <div
                    tabIndex={1}
                    className={cn(
                        styles.values,
                        {
                            [styles.valuesIsOpened]: isFocused,
                        },
                    )}
                >
                    <div className={styles.valuesList}>
                        {values && values.map(({value: valueValue, text}) => {
                            const isValueSelected = String(valueValue) === String(value)

                            return (
                                <Button
                                    key={valueValue}
                                    color={isValueSelected ? COLOR_BLUE : undefined}
                                    state={!isValueSelected ? BUTTON_STATE_INACTIVE_OPACITY_60 : undefined}
                                    text={() => (
                                        <div className={styles.option}>
                                            {text}
                                        </div>
                                    )}
                                    textAlign={BUTTON_ALIGN_START}
                                    onClick={onOptionClick(valueValue)}
                                    isBlock={true}
                                    tabIndex={-1}
                                />
                            )
                        })}
                        {!values && (
                            <div className={styles.valuesNoData}>
                                {noDataText}
                            </div>
                        )}
                    </div>
                </div>
            )
        },
        isFixed: true,
        position: POPUP_POSITION_BOTTOM,
        align: POPUP_ALIGN_START,
    }),
    withForwardedRef,
    withProps(({setIsFocused, popupRef, containerRef}) => ({
        onFocus() {
            setIsFocused(true)
        },
        onClick() {
            setIsFocused(true)
        },
        onBlur() {
            setTimeout(() => {
                const focusedElement = document.activeElement

                const isClickedInContainer
                    = containerRef && containerRef.current && containerRef.current.contains(focusedElement)
                const isClickedInPopup
                    = popupRef && popupRef.current && popupRef.current.contains(focusedElement)

                if (
                    !(isClickedInContainer || isClickedInPopup)
                ) {
                    setIsFocused(false)
                }
            })
        },
    })),
    withProps(({values, value}) => {
        let valueText

        if (value) {
            const filteredValues = values.filter(({value: key}: any) => String(key) === String(value))

            if (filteredValues.length > 0) {
                valueText = filteredValues[0].text
            }
        }

        return {
            valueText,
        }
    }),
)(SelectElement)

export default Select