export interface ISelectScss {
  'select': string;
  'isDisabled': string;
  'isFocused': string;
  'content': string;
  'value': string;
  'placeholder': string;
  'hasInputBefore': string;
  'hasInputAfter': string;
  'icon': string;
  'iconIsRotated': string;
  'options': string;
  'values': string;
  'valuesIsOpened': string;
  'valuesList': string;
  'option': string;
  'valuesNoData': string;
}

export const locals: ISelectScss;
