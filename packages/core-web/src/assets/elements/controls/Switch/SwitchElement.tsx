import {
    ISwitchElement,
    ISwitchValue,
} from '@betpad/types/assets/elements/controls/switch/ISwitchElement'
import cn from 'classnames'
import * as React from 'react'
import { withProps } from 'recompose'
import compose from 'recompose/compose'
import { ISwitchScss } from './switch.scss'


const styles: ISwitchScss = require('./switch.scss')

interface IConnectedSwitchElement extends ISwitchElement {
    onClick?(value: string): () => void
}

const SwitchElement = ({
    values,
    value,
    onClick,
}: IConnectedSwitchElement) => (
    <div className={styles.switch}>
        {values.map(({key, text}: ISwitchValue) => (
            <button
                key={key}
                onClick={onClick && onClick(key)}
                className={cn(
                    styles.value,
                    {
                        [styles.activeValue]: value === key,
                    },
                )}
            >
                {text}
            </button>
        ))}
    </div>
)

const Switch: React.ComponentClass<ISwitchElement> = compose<any, any>(
    withProps(({onChange}) => ({
        onClick: (value: string) => () => {
            onChange(value)
        },
    })),

)(SwitchElement)

export default Switch