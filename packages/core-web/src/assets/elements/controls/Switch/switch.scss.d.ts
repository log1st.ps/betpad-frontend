export interface ISwitchScss {
  'switch': string;
  'value': string;
  'activeValue': string;
}

export const locals: ISwitchScss;
