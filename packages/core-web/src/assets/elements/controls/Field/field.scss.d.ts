export interface IFieldScss {
  'container': string;
  'label': string;
  'inlineType': string;
  'children': string;
  'hint': string;
  'successState': string;
  'errorState': string;
}

export const locals: IFieldScss;
