import * as React from 'react'
import compose from 'recompose/compose'

import renderIRenderable from '@betpad/helpers/utilities/renderIRenderable'
import { FIELD_STATE_DEFAULT } from '@betpad/types/assets/elements/controls/field/fieldStatesConstants'
import {
    FIELD_TYPE_DEFAULT,
    FIELD_TYPE_INLINE,
} from '@betpad/types/assets/elements/controls/field/fieldTypesConstants'
import { IFieldElement } from '@betpad/types/assets/elements/controls/field/IFieldElement'
import cn from 'classnames'
import { SeptenarySubHeading } from '../../../components/Typography/TypographyComponent'
import { IFieldScss } from './field.scss'

const styles: IFieldScss = require('./field.scss')

const FieldElement = ({
    label,
    children,
    type,
    state,
    hint,
    forwardedRef,
}: IFieldElement) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles[`${type}Type`]]: !!type,
                [styles[`${state}State`]]: !!state,
            },
        )}
    >
        {label && (
            <div className={styles.label}>
                {type === FIELD_TYPE_DEFAULT && (
                    <SeptenarySubHeading content={label}/>
                )}
                {type === FIELD_TYPE_INLINE && (
                    renderIRenderable(label)
                )}
            </div>
        )}
        <div className={styles.children}>
            {children}
        </div>

        {hint && (
            <div className={styles.hint}>
                {hint}
            </div>
        )}
    </div>
)

FieldElement.defaultProps = {
    type: FIELD_TYPE_DEFAULT,
    state: FIELD_STATE_DEFAULT,
}

const Field: React.ComponentClass<IFieldElement> = compose<any, any>(

)(FieldElement)

export default Field