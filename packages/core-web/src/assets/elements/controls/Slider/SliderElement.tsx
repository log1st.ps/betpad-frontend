import * as React from 'react'
import {
    createRef,
    RefObject,
} from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'

import { ISliderElement } from '../../../../../../types/assets/elements/controls/slider/ISliderElement'
import { ISliderScss } from './slider.scss'

const styles: ISliderScss = require('./slider.scss')

interface IConnectedSliderElement extends ISliderElement {
    percentageValue: number | number[],
    handlersRefs: RefObject<any> | Array<RefObject<any>>,
    sliderRef: RefObject<any>,
    onDrag(valueIndex: number): () => void,
    onDragStart(): void,
}

const SliderElement = ({
    max,
    min,
    value,
    step,
    isRange,
    onChange,
    forwardedRef,
    percentageValue,
    onDrag,
    handlersRefs,
    sliderRef,
    onDragStart,
}: IConnectedSliderElement) => (
    <div
        className={styles.container}
        ref={forwardedRef}
    >
        <div
            ref={sliderRef}
            className={styles.background}
        >
            <div
                className={styles.backgroundValue}
                style={{
                    right: `${100 - (isRange ? percentageValue[0] : 100)}%`,
                    left: `${isRange ? 0 : percentageValue}%`,
                }}
            />
            {isRange && (
                <div
                    className={styles.backgroundValue}
                    style={{
                        right: `0%`,
                        left: `${percentageValue[1]}%`,
                    }}
                />
            )}
        </div>
        <div
            ref={isRange ? handlersRefs[0] : handlersRefs}
            style={{
                left: `${isRange ? percentageValue[0] : percentageValue}%`,
            }}
            className={styles.handler}
            draggable={true}
            onDragStart={onDragStart}
            onDrag={onDrag(0)}
            data-value={`${isRange ? value[0] : value}`}
        />
        {isRange && (
            <div
                style={{
                    left: `${isRange ? percentageValue[1] : percentageValue}%`,
                }}
                className={styles.handler}
                draggable={true}
                onDragStart={onDragStart}
                onDrag={onDrag(1)}
                ref={handlersRefs[1]}
                data-value={`${isRange ? value[1] : value}`}
            />
        )}
    </div>
)

const Slider: React.ComponentClass<ISliderElement> = compose<any, any>(
    withProps(({
        value,
        isRange,
        max,
        min,
    }: ISliderElement) => {
        const diff = (max - min)
        const multiplier = 100

        return {
            percentageValue: isRange
                ? [
                    multiplier * ((+value[0] - min) / diff),
                    multiplier * ((+value[1] - min) / diff),
                ] :
                multiplier * ((+value - min) / diff),
        }
    }),
    withProps(({isRange}: ISliderElement) => ({
        handlersRefs: isRange ? [createRef(), createRef()] : createRef(),
        sliderRef: createRef(),
    })),
    withProps(({handlersRefs, isRange, sliderRef, max, min, step, onChange, value}: IConnectedSliderElement) => ({
        onDragStart(e: DragEvent) {
            if (e.dataTransfer) {
                e.dataTransfer.setDragImage(document.createElement('img'), 0, 0)
            }
        },
        onDrag: (valueIndex: number) => ({pageX, target }: any) => {
            const handler = (isRange ? (handlersRefs[valueIndex]) : handlersRefs) as RefObject<any>

            if (
                handler
                && handler.current
                && sliderRef
                && sliderRef.current
            ) {
                const {
                    left: containerLeft,
                    width: containerWidth,
                } = sliderRef.current.getBoundingClientRect() as any

                const oneStep = step / (max - min) * 100
                const offset = (pageX - containerLeft) / containerWidth * 100

                const countOfSteps = Math.round(offset / oneStep)

                const newOffset = countOfSteps * oneStep

                if (
                    (isRange && (
                        (
                            valueIndex === 0
                            && newOffset >= 0
                            && newOffset <= value[1] * oneStep
                        )
                        ||
                        (
                            valueIndex === 1
                            && newOffset <= 100
                            && newOffset >= value[0] * oneStep
                        )
                    ))
                    || (!isRange && newOffset >= 0 && newOffset <= 100)
                ) {
                    const newValue = countOfSteps * step
                    if (+target.dataset.lastValue !== newValue) {
                        target.dataset.lastValue = newOffset
                        if (onChange) {
                            if (isRange && Array.isArray(value)) {
                                const val = [...value]
                                val[valueIndex] = countOfSteps * step
                                onChange(val)
                            }   else {
                                onChange(countOfSteps * step)
                            }
                        }
                    }
                }
            }
        },
    })),

)(SliderElement)

export default Slider