export interface ISliderScss {
  'container': string;
  'handler': string;
  'background': string;
  'backgroundValue': string;
}

export const locals: ISliderScss;
