import { IInputElement } from '@betpad/types/assets/elements/controls/input/IInputElement'
import cn from 'classnames'
import * as React from 'react'
import {
    createRef,
    RefObject,
} from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { IInputScss } from './input.scss'

const styles: IInputScss = require('./input.scss')

interface IConnectedInputElement extends IInputElement {
    isHovered: boolean,
    inputRef: RefObject<any>,
    onFocus(): void,
    onBlur(): void,
    onInputChange(): void,
}

const InputElement = ({
    value,
    placeholder,
    suffix,
    prefix,
    forwardedRef,
    isDisabled,
    onFocus,
    onBlur,
    onInputChange,
    isHovered,
    inputRef,
    hasInputAfter,
    hasInputBefore,
}: IConnectedInputElement) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles.isHovered]: isHovered,
                [styles.isDisabled]: isDisabled,
                [styles.hasInputBefore]: hasInputBefore,
                [styles.hasInputAfter]: hasInputAfter,
            },
        )}
        onMouseOver={onFocus}
        onMouseOut={onBlur}
    >
        {prefix && (
            <div className={styles.prefix}>{prefix}</div>
        )}
        <input
            ref={inputRef}
            className={styles.input}
            value={value}
            placeholder={placeholder}
            onChange={onInputChange}
            onFocus={onFocus}
            onBlur={onBlur}
            disabled={isDisabled}
        />
        {suffix && (
            <div className={styles.suffix}>{suffix}</div>
        )}
    </div>
)

const Input: React.ComponentClass<IInputElement> = compose<any, any>(
    withState('isHovered', 'setIsHovered', false),
    withProps(() => ({
        inputRef: createRef(),
    })),
    withProps(({setIsHovered, onChange, isDisabled, inputRef}) => ({
        onFocus() {
            if (!isDisabled) {
                setIsHovered(true)
            }
        },
        onBlur() {
            if (!isDisabled && inputRef.current !== document.activeElement) {
                setIsHovered(false)
            }
        },
        onInputChange({target}: {target: HTMLInputElement}) {
            if (!isDisabled && onChange) {
                onChange(target.value)
            }
        },
    })),

)(InputElement)

export default Input