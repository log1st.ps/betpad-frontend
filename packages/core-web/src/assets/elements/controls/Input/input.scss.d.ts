export interface IInputScss {
  'container': string;
  'input': string;
  'isHovered': string;
  'hasInputBefore': string;
  'hasInputAfter': string;
  'isDisabled': string;
  'prefix': string;
  'suffix': string;
}

export const locals: IInputScss;
