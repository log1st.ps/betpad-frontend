import cn from 'classnames'
import flatpickr from 'flatpickr'
import { unix, utc } from 'moment'
import {
    RefObject,
    Fragment,
} from 'react'
import * as React from 'react'
import { lifecycle } from 'recompose'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import withEvents from '../../../../../../helpers/hocs/withEvents'

import withPopup from '../../../../../../helpers/hocs/withPopup'
import { IDatepickerElement } from '../../../../../../types/assets/elements/controls/datepicker/IDatepickerElement'
import {
    ICON_CALENDAR,
    ICON_CALENDAR_MINI,
} from '../../../../../../types/assets/elements/icon/iconsConstants'
import {
    ICON_SIZE_LG,
    ICON_SIZE_XS,
} from '../../../../../../types/assets/elements/icon/iconSizesConstants'
import { IRenderable } from '../../../../../../types/core/IRenderable'
import {
    POPUP_ALIGN_START,
    POPUP_POSITION_BOTTOM,
} from '../../../../../../types/hocs/IWithPopup'
import {
    SecondarySubHeading,
    SenarySubHeading,
} from '../../../components/Typography/TypographyComponent'
import Icon from '../../Icon/IconElement'
import { IDatepickerScss } from './datepicker.scss'
import { ICON_STATE_INHERIT } from '../../../../../../types/assets/elements/icon/iconStatesConstants'
import withForwardedRef from '../../../../../../helpers/hocs/withForwardedRef'

const styles: IDatepickerScss = require('./datepicker.scss')

interface IConnectedDatepickerElement extends IDatepickerElement {
    textValue: string | string[],
    instance: any,
    popupRef: RefObject<any>,
    isActive: boolean,
    setInstance(instance: any): void,
    onClick(): void,
    setIsActive(value: boolean): void
}

const DatepickerElement = ({
    textValue,
    format,
    isRange,
    isDisabled,
    onChange,
    forwardedRef,
    onClick,
    hasInputAfter,
    hasInputBefore,
}: IConnectedDatepickerElement) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles.isDisabled]: isDisabled,
                [styles.isRange]: isRange,
                [styles.hasInputBefore]: isRange && hasInputBefore,
                [styles.hasInputAfter]: isRange && hasInputAfter,
            },
        )}
        onClick={!isDisabled ? onClick : undefined}
    >
        {isRange && (
            <Fragment>
                <div className={styles.rangeValue}>
                    <SenarySubHeading
                        content={textValue[0]}
                    />
                </div>
                <div className={styles.rangeValue}>
                    <SenarySubHeading
                        content={textValue[1]}
                    />
                </div>
                <div className={styles.rangeIcon}>
                    <Icon
                        icon={ICON_CALENDAR_MINI}
                        size={ICON_SIZE_XS}
                        state={ICON_STATE_INHERIT}
                    />
                </div>
            </Fragment>
        )}
        {!isRange && (
            <Fragment>
                <div className={styles.icon}>
                    <Icon
                        icon={ICON_CALENDAR}
                        size={ICON_SIZE_LG}
                        state={ICON_STATE_INHERIT}
                    />
                </div>
                <div className={styles.value}>
                    <SecondarySubHeading
                        content={textValue}
                    />
                </div>
            </Fragment>
        )}
    </div>
)

const Datepicker: React.ComponentClass<IDatepickerElement> = compose<any, any>(
    withState('instance', 'setInstance', null),
    withState('isActive', 'setIsActive', false),
    withProps(({
        value,
        isRange,
        format,
    }: {
        value: number | number[],
        format: string,
        isRange?: boolean,
    }) => ({
        textValue: isRange ? [
            unix(+value[0]).format(format),
            unix(+value[1]).format(format),
        ] : unix(+value).format(format),
    })),
    withProps(({setIsActive, isActive}: IConnectedDatepickerElement) => ({
        onClick: () => {
            setTimeout(() => {
                setIsActive(!isActive)
            })
        },
    })),
    withPopup({
        position: POPUP_POSITION_BOTTOM,
        align: POPUP_ALIGN_START,
        zStartIndex: 10,
        offsetPrimary: 10,
        isFixed: true,
        renderContent(): IRenderable {
            return <div/>
        },
    }),
    withForwardedRef,
    withEvents({
        targetKey: 'document',
        target: document,
        events: ['click'],
        handlers(getProps: () => IConnectedDatepickerElement): { [p: string]: (e: Event) => void } {
            return {
                onDocumentClick({target}: {target: EventTarget | null}) {
                    const {
                        setIsActive,
                        isActive,
                        popupRef,
                    } = getProps()

                    if (
                        target
                        && isActive
                        && popupRef.current
                        && !popupRef.current.contains(target)
                    ) {
                        setIsActive(false)
                    }
                },
            }
        },
    }),
    lifecycle({
        shouldComponentUpdate({isActive, instance}: IConnectedDatepickerElement) {
            const {
                calendarContainer,
            } = instance as any

            if (isActive && !calendarContainer.classList.contains('active')) {
                calendarContainer.classList.add('active')
            }
            if (!isActive && calendarContainer.classList.contains('active')) {
                calendarContainer.classList.remove('active')
            }

            return true
        },
        componentDidMount() {
            const {
                popupRef,
                setInstance,
                isRange,
                value,
                onChange,
            } = this.props as IConnectedDatepickerElement

            if (popupRef.current) {
                const defaultDates = isRange && Array.isArray(value) ? [
                    unix(value[0]).toDate(),
                    unix(value[1]).toDate(),
                ] : unix(value as number).toDate()
                const instance =
                    flatpickr(popupRef.current.childNodes[0], {
                        static: true,
                        mode: isRange ? 'range' : 'single',
                        defaultDate: defaultDates,
                        onDayCreate(
                            dObj: any,
                            dStr: any,
                            fp: any,
                            dayElem: HTMLDivElement,
                        ) {
                            dayElem.innerHTML = `<div class="dayValue">${dayElem.innerHTML}</div>`
                        },
                        onChange(selectedDates: Date[]) {
                            if (!onChange) {
                                return
                            }
                            const values = [
                                utc(selectedDates[0]).unix(),
                            ]
                            if (isRange) {
                                values[1] = utc(selectedDates[1]).unix()
                            }

                            onChange(isRange ? values : values[0])
                        },
                    })
                if (instance) {
                    setInstance(instance)
                }
            }
        },
        componentWillUnmount() {
            const {
                instance,
            } = this.props as IConnectedDatepickerElement

            if (instance) {
                instance.destroy()
            }
        },
    }),
)(DatepickerElement)

export default Datepicker