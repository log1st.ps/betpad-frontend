export interface IDatepickerScss {
  'container': string;
  'isDisabled': string;
  'icon': string;
  'value': string;
  'isRange': string;
  'rangeValue': string;
  'rangeIcon': string;
  'hasInputBefore': string;
  'hasInputAfter': string;
}

export const locals: IDatepickerScss;
