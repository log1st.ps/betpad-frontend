import cn from 'classnames'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { IToggleElement } from '../../../../../../types/assets/elements/controls/toggle/IToggleElement'
import {
    TOGGLE_LABEL_POSITION_AFTER,
    TOGGLE_LABEL_POSITION_BEFORE,
} from '../../../../../../types/assets/elements/controls/toggle/toggleLabelPositionsConstants'
import { TOGGLE_TYPE_CHECKBOX } from '../../../../../../types/assets/elements/controls/toggle/toggleTypesConstants'
import { ICON_CHECK } from '../../../../../../types/assets/elements/icon/iconsConstants'
import { ICON_STATE_INHERIT } from '../../../../../../types/assets/elements/icon/iconStatesConstants'
import Icon from '../../Icon/IconElement'
import { IToggleScss } from './toggle.scss'


const styles: IToggleScss = require('./toggle.scss')

interface IConnectedToggleElement extends IToggleElement {
    onInputChange(): void,
}

const ToggleElement = ({
    forwardedRef,
    type,
    label,
    labelPosition,
    isDisabled,
    isChecked,
    onInputChange,
}: IConnectedToggleElement) => (
    <div
        ref={forwardedRef}
        className={cn(
            styles.container,
            {
                [styles.isDisabled]: isDisabled,
            },
        )}
        onClick={onInputChange}
    >
        {label && labelPosition === TOGGLE_LABEL_POSITION_BEFORE && (
            <div className={cn(styles.label, styles.labelBefore)}>{label}</div>
        )}
        <div
            className={cn(
                styles.control,
                styles[`${type}Type`],
                {
                    [styles.isChecked]: isChecked,
                },
            )}
        >
            {type === TOGGLE_TYPE_CHECKBOX && (
                <div className={styles.checkboxIcon}>
                    <Icon
                        icon={ICON_CHECK}
                        state={ICON_STATE_INHERIT}
                    />
                </div>
            )}
        </div>
        {label && labelPosition === TOGGLE_LABEL_POSITION_AFTER && (
            <div className={cn(styles.label, styles.labelAfter)}>{label}</div>
        )}
    </div>
)

const Toggle: React.ComponentClass<IToggleElement> = compose<any, any>(
    withProps(({onChange, isChecked, isDisabled, trueValue, falseValue}: IToggleElement) => ({
        onInputChange() {
            if (!isDisabled && onChange) {
                onChange(isChecked ? falseValue : trueValue)
            }
        },
    })),

)(ToggleElement)

export default Toggle