export interface IToggleScss {
  'container': string;
  'isDisabled': string;
  'checkboxType': string;
  'isChecked': string;
  'radiobuttonType': string;
  'switchType': string;
  'label': string;
  'labelBefore': string;
  'labelAfter': string;
  'control': string;
  'checkboxIcon': string;
}

export const locals: IToggleScss;
