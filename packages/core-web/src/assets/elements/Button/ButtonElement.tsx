import cn from 'classnames'
import { createElement } from 'react'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import {
    BUTTON_SIZE_LG,
    BUTTON_SIZE_LT,
    BUTTON_SIZE_MD,
    BUTTON_SIZE_SM,
    BUTTON_SIZE_XL,
    BUTTON_SIZE_XS,
} from '../../../../../types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_TYPE_BUTTON,
    BUTTON_TYPE_LINK,
} from '../../../../../types/assets/elements/button/buttonTypesConstants'
import { IButtonElement } from '../../../../../types/assets/elements/button/IButtonElement'
import * as icons from '../../../../../types/assets/elements/icon/iconsConstants'
import {
    ICON_SIZE_LG,
    ICON_SIZE_LT,
    ICON_SIZE_MD,
    ICON_SIZE_SM,
    ICON_SIZE_XL,
    ICON_SIZE_XS,
} from '../../../../../types/assets/elements/icon/iconSizesConstants'
import { ICON_STATE_INHERIT } from '../../../../../types/assets/elements/icon/iconStatesConstants'
import Icon from '../Icon/IconElement'
import { IButtonScss } from './button.scss'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'

const styles: IButtonScss = require('./button.scss')

interface IConnectedButtonElement extends IButtonElement {
    Tag: any,
}

const ButtonElement = ({
    Tag,
    type,
    url,
    renderLeft,
    renderLeftInfo,
    text,
    renderRight,
    renderRightInfo,
    isCircle,
    isBlock,
    isSquare,
    state,
    size,
    isDisabled,
    onClick,
    forwardedRef,
    hasStackFromTop,
    hasStackFromRight,
    hasStackFromBottom,
    hasStackFromLeft,
    color,
    align,
    textAlign,
    onFocus,
    onBlur,
    tabIndex,
}: IConnectedButtonElement) => (
    <Tag
        tabIndex={tabIndex}
        onClick={onClick}
        onFocus={onFocus}
        onBlur={onBlur}
        ref={forwardedRef}
        className={cn(
            styles.button,
            state && (
                Array.isArray(state)
                    ? state.map(item => styles[`${item}State`])
                    : styles[`${state}State`]
            ),
            {
                [styles[`${color}Color`]]: color,
                [styles[`${size}Size`]]: size,
                [styles[`${textAlign}TextAlign`]]: textAlign,
                [styles.isCircle]: isCircle,
                [styles.isBlock]: isBlock,
                [styles.isSquare]: isSquare,
                [styles.isDisabled]: isDisabled,
                [styles.stackedFromTop]: hasStackFromTop,
                [styles.stackedFromRight]: hasStackFromRight,
                [styles.stackedFromBottom]: hasStackFromBottom,
                [styles.stackedFromLeft]: hasStackFromLeft,
            },
        )}
        href={type === BUTTON_TYPE_LINK ? url : undefined}
    >
        <div
            className={cn(
                styles.content,
                {
                    [styles[`${align}Align`]]: align,
                },
            )}
        >
            {renderLeftInfo && renderLeftInfo}
            {renderLeft && (
                <div className={styles.renderLeft}>
                    {typeof renderLeft === 'function' ? createElement(renderLeft as any) : renderLeft}
                </div>
            )}
            {text && (
                <div className={styles.text}>
                    {typeof text === 'function' ? createElement(text as any) : text}
                </div>
            )}
            {renderRight && (
                <div className={styles.renderRight}>
                    {typeof renderRight === 'function' ? createElement(renderRight as any) : renderRight}
                </div>
            )}
            {renderRightInfo && renderRightInfo}
        </div>
    </Tag>
)

ButtonElement.defaultProps = {
    textAlign: BUTTON_ALIGN_CENTER,
}

const iconKeys = Object.keys(icons).map(key => icons[key])

const getIconSize = (size: IButtonElement['size']) => (size && {
    [BUTTON_SIZE_LT]: ICON_SIZE_LT,
    [BUTTON_SIZE_XS]: ICON_SIZE_XS,
    [BUTTON_SIZE_SM]: ICON_SIZE_SM,
    [BUTTON_SIZE_MD]: ICON_SIZE_MD,
    [BUTTON_SIZE_LG]: ICON_SIZE_LG,
    [BUTTON_SIZE_XL]: ICON_SIZE_XL,
}[size as string])

const getIconState = (state: IButtonElement['state']) => ICON_STATE_INHERIT as any

const Button: React.ComponentClass<IButtonElement> = compose<any, any>(
    withProps(({type}: IButtonElement) => ({
        Tag: type,
    })),
    withProps(({text}: IButtonElement) => ({
        text,
    })),
    withProps(({
        renderLeft,
        renderRight,
        size,
        state,
        isSquare,
    }: IButtonElement) => ({
        renderLeft: iconKeys.indexOf(renderLeft as string) > -1 ? (
            <Icon
                icon={renderLeft as any}
                size={getIconSize(size)}
                state={getIconState(state)}
                isSquare={isSquare}
            />
        ) : renderLeft,
        renderRight: iconKeys.indexOf(renderRight as string) > -1 ? (
            <Icon
                icon={renderRight as any}
                size={getIconSize(size)}
                state={getIconState(state)}
                isSquare={isSquare}
            />
        ) : renderRight,
    })),
    withProps(({isDisabled, onClick}) => ({
        onClick: isDisabled ? undefined : onClick,
    })),
)(ButtonElement)

Button.defaultProps = {
    type: BUTTON_TYPE_BUTTON,
    isCircle: false,
    isSquare: false,
    isBlock: false,
}

export default Button