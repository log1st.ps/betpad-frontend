import * as React from 'react'
import { Consumer } from './ThemeProviderComponent'

export default (Component : any) => (props : any) => (
    <Consumer>
        {contexts => (
            <Component {...{...props, ...contexts}}/>
        )}
    </Consumer>
)