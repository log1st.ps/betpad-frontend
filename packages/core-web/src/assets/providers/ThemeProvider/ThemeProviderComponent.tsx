import { IThemeProviderComponent } from '@betpad/types/assets/providers/themeProvider/IThemeProviderComponent'
import { THEME_LIGHT } from '@betpad/types/assets/providers/themeProvider/themeConstants'
import * as React from 'react'

const context = React.createContext({
    theme: THEME_LIGHT,
})

const {Provider, Consumer} = context

const ThemeProvider = ({
   children,
   ...props
}: IThemeProviderComponent) => (
    <Provider value={props}>
        {children}
    </Provider>
)

export { Consumer }

export default ThemeProvider
