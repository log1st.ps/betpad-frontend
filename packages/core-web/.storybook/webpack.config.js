const TSDocgenPlugin = require('react-docgen-typescript-webpack-plugin');

module.exports = (baseConfig, env, config) => {
    config.module.rules.push({
        loader: require.resolve('awesome-typescript-loader'),
        test: /\.(ts|tsx)$/,
    });
    config.module.rules.push({
        test: /\.(scss)$/,
        use: [
            {
                loader: 'style-loader',
                options: {
                    hmr: false
                }
            },
            {
                loader: "typings-for-css-modules-loader",
                options: {
                    camelcase: true,
                    localIdentName: '[name]--[local]',
                    modules: true,
                    namedexport: true,
                }
            },
            {
                loader: "sass-loader"
            }
        ]
    })
    config.plugins.push(new TSDocgenPlugin());
    config.resolve.extensions.push('.ts', '.tsx');
    config.module.rules.map(rule => {
        const source = rule.test.source

        if(source === '\\.(svg|ico|jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|cur|ani)(\\?.*)?$') {
            rule.exclude = [
                ...(rule.exclude || []),
                /.*(inline\.svg).*/,
            ]
        }

        console.log(rule)

        return rule
    })

    config.module.rules.push({
        test: /\.inline\.svg$/,
        use: [
            {
                loader: 'babel-loader'
            },
            {
                loader: 'svg-react-loader',
            }
        ]
    })

    return config;
};