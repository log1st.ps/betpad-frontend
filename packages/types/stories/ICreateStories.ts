import {
    DecoratorParameters,
    RenderFunction,
} from '@storybook/react'
import {
    THEME_DARK,
    THEME_LIGHT,
} from '../assets/providers/themeProvider/themeConstants'

export interface ICreateStories {
    title: string,
    stories: Array<{
        name: string,
        render: RenderFunction,
        parameters?: DecoratorParameters,
    }>,
    hasTheme?: boolean,
    defaultTheme?:
        typeof THEME_DARK
        | typeof THEME_LIGHT,

    module: any,
}