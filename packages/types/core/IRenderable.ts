import { ReactNode } from 'react'

export type IRenderable = string | ReactNode | (() => any) | undefined