import { RefObject } from 'react'

export interface IWithForwadedRefComponent {
    forwardedRef?: RefObject<any>
}