export const COLOR_BLUE = 'blue'
export const COLOR_BLUE_LIGHTER = 'blueLighter'
export const COLOR_BLUE_DEEP = 'blueDeep'
export const COLOR_TEAL = 'teal'
export const COLOR_GREEN_DARKER = 'greenDarker'
export const COLOR_GREEN = 'green'
export const COLOR_YELLOW = 'yellow'
export const COLOR_RED = 'red'
export const COLOR_GRADIENT_BLUE = 'gradientBlue'
export const COLOR_GRADIENT_GREEN = 'gradientGreen'
export const COLOR_GRADIENT_SPECTRE = 'gradientSpectre'
export const COLOR_BLACK = 'black'
export const COLOR_GRAY_900 = 'gray900'
export const COLOR_GRAY_800 = 'gray800'
export const COLOR_GRAY_700 = 'gray700'
export const COLOR_GRAY_600 = 'gray600'
export const COLOR_GRAY_500 = 'gray500'
export const COLOR_GRAY_400 = 'gray400'
export const COLOR_GRAY_300 = 'gray300'
export const COLOR_WHITE = 'white'

export type availableColors =
    typeof COLOR_BLUE
    | typeof COLOR_BLUE_DEEP
    | typeof COLOR_BLUE_LIGHTER
    | typeof COLOR_TEAL
    | typeof COLOR_GREEN_DARKER
    | typeof COLOR_GREEN
    | typeof COLOR_YELLOW
    | typeof COLOR_RED
    | typeof COLOR_GRADIENT_BLUE
    | typeof COLOR_GRADIENT_GREEN
    | typeof COLOR_GRADIENT_SPECTRE
    | typeof COLOR_BLACK
    | typeof COLOR_GRAY_900
    | typeof COLOR_GRAY_800
    | typeof COLOR_GRAY_700
    | typeof COLOR_GRAY_600
    | typeof COLOR_GRAY_500
    | typeof COLOR_GRAY_400
    | typeof COLOR_GRAY_300
    | typeof COLOR_WHITE