export interface IWithEvents {
    target: string | EventTarget,
    targetKey?: string,
    events: string[],
    handlers?(getProps: (() => any)): {
        [key: string]: (e: Event) => void,
    }
}