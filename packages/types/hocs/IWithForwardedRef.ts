import * as React from 'react'
export type IWithForwardedRef =
    <Props extends {[_: string]: any}>(base: React.ReactType<Props>)
    => any