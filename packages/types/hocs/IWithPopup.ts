import {
    ReactNode,
} from 'react'
import { IRenderable } from '../core/IRenderable'

export const POPUP_POSITION_LEFT = 'left'
export const POPUP_POSITION_TOP = 'top'
export const POPUP_POSITION_RIGHT = 'right'
export const POPUP_POSITION_BOTTOM = 'bottom'
export const POPUP_POSITION_AUTO = 'auto'

export const POPUP_ALIGN_START = 'start'
export const POPUP_ALIGN_END = 'end'
export const POPUP_ALIGN_AUTO = 'auto'

type availablePositions =
    typeof POPUP_POSITION_LEFT
    | typeof POPUP_POSITION_TOP
    | typeof POPUP_POSITION_RIGHT
    | typeof POPUP_POSITION_BOTTOM
    | typeof POPUP_POSITION_AUTO

type availableAligns =
    typeof POPUP_ALIGN_START
    | typeof POPUP_ALIGN_AUTO
    | typeof POPUP_ALIGN_END

export interface IWithPopupProps {
    position?: availablePositions,
    align?: availableAligns,
    zStartIndex?: number,
    zPopupOffset?: number,
    offsetPrimary?: number,
    offsetSecondary?: number,
    isFixed?: boolean,
    into?: string | Element,
    children?: ReactNode,
    onCreate?(data: any): void,
    onUpdate?(data: any): void,
    onArrowPositionUpdate?(position: string): void,
    renderContent({zIndex, ...props}: {zIndex: number}): IRenderable,
}

export type IWithPopup = (props: IWithPopupProps) => any