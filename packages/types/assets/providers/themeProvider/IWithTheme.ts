import { ReactNode } from 'react'

export type IWithTheme = ReactNode