import { Renderable } from '@storybook/react'
import {
    THEME_DARK,
    THEME_LIGHT,
} from './themeConstants'

export interface IThemeProviderComponent {
    theme:
        typeof THEME_LIGHT
        | typeof THEME_DARK,
    isMobile?: boolean,
    children?: Renderable,
}