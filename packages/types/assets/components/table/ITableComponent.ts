import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    TABLE_CELL_ALIGN_CENTER,
    TABLE_CELL_ALIGN_END,
    TABLE_CELL_ALIGN_START,
} from './tableCellAlignsConstants'

type availableCellAligns =
    typeof TABLE_CELL_ALIGN_START
    | typeof TABLE_CELL_ALIGN_CENTER
    | typeof TABLE_CELL_ALIGN_END

export interface ITableItem {
    key: string,
    render?: IRenderable,
    width?: number | string,
    hasHorizontalPadding?: boolean,
    hasTopPadding?: boolean,
    hasBottomPadding?: boolean,
    align?: availableCellAligns,
    hasBorder?: boolean,
    hasBackground?: boolean,
}

export interface ITableRow {
    key: string,
    items: ITableItem[],
}

interface ITableTitle {
    key: string,
    title: string,
    width?: number | string,
}

export interface ITableComponent extends IWithForwadedRefComponent {
    titles: ITableTitle[],
    rows: ITableRow[],
}