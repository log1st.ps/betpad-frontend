import { availableColors } from '../../../core/colors'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IIconElement } from '../../elements/icon/IIconElement'
import {
    CARD_TYPE_ICON,
    CARD_TYPE_LOGO,
    CARD_TYPE_TEXT,
} from './cardTypesConstants'

type availableTypes =
    typeof CARD_TYPE_ICON
    | typeof CARD_TYPE_LOGO
    | typeof CARD_TYPE_TEXT

export interface ICardComponent extends IWithForwadedRefComponent {
    type: availableTypes,
    isActive?: boolean,
    label?: string,
    icon?: IIconElement['icon'],
    logo?: string,
    value?: string,
    isDisabled?: boolean,
    logoBackground?: availableColors,
    onClick?(): void,
}