import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IButtonElement } from '../../elements/button/IButtonElement'

export interface IDropdownComponent extends IWithForwadedRefComponent {
    items: Array<{
        key: string,
        url?: IButtonElement['url'],
        onClick?: IButtonElement['onClick'],
        renderLeft?: IButtonElement['renderLeft'],
        renderRight?: IButtonElement['renderRight'],
        text?: IButtonElement['text'],
    }>,
    onOutsideClick?(): void
}