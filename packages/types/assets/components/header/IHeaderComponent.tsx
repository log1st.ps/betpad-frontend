import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IButtonElement } from '../../elements/button/IButtonElement'
import {
    HEADER_MENU_ALIGN_CENTER,
    HEADER_MENU_ALIGN_LEFT,
    HEADER_MENU_ALIGN_RIGHT,
} from './headerMenuAlignsConstants'

export interface IHeaderMenuItem {
    key: string,
    url?: string,
    text?: string,
    isActive?: boolean,
    isDisabled?: boolean,
    isBig?: boolean,
    onClick?(): void,
}

export interface IHeaderComponent extends IWithForwadedRefComponent {
    menuItems?: IHeaderMenuItem[],
    menuAlign?: 
        typeof HEADER_MENU_ALIGN_LEFT
        | typeof HEADER_MENU_ALIGN_CENTER
        | typeof HEADER_MENU_ALIGN_RIGHT
    primaryButton?: IButtonElement,
    userInfo?: {
        name?: string,
        avatar?: string,
        url?: string,
        onClick?(): void,
    },
    onLogoClick?(): void,
}