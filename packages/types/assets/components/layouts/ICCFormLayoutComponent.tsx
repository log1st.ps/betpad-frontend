import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ICCFormLayoutComponent extends IWithForwadedRefComponent {
    renderNameField?: IRenderable,
    renderNumberField?: IRenderable,
    expirationLabel?: string,
    renderExpirationMonthField?: IRenderable,
    renderExpirationYearField?: IRenderable,
    renderCVCField?: IRenderable,
}