import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IListingPageLayout extends IWithForwadedRefComponent {
    renderMode?: IRenderable,
    renderFilters?: IRenderable,
    renderDatepicker?: IRenderable,
    renderCategories?: IRenderable,
    renderTabs?: IRenderable,
    renderTable?: IRenderable,
}