import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ITariffsPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderSubTitle?: IRenderable,
    renderTariffs?: IRenderable,
    renderForm?: IRenderable,
    renderCTA?: IRenderable
}