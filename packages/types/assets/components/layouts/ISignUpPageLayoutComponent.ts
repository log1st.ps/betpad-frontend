import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ISignUpPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderForm?: IRenderable,
    renderCTA?: IRenderable,
    renderSubActions?: IRenderable
}