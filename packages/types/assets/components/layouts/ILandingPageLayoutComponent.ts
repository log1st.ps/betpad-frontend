import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ILandingPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderSubTitle?: IRenderable,
    renderCategories?: IRenderable,
    renderCTA?: IRenderable,
}