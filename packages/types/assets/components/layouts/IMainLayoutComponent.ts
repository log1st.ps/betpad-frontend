import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IThemeProviderComponent } from '../../providers/themeProvider/IThemeProviderComponent'

export interface IMainLayoutComponent extends IWithForwadedRefComponent{
    children: IRenderable,
    theme?: IThemeProviderComponent['theme'],
    renderHeader?: IRenderable,
    isHeaderShadowed?: boolean,
}