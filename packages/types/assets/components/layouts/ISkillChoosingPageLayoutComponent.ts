import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ISkillChoosingPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderSubTitle?: IRenderable,
    renderValues?: IRenderable,
    renderCTA?: IRenderable,
    renderStep?: IRenderable,
}