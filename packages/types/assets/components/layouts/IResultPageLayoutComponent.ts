import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IResultPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderSubTitle?: IRenderable,
    renderCTA?: IRenderable
}