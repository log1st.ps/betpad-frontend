import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IBookmakersChoosingPageLayoutComponent extends IWithForwadedRefComponent {
    renderTitle?: IRenderable,
    renderSubTitle?: IRenderable,
    renderBookmakers?: IRenderable,
    renderCTA?: IRenderable,
    renderStep?: IRenderable,
}