import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IFormComponent extends IWithForwadedRefComponent {
    children?: IRenderable,
    onSubmit?(): void,
}