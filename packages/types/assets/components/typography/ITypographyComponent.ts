import { availableColors } from '../../../core/colors'
import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    TYPOGRAPHY_TEXT_ALIGN_CENTER,
    TYPOGRAPHY_TEXT_ALIGN_LEFT,
    TYPOGRAPHY_TEXT_ALIGN_RIGHT,
} from './typographyTextAlignsConstants'

type availableTextAligns =
    typeof TYPOGRAPHY_TEXT_ALIGN_LEFT
    | typeof TYPOGRAPHY_TEXT_ALIGN_CENTER
    | typeof TYPOGRAPHY_TEXT_ALIGN_RIGHT

export interface ITypographyComponent extends IWithForwadedRefComponent {
    content: IRenderable,
    color?: availableColors,
    textAlign?: availableTextAligns,
}

export interface IPrimaryHeadingComponent extends ITypographyComponent {}
export interface ISecondaryHeadingComponent extends ITypographyComponent {}
export interface ITertiaryHeadingComponent extends ITypographyComponent {}
export interface IQuaternaryHeadingComponent extends ITypographyComponent {}
export interface IQuinaryHeadingComponent extends ITypographyComponent {}
export interface ISenaryHeadingComponent extends ITypographyComponent {}

export interface IPrimarySubHeadingComponent extends ITypographyComponent {}
export interface ISecondarySubHeadingComponent extends ITypographyComponent {}
export interface ITertiarySubHeadingComponent extends ITypographyComponent {}
export interface IQuaternarySubHeadingComponent extends ITypographyComponent {}
export interface IQuinarySubHeadingComponent extends ITypographyComponent {}
export interface ISenarySubHeadingComponent extends ITypographyComponent {}
export interface ISeptenarySubHeadingComponent extends ITypographyComponent {}

export interface IPrimaryCaption extends ITypographyComponent {}
export interface ISecondaryCaption extends ITypographyComponent {}

export interface IParagraphComponent extends ITypographyComponent {}
export interface ISubTextComponent extends ITypographyComponent {}
export interface IHintComponent extends ITypographyComponent {}