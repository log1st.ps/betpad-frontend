export const TYPOGRAPHY_TEXT_ALIGN_LEFT = 'left'
export const TYPOGRAPHY_TEXT_ALIGN_CENTER = 'center'
export const TYPOGRAPHY_TEXT_ALIGN_RIGHT = 'right'