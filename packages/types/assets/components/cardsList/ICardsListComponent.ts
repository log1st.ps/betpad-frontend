import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { ICardComponent } from '../card/ICardComponent'
import {
    CARDS_LIST_ALIGN_CENTER,
    CARDS_LIST_ALIGN_LEFT,
    CARDS_LIST_ALIGN_RIGHT,
} from './cardsListAlignsConstants'

interface IConnectedCardComponent extends ICardComponent {
    key: string,
    width?: number,
}

type availableAligns =
    typeof CARDS_LIST_ALIGN_LEFT
    | typeof CARDS_LIST_ALIGN_CENTER
    | typeof CARDS_LIST_ALIGN_RIGHT

export interface ICardsListComponent extends IWithForwadedRefComponent {
    cards: IConnectedCardComponent[],
    align: availableAligns,
}