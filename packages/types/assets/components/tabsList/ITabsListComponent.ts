import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface ITabsListComponent extends IWithForwadedRefComponent {
    items: Array<{
        value: string,
        label: string,
        isDisabled?: boolean,
    }>,
    value?: string,
    onClick?(value: string): void,
}