import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IPaginationElement {
    value: string,
    label: string,
    isDisabled?: boolean,
}

export interface IPaginationComponent extends IWithForwadedRefComponent {
    items: IPaginationElement[],
    value?: string,
    onClick(value: string): void,
}