import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IButtonElement } from '../../elements/button/IButtonElement'
import {
    BUTTONS_LIST_JUSTIFY_AROUND,
    BUTTONS_LIST_JUSTIFY_BETWEEN,
    BUTTONS_LIST_JUSTIFY_CENTER,
    BUTTONS_LIST_JUSTIFY_END,
    BUTTONS_LIST_JUSTIFY_EVENLY,
    BUTTONS_LIST_JUSTIFY_START,
} from './buttonsListAlignsConstants'

interface IConnectedButtonElement extends IButtonElement {
    key: string,
}

type availableAligns =
    typeof BUTTONS_LIST_JUSTIFY_START
    | typeof BUTTONS_LIST_JUSTIFY_CENTER
    | typeof BUTTONS_LIST_JUSTIFY_END
    | typeof BUTTONS_LIST_JUSTIFY_BETWEEN
    | typeof BUTTONS_LIST_JUSTIFY_AROUND
    | typeof BUTTONS_LIST_JUSTIFY_EVENLY

export interface IButtonsListComponent extends IWithForwadedRefComponent {
    buttons: IConnectedButtonElement[],
    align?: availableAligns,
}