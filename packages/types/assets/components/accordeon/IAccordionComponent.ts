import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'

export interface IAccordionComponent extends IWithForwadedRefComponent {
    isActive?: boolean,
    label?: string,
    children: IRenderable,
    onClick?(): void,
}