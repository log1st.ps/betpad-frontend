import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import { IIconElement } from '../../elements/icon/IIconElement'

export interface ITagsListElement {
    value: string,
    label?: string,
    icon?: IIconElement['icon'],
}

export interface ITagsListComponent extends IWithForwadedRefComponent {
    items: ITagsListElement[],
    onClick?(key: string): void,
}