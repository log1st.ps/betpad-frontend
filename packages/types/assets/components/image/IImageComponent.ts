import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    IMAGE_BACKGROUND_TYPE_CONTAIN,
    IMAGE_BACKGROUND_TYPE_COVER,
    IMAGE_BACKGROUND_TYPE_INITIAL,
    IMAGE_BACKGROUND_TYPE_STRETCH,
} from './imageBackgroundTypesConstants'
import {
    IMAGE_BOUND_AUTO,
    IMAGE_BOUND_STRETCH,
} from './imageBoundsConstants'
import {
    IMAGE_TYPE_BLOCK,
    IMAGE_TYPE_IMG,
} from './imageTypesConstants'

type availableImageTypes =
    typeof IMAGE_TYPE_IMG
    | typeof IMAGE_TYPE_BLOCK

type availableImageBackgroundTypes =
    typeof IMAGE_BACKGROUND_TYPE_INITIAL
    | typeof IMAGE_BACKGROUND_TYPE_STRETCH
    | typeof IMAGE_BACKGROUND_TYPE_COVER
    | typeof IMAGE_BACKGROUND_TYPE_CONTAIN

type availableBounds =
    typeof IMAGE_BOUND_AUTO
    | typeof IMAGE_BOUND_STRETCH
    | number
    | string

export interface IImageComponent extends IWithForwadedRefComponent {
    url: string,
    alt?: string,
    type?: availableImageTypes,
    backgroundType?: availableImageBackgroundTypes,
    height?: availableBounds,
    width?: availableBounds,
}