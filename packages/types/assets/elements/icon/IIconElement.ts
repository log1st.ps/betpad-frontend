import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    ICON_ANGLE,
    ICON_ANGLE_BOTTOM,
    ICON_ANGLE_TOP,
    ICON_CALENDAR,
    ICON_CALENDAR_MINI,
    ICON_CARET,
    ICON_CARET_DOWN,
    ICON_CHECK,
    ICON_CLOSE,
    ICON_PAUSE,
    ICON_PLAY,
    ICON_PLUS,
    ICON_SOCIAL_FACEBOOK,
    ICON_SOCIAL_GOOGLE,
    ICON_SOCIAL_VK,
    ICON_SPORT_BASKETBALL,
    ICON_SPORT_FIGHTING,
    ICON_SPORT_FOOTBALL,
    ICON_SPORT_GANDBALL,
    ICON_SPORT_HOCKEY,
    ICON_SPORT_TENNIS,
    ICON_SPORT_VOLLEYBALL,
} from './iconsConstants'
import {
    ICON_SIZE_LG,
    ICON_SIZE_LT,
    ICON_SIZE_MD,
    ICON_SIZE_SM,
    ICON_SIZE_STRETCH,
    ICON_SIZE_XL,
    ICON_SIZE_XS,
} from './iconSizesConstants'
import {
    ICON_STATE_INHERIT,
    ICON_STATE_PURE,
} from './iconStatesConstants'

type availableIcons =
    typeof ICON_CLOSE
    | typeof ICON_CALENDAR
    | typeof ICON_PAUSE
    | typeof ICON_PLAY
    | typeof ICON_SPORT_BASKETBALL
    | typeof ICON_SPORT_FIGHTING
    | typeof ICON_SPORT_FOOTBALL
    | typeof ICON_SPORT_GANDBALL
    | typeof ICON_SPORT_HOCKEY
    | typeof ICON_SPORT_TENNIS
    | typeof ICON_SPORT_VOLLEYBALL
    | typeof ICON_PLUS
    | typeof ICON_CALENDAR_MINI
    | typeof ICON_SOCIAL_VK
    | typeof ICON_SOCIAL_FACEBOOK
    | typeof ICON_SOCIAL_GOOGLE
    | typeof ICON_CARET
    | typeof ICON_CARET_DOWN
    | typeof ICON_CHECK
    | typeof ICON_ANGLE
    | typeof ICON_ANGLE_BOTTOM
    | typeof ICON_ANGLE_TOP

type availableSizes =
    typeof ICON_SIZE_STRETCH
    | typeof ICON_SIZE_LT
    | typeof ICON_SIZE_XS
    | typeof ICON_SIZE_SM
    | typeof ICON_SIZE_MD
    | typeof ICON_SIZE_LG
    | typeof ICON_SIZE_XL

type availableStates =
    typeof ICON_STATE_PURE
    | typeof ICON_STATE_INHERIT

export interface IIconElement extends IWithForwadedRefComponent {
    icon: availableIcons,
    size?: availableSizes,
    state?: availableStates,
    isSquare?: boolean
}