import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'

export interface ISliderElement extends IWithForwadedRefComponent {
    min: number,
    max: number,
    step: number,
    value: number | number[],
    isRange?: boolean,
    onChange?(value: number | number[]): void,
}