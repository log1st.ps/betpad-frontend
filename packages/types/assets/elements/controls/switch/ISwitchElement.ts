import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'

export interface ISwitchValue {
    key: string,
    text: string,
}

export interface ISwitchElement extends IWithForwadedRefComponent {
    values: ISwitchValue[],
    value?: string,
    onChange?(value: string): void
}