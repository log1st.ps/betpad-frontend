import { IRenderable } from '../../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'
import {
    FIELD_STATE_DEFAULT,
    FIELD_STATE_ERROR,
    FIELD_STATE_SUCCESS,
} from './fieldStatesConstants'
import {
    FIELD_TYPE_DEFAULT,
    FIELD_TYPE_INLINE,
} from './fieldTypesConstants'

type availableTypes =
    typeof FIELD_TYPE_DEFAULT
    | typeof FIELD_TYPE_INLINE

type availableStates = 
    typeof FIELD_STATE_DEFAULT
    | typeof FIELD_STATE_SUCCESS
    | typeof FIELD_STATE_ERROR

export interface IFieldElement extends IWithForwadedRefComponent {
    label?: IRenderable,
    children?: IRenderable,
    type?: availableTypes,
    hint?: string,
    state?: availableStates,
}