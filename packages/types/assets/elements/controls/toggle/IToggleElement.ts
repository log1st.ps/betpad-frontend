import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'
import {
    TOGGLE_LABEL_POSITION_AFTER,
    TOGGLE_LABEL_POSITION_BEFORE,
} from './toggleLabelPositionsConstants'
import {
    TOGGLE_TYPE_CHECKBOX,
    TOGGLE_TYPE_RADIOBUTTON,
    TOGGLE_TYPE_SWITCH,
} from './toggleTypesConstants'

type availableTypes =
    typeof TOGGLE_TYPE_CHECKBOX
    | typeof TOGGLE_TYPE_RADIOBUTTON
    | typeof TOGGLE_TYPE_SWITCH

type availableLabelPositions =
    typeof TOGGLE_LABEL_POSITION_BEFORE
    | typeof TOGGLE_LABEL_POSITION_AFTER

export interface IToggleElement extends IWithForwadedRefComponent {
    type: availableTypes,
    isChecked?: boolean,
    trueValue?: any,
    falseValue?: any,
    isDisabled?: boolean,
    label?: string,
    labelPosition?: availableLabelPositions,
    onChange?(value: any): void,
}