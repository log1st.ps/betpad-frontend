import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'

export interface IDatepickerElement extends IWithForwadedRefComponent {
    value: number | number[],
    format?: string,
    isRange?: boolean,
    isDisabled?: boolean,
    hasInputBefore?: boolean,
    hasInputAfter?: boolean,
    onChange?(value: number | number[]): void,
}