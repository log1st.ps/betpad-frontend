import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'

export interface IInputElement extends IWithForwadedRefComponent {
    value?: string,
    placeholder?: string,
    isDisabled?: boolean,
    prefix?: string,
    suffix?: string,
    hasInputBefore?: boolean,
    hasInputAfter?: boolean,
    onChange?(value: string): void,
}