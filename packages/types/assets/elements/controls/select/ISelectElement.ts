import { IWithForwadedRefComponent } from '../../../../core/IWithForwadedRefComponent'

export interface ISelectElement extends IWithForwadedRefComponent {
    texts: {
        noData: string,
    },
    value?: string,
    values?: Array<{
        value: string,
        text: string,
    }>
    placeholder?: string,
    isDisabled?: boolean,
    hasInputBefore?: boolean,
    hasInputAfter?: boolean,
    onChange?(value: string): void,
}