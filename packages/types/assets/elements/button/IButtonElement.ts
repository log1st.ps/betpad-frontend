import { availableColors } from '../../../core/colors'
import { IRenderable } from '../../../core/IRenderable'
import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    BUTTON_ALIGN_CENTER,
    BUTTON_ALIGN_END,
    BUTTON_ALIGN_START,
} from './buttonAlignsConstants'
import {
    BUTTON_SIZE_LG,
    BUTTON_SIZE_LT,
    BUTTON_SIZE_MD,
    BUTTON_SIZE_SM,
    BUTTON_SIZE_XL,
    BUTTON_SIZE_XS,
} from './buttonSizesConstants'
import {
    BUTTON_STATE_ACTIVE,
    BUTTON_STATE_GRADIENT,
    BUTTON_STATE_INACTIVE_OPACITY_0,
    BUTTON_STATE_INACTIVE_OPACITY_10,
    BUTTON_STATE_INACTIVE_OPACITY_100,
    BUTTON_STATE_INACTIVE_OPACITY_20,
    BUTTON_STATE_INACTIVE_OPACITY_30,
    BUTTON_STATE_INACTIVE_OPACITY_40,
    BUTTON_STATE_INACTIVE_OPACITY_50,
    BUTTON_STATE_INACTIVE_OPACITY_60,
    BUTTON_STATE_INACTIVE_OPACITY_70,
    BUTTON_STATE_INACTIVE_OPACITY_80,
    BUTTON_STATE_INACTIVE_OPACITY_90,
    BUTTON_STATE_NAVIGATE,
    BUTTON_STATE_OUTLINE,
    BUTTON_STATE_PRIMARY,
    BUTTON_STATE_PURE,
    BUTTON_STATE_SECONDARY,
    BUTTON_STATE_TAB,
    BUTTON_STATE_TAG,
    BUTTON_STATE_VALUE,
} from './buttonStatesConstants'
import {
    BUTTON_TYPE_BUTTON,
    BUTTON_TYPE_LINK,
} from './buttonTypesConstants'

type availableStates =
    typeof BUTTON_STATE_ACTIVE
    | typeof BUTTON_STATE_PURE
    | typeof BUTTON_STATE_PRIMARY
    | typeof BUTTON_STATE_SECONDARY
    | typeof BUTTON_STATE_GRADIENT
    | typeof BUTTON_STATE_OUTLINE
    | typeof BUTTON_STATE_VALUE
    | typeof BUTTON_STATE_TAG
    | typeof BUTTON_STATE_NAVIGATE
    | typeof BUTTON_STATE_TAB
    | typeof BUTTON_STATE_INACTIVE_OPACITY_100
    | typeof BUTTON_STATE_INACTIVE_OPACITY_90
    | typeof BUTTON_STATE_INACTIVE_OPACITY_80
    | typeof BUTTON_STATE_INACTIVE_OPACITY_70
    | typeof BUTTON_STATE_INACTIVE_OPACITY_60
    | typeof BUTTON_STATE_INACTIVE_OPACITY_50
    | typeof BUTTON_STATE_INACTIVE_OPACITY_40
    | typeof BUTTON_STATE_INACTIVE_OPACITY_30
    | typeof BUTTON_STATE_INACTIVE_OPACITY_20
    | typeof BUTTON_STATE_INACTIVE_OPACITY_10
    | typeof BUTTON_STATE_INACTIVE_OPACITY_0

type availableSizes =
    typeof BUTTON_SIZE_LT
    | typeof BUTTON_SIZE_XS
    | typeof BUTTON_SIZE_SM
    | typeof BUTTON_SIZE_MD
    | typeof BUTTON_SIZE_LG
    | typeof BUTTON_SIZE_XL

type availableTypes =
    typeof BUTTON_TYPE_BUTTON
    | typeof BUTTON_TYPE_LINK

type availableAligns =
    typeof BUTTON_ALIGN_START
    | typeof BUTTON_ALIGN_CENTER
    | typeof BUTTON_ALIGN_END

export interface IButtonElement extends IWithForwadedRefComponent {
    type?: availableTypes,
    url?: string,
    renderLeft?: IRenderable,
    renderLeftInfo?: IRenderable
    text?: IRenderable,
    renderRight?: IRenderable,
    renderRightInfo?: IRenderable,
    isBlock?: boolean,
    isCircle?: boolean,
    isSquare?: boolean,
    isDisabled?: boolean,
    size?: availableSizes,
    state?: availableStates | availableStates[],
    hasStackFromTop?: boolean,
    hasStackFromRight?: boolean,
    hasStackFromBottom?: boolean,
    hasStackFromLeft?: boolean,
    color?: availableColors,
    align?: availableAligns,
    textAlign?: availableAligns,
    tabIndex?: number,
    onClick?(): void,
    onFocus?(): void,
    onBlur?(): void,
}