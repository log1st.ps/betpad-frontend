import { IWithForwadedRefComponent } from '../../../core/IWithForwadedRefComponent'
import {
    AVATAR_SIZE_LG,
    AVATAR_SIZE_LT,
    AVATAR_SIZE_MD,
    AVATAR_SIZE_SM,
    AVATAR_SIZE_XL,
    AVATAR_SIZE_XS,
} from './avatarSizesConstants'

type availableSizes =
    typeof AVATAR_SIZE_LT
    | typeof AVATAR_SIZE_XS
    | typeof AVATAR_SIZE_SM
    | typeof AVATAR_SIZE_MD
    | typeof AVATAR_SIZE_LG
    | typeof AVATAR_SIZE_XL

export interface IAvatarElement extends IWithForwadedRefComponent {
    name: string,
    size: availableSizes,
    url?: string,
}