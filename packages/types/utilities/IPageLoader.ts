import { Reducer } from 'redux'
import { IRenderable } from '../core/IRenderable'
import { IDataConverter } from './IDataConverter'

export type IPageLoader = () => ({
    component?: IRenderable,
    dataConverter?: IDataConverter,
    reducer?: Reducer,
})