export const SKILL_NOVICE = 'novice'
export const SKILL_SMALL_KNOWLEDGE = 'small-knowledge'
export const SKILL_RISKY = 'risky'
export const SKILL_PRO = 'pro'