export const LISTING_MODE_PREMATCH = 'prematch'
export const LISTING_MODE_LIVE = 'live'

export const LISTING_TYPE_RATING = 'rating'
export const LISTING_TYPE_FORK = 'fork'