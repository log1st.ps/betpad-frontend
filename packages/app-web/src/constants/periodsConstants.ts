export const PERIOD_MONTH = 'month'
export const PERIOD_HALF_YEAR = 'half-year'
export const PERIOD_YEAR = 'year'