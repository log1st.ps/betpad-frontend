import { formatString } from './stringExtensions'

export const CURRENCY_RUR = 'RUR'

const currencies: {
    [key: string]: string,
} = {
    [CURRENCY_RUR]: '{0} руб.',
}

type availableCurrency =
    typeof CURRENCY_RUR

export default (value: number, currency: availableCurrency) => formatString(currencies[currency], String(value))