import createStore from '@betpad/helpers/redux/createStore'
import { createContext } from 'react'
import * as React from 'react'
import commonReducer, { ICommonReducer } from '../reducers/commonReducer'
import i18nReducer, { II18N } from '../reducers/i18nReducer'
import userReducer, { IUserReducer } from '../reducers/userReducer'

export interface IStorePage {
    key: string,
}

export interface IStore {
    common: ICommonReducer,
    i18n: II18N,
    page?: IStorePage,
    user: IUserReducer,
}

const store =  createStore(
    {},
    {
        common: commonReducer,
        i18n: i18nReducer,
        user: userReducer,
    },
)

const StoreContext = createContext(null as any)

export const passStore = (BaseComponent: any) => (props: any) => (
    <StoreContext.Consumer>
        {({store: myStore}) => (
            <BaseComponent {...myStore} {...props}/>
        )}
    </StoreContext.Consumer>
)

export {StoreContext}

export default store