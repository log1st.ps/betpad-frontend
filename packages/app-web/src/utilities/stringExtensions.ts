export const formatString = (str: string, ...args: string[]) => {
    let formatted: string = str

    for (const i in args) {
        if (args.hasOwnProperty(i)) {
            formatted = formatted.replace(`{${i}}`, args[i])
        }
    }

    return formatted
}