import Header from '@betpad/core-web/components/HeaderComponent'
import MainLayout from '@betpad/core-web/components/layouts/MainLayoutComponent'
import ThemeProvider from '@betpad/core-web/components/providers/ThemeProviderComponent'
import '@betpad/core-web/src/core/core.scss'
import {
    HEADER_MENU_ALIGN_LEFT,
    HEADER_MENU_ALIGN_RIGHT,
} from '@betpad/types/assets/components/header/headerMenuAlignsConstants'
import { IHeaderComponent } from '@betpad/types/assets/components/header/IHeaderComponent'
import { BUTTON_SIZE_SM } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_OUTLINE } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { BUTTON_TYPE_LINK } from '@betpad/types/assets/elements/button/buttonTypesConstants'
import { THEME_LIGHT } from '@betpad/types/assets/providers/themeProvider/themeConstants'
import * as React from 'react'
import { connect } from 'react-redux'
import {
    matchPath,
    withRouter,
} from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import withProps from 'recompose/withProps'
import {
    navigate,
    setHistory,
} from '../actions/commonActions'
import {
    IStore,
    StoreContext,
} from '../utilities/store'
import RouterContainer from './RouterContainer'


const ConnectedHeader = compose(
    withRouter,
    connect(
        (store: IStore) => ({
            isUserAuthorized: store.user.isAuthorized,
            texts: store.i18n.header,
        }),
        {
            navigateTo: navigate,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(({
        isUserAuthorized,
        texts: {
            menu: menuTexts,
            primaryButton: primaryButtonText,
        },
        navigateTo,
        location,
    }): IHeaderComponent => ({
        onLogoClick() {
            event && event.preventDefault()
            navigateTo('/')
        },
        menuAlign: isUserAuthorized ? HEADER_MENU_ALIGN_LEFT : HEADER_MENU_ALIGN_RIGHT,
        menuItems: !isUserAuthorized
            ? ['functionality', 'tariffs', 'blog', 'signUp']
                .map(item => ({
                    key: item,
                    text: menuTexts[item],
                    url: `/${item}`,
                    isActive: !!matchPath(location.pathname, `/${item}`),
                    onClick() {
                        event && event.preventDefault()
                        navigateTo(`/${item}`)
                    },
                }))
            : [],
        primaryButton: !isUserAuthorized ? {
            state: BUTTON_STATE_OUTLINE,
            text: primaryButtonText,
            size: BUTTON_SIZE_SM,
            isBlock: true,
            type: BUTTON_TYPE_LINK,
            url: '/signUp',
            onClick(): void {
                event && event.preventDefault()
                navigateTo('/signIn')
            },
        } : undefined,
    })),
)(Header)

const MainLayoutContainer = compose(
    withRouter,
    connect(
        (store: IStore) => ({
            isUserAuthorized: store.user.isAuthorized,
        }),
        {
            onSetHistory: setHistory,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    lifecycle({
        componentDidMount() {
            const {
                onSetHistory,
                history,
            } = this.props as any

            onSetHistory(history)
        },
    }),
    withProps(({isUserAuthorized}) => ({
        renderHeader: ConnectedHeader,
        isHeaderShadowed: isUserAuthorized,
    })),
)(MainLayout)


const AppContainer = () => (
    <ThemeProvider theme={THEME_LIGHT}>
        <BrowserRouter>
            <MainLayoutContainer>
                <RouterContainer/>
            </MainLayoutContainer>
        </BrowserRouter>
    </ThemeProvider>
)

export default AppContainer