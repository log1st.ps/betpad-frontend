import Form, {
    Field,
    Input,
} from '@betpad/core-web/components/FormComponent'
import { SeptenarySubHeading } from '@betpad/core-web/components/TypographyComponent'
import ButtonsList from '@betpad/core-web/src/assets/components/ButtonsList/ButtonsListComponent'
import { BUTTONS_LIST_JUSTIFY_BETWEEN } from '@betpad/types/assets/components/buttonsList/buttonsListAlignsConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_90,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { BUTTON_TYPE_LINK } from '@betpad/types/assets/elements/button/buttonTypesConstants'
import { COLOR_GRAY_700 } from '@betpad/types/core/colors'
import * as React from 'react'
import compose from 'recompose/compose'

const SignInField = ({
    label,
    value,
    onChange,
}: {
    label?: string,
    value?: string,
    onChange?(value: string): void,
}) => (
    <Field label={label}>
        <Input value={value} onChange={onChange}/>
    </Field>
)

interface ISignInFormContainer {
    emailLabel?: string,
    passwordLabel?: string,

    forgotLabel?: string,
    signUpLabel?: string,

    emailValue?: string,
    passwordValue?: string,

    onSubmit?(): void,

    onForgotClick?(): void,
    onSignUpClick?(): void,

    onChangeField(key: string): (value: string) => void,
}

interface IConnectedSignInFormContainer extends ISignInFormContainer {
}

const SignInFormContainer: React.ComponentClass<ISignInFormContainer> = compose<any, any>(

)(
    ({
        onSubmit,
        emailLabel,
        passwordLabel,

        emailValue,
        passwordValue,

        forgotLabel,
        signUpLabel,

        onChangeField,

        onForgotClick,
        onSignUpClick,
    }: IConnectedSignInFormContainer) => {

        return (
            <Form onSubmit={onSubmit}>
                <SignInField label={emailLabel} value={emailValue} onChange={onChangeField('email')}/>
                <SignInField label={passwordLabel} value={passwordValue} onChange={onChangeField('password')}/>
                <Field>
                    <ButtonsList
                        align={BUTTONS_LIST_JUSTIFY_BETWEEN}
                        buttons={['forgot', 'signUp'].map((item: string) => ({
                            key: item,
                            color: COLOR_GRAY_700 as any,
                            state: BUTTON_STATE_INACTIVE_OPACITY_90 as any,
                            type: BUTTON_TYPE_LINK as any,
                            url: {
                                forgot: '/restorePassword',
                                signUp: '/signUp',
                            }[item],
                            text: () => (
                                <SeptenarySubHeading
                                    content={{
                                        forgot: forgotLabel,
                                        signUp: signUpLabel,
                                    }[item]}
                                />
                            ),
                            onClick: {
                                forgot: onForgotClick,
                                signUp: onSignUpClick,
                            }[item],
                        }))}
                    />
                </Field>
            </Form>
        )
    },
)

export default SignInFormContainer