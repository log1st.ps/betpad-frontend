import Form, {
    Field,
    Input,
} from '@betpad/core-web/components/FormComponent'
import {
    FIELD_STATE_ERROR,
    FIELD_STATE_SUCCESS,
} from '@betpad/types/assets/elements/controls/field/fieldStatesConstants'
import * as React from 'react'

const SignUpField = ({
    label,
    value,
    onChange,
    validation,
}: {
    label?: string,
    value?: string,
    validation?: string[],
    onChange?(): void,
}) => (
    <Field
        label={label}
        hint={validation && validation.join(' ')}
        state={
            validation && validation.length
            ? FIELD_STATE_ERROR
            : FIELD_STATE_SUCCESS
        }
    >
        <Input value={value} onChange={onChange}/>
    </Field>
)

const SignUpFormContainer = ({
    onSubmit,

    firstNameLabel,
    lastNameLabel,
    emailLabel,
    passwordLabel,
    passwordConfirmationLabel,

    firstNameValue,
    lastNameValue,
    emailValue,
    passwordValue,
    passwordConfirmationValue,

    validation,

    onChangeFirstName,
    onChangeLastName,
    onChangeEmail,
    onChangePassword,
    onChangePasswordConfirmation,
}: {
    firstNameLabel?: string,
    lastNameLabel?: string,
    emailLabel?: string,
    passwordLabel?: string,
    passwordConfirmationLabel?: string,

    firstNameValue?: string,
    lastNameValue?: string,
    emailValue?: string,
    passwordValue?: string,
    passwordConfirmationValue?: string,

    validation: {
        [key: string]: string[],
    },

    onChangeFirstName?(): void,
    onChangeLastName?(): void,
    onChangeEmail?(): void,
    onChangePassword?(): void,
    onChangePasswordConfirmation?(): void,

    onSubmit?(): void,
}) => (
    <Form onSubmit={onSubmit}>
        <SignUpField
            label={firstNameLabel}
            value={firstNameValue}
            onChange={onChangeFirstName}
            validation={validation.firstName}
        />
        <SignUpField
            label={lastNameLabel}
            value={lastNameValue}
            onChange={onChangeLastName}
            validation={validation.lastName}
        />
        <SignUpField
            label={emailLabel}
            value={emailValue}
            onChange={onChangeEmail}
            validation={validation.email}
        />
        <SignUpField
            label={passwordLabel}
            value={passwordValue}
            onChange={onChangePassword}
            validation={validation.password}
        />
        <SignUpField
            label={passwordConfirmationLabel}
            value={passwordConfirmationValue}
            onChange={onChangePasswordConfirmation}
            validation={validation.passwordConfirmation}
        />
    </Form>
)

export default SignUpFormContainer