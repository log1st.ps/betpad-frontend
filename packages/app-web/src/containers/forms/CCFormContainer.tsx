import Form, {
    Field,
    Input,
    Select,
} from '@betpad/core-web/components/FormComponent'
import CCFormLayout from '@betpad/core-web/components/layouts/CCFormLayoutComponent'
import { ISelectElement } from '@betpad/types/assets/elements/controls/select/ISelectElement'
import { utc as moment } from 'moment'
import * as React from 'react'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'

const NameField = ({
    label,
    value,
    onChange,
}: {
    label?: string,
    value?: string,
    onChange?(): void,
}) => (
    <React.Fragment>
        <Field label={label}>
            <Input value={value} onChange={onChange}/>
        </Field>
    </React.Fragment>
)

const NumberField = ({
    label,
    value,
    onChange,
}: {
    label?: string,
    value?: string,
    onChange?(): void,
}) => (
    <React.Fragment>
        <Field label={label}>
            <Input placeholder={'1234 1234 1234 1234'} value={value} onChange={onChange}/>
        </Field>
    </React.Fragment>
)

interface IMonthField {
    placeholder?: string,
    value?: string,
    yearValue?: string,
    options?: ISelectElement['values'],
    texts: {
        noData: string,
    },
    onChange?(): void,
}

const MonthField = ({
    placeholder,
    value,
    yearValue,
    onChange,
    options,
    texts: {
        noData: noDataText,
    },
}: IMonthField) => (
    <React.Fragment>
        <Select
            values={options}
            value={value}
            onChange={onChange}
            texts={{
                noData: noDataText,
            }}
            placeholder={placeholder}
        />
    </React.Fragment>
)

const ConnectedMonthField: React.ComponentClass<IMonthField> = compose<any, any>(
    withProps(
        ({value, yearValue}) => {
            const isCurrentYearSelected = String(yearValue) === String(moment().year())
            const currentMonth = String(moment().month())

            return {
                options:
                    Array(12)
                        .fill(null)
                        .map((item: null, i: number) => ({
                            value: i+1,
                            text: i+1,
                        }))
                        .filter(({value: month}) => {
                            return !isCurrentYearSelected || (String(month - 1) >= currentMonth)
                        }),
            }
        },
    ),
)(MonthField)


interface IYearField {
    placeholder?: string,
    value?: string,
    options?: ISelectElement['values'],
    texts: {
        noData: string,
    },
    onChange?(): void,
}


const YearField = ({
    placeholder,
    value,
    onChange,
    options,
    texts: {
        noData: noDataText,
    },
}: IYearField) => (
    <React.Fragment>
        <Select
            values={options}
            value={value}
            onChange={onChange}
            texts={{
                noData: noDataText,
            }}
            placeholder={placeholder}
        />
    </React.Fragment>
)


const ConnectedYearField: React.ComponentClass<IYearField> = compose<any, any>(
    withProps(() => {
        const currentYear = moment().year()

        return {
            options: Array(11).fill(null).map((item: null, i: number) => ({
                value: currentYear + i,
                text: currentYear + i,
            })),
        }
    }),
)(YearField)

const CVCField = ({
    label,
    value,
    onChange,
}: {
    label?: string,
    value?: string,
    onChange?(): void,
}) => (
    <React.Fragment>
        <Field label={label}>
            <Input value={value} onChange={onChange}/>
        </Field>
    </React.Fragment>
)

const CCFormContainer = ({
    onSubmit,
    onNameChange,
    onNumberChange,
    onMonthChange,
    onYearChange,
    onCVCChange,
    nameLabel,
    nameValue,
    numberLabel,
    numberValue,
    expirationLabel,
    monthPlaceholder,
    monthValue,
    yearPlaceholder,
    yearValue,
    cvcLabel,
    cvcValue,
    texts: {
        noData: noDataText,
    },
}: {
    nameLabel?: string,
    nameValue?: string,
    numberLabel?: string,
    numberValue?: string,
    expirationLabel?: string,
    monthPlaceholder?: string,
    monthValue?: string,
    yearPlaceholder?: string,
    yearValue?: string,
    cvcLabel?: string,
    cvcValue?: string,
    texts: {
        noData: string,
    }
    onSubmit?(): void,
    onNameChange?(): void,
    onNumberChange?(): void,
    onMonthChange?(): void,
    onYearChange?(): void,
    onCVCChange?(): void,
}) => (
    <Form onSubmit={onSubmit}>
        <CCFormLayout
            renderNameField={<NameField label={nameLabel} value={nameValue} onChange={onNameChange}/>}
            renderNumberField={<NumberField label={numberLabel} value={numberValue} onChange={onNumberChange}/>}
            expirationLabel={expirationLabel}
            renderExpirationMonthField={<ConnectedMonthField
                placeholder={monthPlaceholder}
                texts={{noData: noDataText}}
                value={monthValue}
                yearValue={yearValue}
                onChange={onMonthChange}
            />}
            renderExpirationYearField={<ConnectedYearField
                texts={{noData: noDataText}}
                placeholder={yearPlaceholder}
                value={yearValue}
                onChange={onYearChange}
            />}
            renderCVCField={<CVCField
                label={cvcLabel}
                value={cvcValue}
                onChange={onCVCChange}
            />}
        />
    </Form>
)

export default CCFormContainer