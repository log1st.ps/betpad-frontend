import * as React from 'react'
import {
    Route,
    Switch,
} from 'react-router'
import BalancesSettingPageContainer from './pages/BalancesSettingPageContainer'
import BookmakersChoosingPageContainer from './pages/BookmakersChoosingPageContainer'
import CCPageContainer from './pages/CCPageContainer'
import IndexPageContainer from './pages/IndexPageContainer'
import ListingPageContainer from './pages/ListingPageContainer'
import SignedUpPageContainer from './pages/SignedUpPageContainer'
import SignInPageContainer from './pages/SignInPageContainer'
import SignUpPageContainer from './pages/SignUpPageContainer'
import SkillChoosingPageContainer from './pages/SkillChoosingPageContainer'
import TariffsPageContainer from './pages/TariffsPageContainer'

const RouterContainer = ({

}) => (
    <React.Fragment>
        <Switch>
            <Route path='/' exact={true} component={IndexPageContainer}/>
            <Route path={'/tariffs'} component={TariffsPageContainer}/>
            <Route path={'/cc'} component={CCPageContainer}/>
            <Route path={'/signUp'} component={SignUpPageContainer}/>
            <Route path={'/signIn'} component={SignInPageContainer}/>
            <Route path={'/signedUp'} component={SignedUpPageContainer}/>
            <Route path={'/bookmakersChoosing'} component={BookmakersChoosingPageContainer}/>
            <Route path={'/balancesSetting'} component={BalancesSettingPageContainer}/>
            <Route path={'/skillChoosing'} component={SkillChoosingPageContainer}/>
            <Route path={'/listing'} component={ListingPageContainer}/>
        </Switch>
    </React.Fragment>
)

export default RouterContainer