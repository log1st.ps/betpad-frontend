import {
    Field,
    Input,
} from '@betpad/core-web/components/FormComponent'
import Image from '@betpad/core-web/components/ImageComponent'
import ButtonsList from '@betpad/core-web/src/assets/components/ButtonsList/ButtonsListComponent'
import BalancesSettingPageLayout from '@betpad/core-web/src/assets/components/layouts/BalancesSettingPageLayout/BalancesSettingPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    SenarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/src/assets/components/Typography/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import {
    IMAGE_BACKGROUND_TYPE_CONTAIN,
} from '@betpad/types/assets/components/image/imageBackgroundTypesConstants'
import { IMAGE_TYPE_BLOCK } from '@betpad/types/assets/components/image/imageTypesConstants'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { FIELD_TYPE_INLINE } from '@betpad/types/assets/elements/controls/field/fieldTypesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import { lifecycle } from 'recompose'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { submitBalances } from '../../actions/pages/balancesSettingPageActions'
import balancesSettingPageReducer, { IBalancesSettingStorePage } from '../../reducers/pages/balancesSettingPageReducer'
import {
    availableBookmakers,
    IBookmaker,
} from '../../services/bookmakers'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'

const bookmakers: {
    [key: string]: IBookmaker,
} = {}

Object.entries(availableBookmakers).map(([key, item]) => {
    bookmakers[item.key] = item
})

const Title = ({title}: {title: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: {subTitle: string}) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

interface IBalancesSettingPage {
    title?: string,
    subTitle?: string,
    cta?: string,
    isCTADisabled?: boolean,
    setBalances?: {
        [key: string]: string,
    },
    chosenBookmakers: string[],
    bookmakerBankValues: {
        [key: string]: string,
    },
    currencyName?: string,
    onInputChange?(key: string): (value: string) => void,
    setSetBalances?(values: {
        [key: string]: string,
    }): void,
    onSubmit?(values: {
        [key: string]: string,
    }): void,
}
const CTA = ({
    text,
    onClick,
    isDisabled,
}: {
    text: string,
    isDisabled: boolean
    onClick?(): void,
}) => (
    <React.Fragment>
        <ButtonsList
            align={BUTTON_ALIGN_CENTER}
            buttons={[
                {
                    key: 'submit',
                    state: BUTTON_STATE_PRIMARY as any,
                    size: BUTTON_SIZE_XL as any,
                    isBlock: true,
                    text: () => (
                        <PrimarySubHeading content={text}/>
                    ),
                    onClick,
                    isDisabled,
                },
            ]}
        />
    </React.Fragment>
)

const Values = ({
    bookmakerBankValues,
    currencyName,
    onChange,
}: {
    bookmakerBankValues: {
        [key: string]: string,
    },
    currencyName: string,
    onChange(value: string): any,
}) => (
    <form>
        {Object.entries(bookmakerBankValues).map(([key, value]) => (
            <Field
                key={key}
                type={FIELD_TYPE_INLINE}
                label={(
                    <React.Fragment>
                        <Image
                            height={16}
                            width={32}
                            type={IMAGE_TYPE_BLOCK}
                            backgroundType={IMAGE_BACKGROUND_TYPE_CONTAIN}
                            url={bookmakers[key].icon}
                        />
                        <SenarySubHeading content={bookmakers[key].title}/>
                    </React.Fragment>
                )}
            >
                <Input
                    value={value}
                    onChange={onChange(key)}
                    suffix={currencyName}
                />
            </Field>
        ))}
    </form>
)

const BalancesSettingPageContainer = compose(
    passStore,
    withAsyncReducer('page', balancesSettingPageReducer),
    withState('setBalances', 'setSetBalances', {}),
    connect(
        (store: IStore & {
            page: IBalancesSettingStorePage,
        }): IBalancesSettingPage => ({
            title: store.i18n.pages.balancesSetting.title,
            subTitle: store.i18n.pages.balancesSetting.subTitle,
            cta: store.i18n.pages.balancesSetting.cta,
            chosenBookmakers: store.user.model.bookmakers,
            bookmakerBankValues: store.user.model.bookmakerBankValues,
            currencyName: store.i18n.other.currency.rub,
        }),
        {
            onSubmit: submitBalances,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    lifecycle({
        componentDidMount() {
            const {
                setSetBalances,
                chosenBookmakers,
                bookmakerBankValues,
            } = this.props as IBalancesSettingPage

            if (setSetBalances && chosenBookmakers) {
                let balances = {}
                if (chosenBookmakers) {
                    chosenBookmakers.forEach(item => {
                        balances[item] = ''
                    })
                }
                if (bookmakerBankValues) {
                    balances = {
                        ...balances,
                        ...bookmakerBankValues,
                    }
                }
                setSetBalances(balances)
            }
        },
    }),
    withProps(
        ({onSubmit, setBalances, setSetBalances}: IBalancesSettingPage) => ({
            onSubmit() {
                if (setBalances && onSubmit) {
                    onSubmit(setBalances as any)
                }
            },
            onInputChange(key: string) {
                return (value: string) => {
                    if (setSetBalances) {
                        setSetBalances({
                            ...setBalances,
                            [key]: value,
                        })
                    }
                }
            },
        }),
    ),
)(
    ({
         title,
         subTitle,
         cta,
         onSubmit,
         isCTADisabled,
         setBalances,
         currencyName,
         onInputChange,
     }: IBalancesSettingPage | any) => (
        <BalancesSettingPageLayout
            renderTitle={<Title title={title}/>}
            renderCTA={<CTA text={cta} onClick={onSubmit} isDisabled={isCTADisabled}/>}
            renderStep={<QuaternarySubHeading content={'2/3'} color={COLOR_GRAY_700}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderValues={(
                <Values
                    currencyName={currencyName}
                    bookmakerBankValues={setBalances}
                    onChange={onInputChange}
                />
            )}
        />
    ),
)


export default BalancesSettingPageContainer