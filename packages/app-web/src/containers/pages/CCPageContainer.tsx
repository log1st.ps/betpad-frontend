import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import CCPageLayout from '@betpad/core-web/components/layouts/CCPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/components/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import withProps from 'recompose/withProps'
import { navigate } from '../../actions/commonActions'
import { setField } from '../../actions/forms/cCFormActions'
import { submitCCForm } from '../../actions/pages/ccPageActions'
import {
    availablePeriods,
    availableTariffs,
} from '../../reducers/userReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'
import CCFormContainer from '../forms/CCFormContainer'
import cCPageReducer from '../../reducers/pages/cCPageReducer'


const Title = ({title}: {title: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: {subTitle: string}) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const CTA = ({
    text,
    onClick,
    isDisabled,
}: {
    text: string,
    isDisabled?: boolean,
    onClick?(): void,
}) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
                isDisabled,
            },
        ]}
    />
)

interface ICCPageContainer {
    title?: string,
    subTitle?: string,
    cta?: string,
    isCTADisabled?: boolean,
    tariffsText?: {[key: string]: string},
    periodsText?: {[key: string]: string},
    tariff?: availableTariffs | null,
    period?: availablePeriods | null,
    nameLabel?: string,
    nameValue?: string,
    numberLabel?: string,
    numberValue?: string,
    expirationLabel?: string,
    monthPlaceholder?: string,
    monthValue?: string,
    yearPlaceholder?: string,
    yearValue?: string,
    cvcLabel?: string,
    cvcValue?: string,
    noDataText?: string,
    onSubmit?(): void,
    navigateTo?(url: string): void,
    onChangeField?(key: string): void,
}

const CCPageContainer = compose(
    passStore,
    withAsyncReducer('page', cCPageReducer),
    connect(
        (store: IStore & {}): ICCPageContainer => ({
            title: store.i18n.pages.cc.title,
            cta: store.i18n.pages.cc.cta,
            tariff: store.user.model.tariff,
            period: store.user.model.period,
            tariffsText: store.i18n.tariffs,
            periodsText: store.i18n.periods,

            nameLabel: store.i18n.pages.cc.form.name,
            numberLabel: store.i18n.pages.cc.form.number,
            expirationLabel: store.i18n.pages.cc.form.expiration,
            monthPlaceholder: store.i18n.pages.cc.form.month,
            yearPlaceholder: store.i18n.pages.cc.form.year,
            cvcLabel: store.i18n.pages.cc.form.cvc,

            nameValue: store.user.model.cc.name,
            numberValue: store.user.model.cc.number,
            monthValue: store.user.model.cc.expiration.month,
            yearValue: store.user.model.cc.expiration.year,
            cvcValue: store.user.model.cc.cvc,

            noDataText: store.i18n.form.select.noData,
        }),
        {
            navigateTo: navigate,
            onChangeField: setField,
            onSubmit: submitCCForm,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(
        ({tariff, tariffsText, period, periodsText}: ICCPageContainer): ICCPageContainer => ({
            subTitle:
                `${tariffsText && tariff ? tariffsText[tariff] : 'N/A'}
                (${periodsText && period ? periodsText[period] : 'N/A'})`,
        }),
    ),
    withProps(({onChangeField}) => ({
        onChangeField: (key: string) => (value: string) => {
            onChangeField({key, value})
        },
    })),
    lifecycle({
        componentWillMount() {
            const {
                navigateTo,
                tariff,
                period,
            } = this.props as ICCPageContainer

            if ((!tariff || !period) && navigateTo) {
                navigateTo('/tariffs')
            }
        },
    }),
)(
    ({
        title,
        subTitle,
        cta,

        isCTADisabled,

        onSubmit,

        nameLabel,
        nameValue,

        numberValue,
        numberLabel,

        expirationLabel,

        monthPlaceholder,
        monthValue,

        yearPlaceholder,
        yearValue,
        
        cvcLabel,
        cvcValue,

        noDataText,
        onChangeField,
    }: ICCPageContainer | any) => (
        <CCPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderCTA={<CTA text={cta} onClick={onSubmit} isDisabled={isCTADisabled}/>}
            renderForm={<CCFormContainer
                onNameChange={onChangeField('name')}
                onNumberChange={onChangeField('number')}
                onMonthChange={onChangeField('month')}
                onYearChange={onChangeField('year')}
                onCVCChange={onChangeField('cvc')}
                onSubmit={onSubmit}
                nameLabel={nameLabel}
                nameValue={nameValue}
                numberLabel={numberLabel}
                numberValue={numberValue}
                expirationLabel={expirationLabel}
                monthPlaceholder={monthPlaceholder}
                monthValue={monthValue}
                yearPlaceholder={yearPlaceholder}
                yearValue={yearValue}
                cvcLabel={cvcLabel}
                cvcValue={cvcValue}
                texts={{
                    noData: noDataText,
                }}
            />}
        />
    ),
)


export default CCPageContainer