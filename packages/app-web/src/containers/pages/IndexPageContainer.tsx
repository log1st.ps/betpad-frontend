import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import CardsList from '@betpad/core-web/components/CardsListComponent'
import LandingPageLayout from '@betpad/core-web/components/layouts/LandingPageLayoutComponent'
import {
    PrimaryHeading,
    PrimarySubHeading,
    QuaternarySubHeading,
} from '@betpad/core-web/components/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { CARD_TYPE_ICON } from '@betpad/types/assets/components/card/cardTypesConstants'
import { ICardComponent } from '@betpad/types/assets/components/card/ICardComponent'
import { CARDS_LIST_ALIGN_CENTER } from '@betpad/types/assets/components/cardsList/cardsListAlignsConstants'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GREEN_DARKER,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { submitCategories } from '../../actions/pages/indexPageActions'
import indexPagesReducer, { IIndexStorePage } from '../../reducers/pages/indexPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'

const Title = ({title}: any) => (
    <PrimaryHeading
        content={title}
        color={COLOR_GREEN_DARKER}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: any) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const Categories = ({cards, onClick}: any) => (
    <CardsList
        align={CARDS_LIST_ALIGN_CENTER}
        cards={
            cards.map((card: any): ICardComponent & {key: string} => ({
                key: card.key,
                icon: card.icon,
                type: CARD_TYPE_ICON,
                label: card.label,
                isActive: card.isActive,
                onClick: onClick(card.key),
            }))
        }
    />
)


const CTA = ({text, onClick, isDisabled}: any) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
                isDisabled,
            },
        ]}
    />
)


const IndexPageContainer = compose(
    withState('selectedCategories', 'setSelectedCategories', []),
    withProps(
        ({
             setSelectedCategories,
             selectedCategories,
        }: any) => ({
        onCategoryClick: (value: string) => () => {
            if (setSelectedCategories) {
                const index = selectedCategories.indexOf(value)

                const newCategories = [...selectedCategories]

                if (index > -1) {
                    newCategories.splice(index, 1)
                }   else {
                    newCategories.push(value)
                }

                setSelectedCategories(newCategories)
            }
        },
    })),

    passStore,
    withAsyncReducer('page', indexPagesReducer),
    connect(
        (store: IStore & {
            page: IIndexStorePage,
        }) => ({
            ...store.i18n.pages.index,
            categories: store.page.categories.map(category => ({
                ...category,
                label: store.i18n.categories[category.key],
            })),
        }),
        {
            onSubmit: submitCategories,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(({categories, selectedCategories}) => ({
        categories: categories.map((category: any) => ({
            ...category,
            isActive: selectedCategories.indexOf(category.key) > -1,
        })),
    })),
    withProps(({onSubmit, selectedCategories}) => ({
        onSubmit: () => {
            onSubmit(selectedCategories)
        },
    })),
)(
    ({title, subTitle, categories, cta, onCategoryClick, selectedCategories, onSubmit}: any) => (
        <LandingPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderCategories={<Categories cards={categories} onClick={onCategoryClick}/>}
            renderCTA={<CTA text={cta} onClick={onSubmit} isDisabled={selectedCategories.length === 0}/>}
        />
    ),
)

export default IndexPageContainer