import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import SignInPageLayout from '@betpad/core-web/components/layouts/SignInPageLayoutComponent'
import {
    PrimarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/components/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { COLOR_GRAY_900 } from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { openSocialAuthorization } from '../../actions/authActions'
import { navigate } from '../../actions/commonActions'
import { setField } from '../../actions/forms/signInFormActions'
import { submitSignInForm } from '../../actions/pages/signInPageActions'
import signInPageReducer, { SignInPageReducer } from '../../reducers/pages/signInPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'
import SocialAuthorizationButtonsList, {
    SOCIAL_FACEBOOK,
    SOCIAL_GOOGLE,
    SOCIAL_VK,
} from '../auth/SocialAuthorizationButtonsList'
import SignInFormContainer from '../forms/SignInFormContainer'

interface ISignInPageContainer {
    title?: string,
    cta?: string,
    isCTADisabled?: boolean,
    emailLabel?: string,
    passwordLabel?: string,
    forgotLabel?: string,
    signUpLabel?: string,
    onSubmit?(): void,
    onSocialClick?(key: string): void,
    navigateTo?(key: string): void,
    onForgotClick?(): void,
    onSignUpClick?(): void,
    onChangeField?(key: string): void,
}

const Title = ({title}: {title: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const CTA = ({
    text,
    onClick,
    isDisabled,
}: {
    text: string,
    isDisabled?: boolean,
    onClick?(): void,
}) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
                isDisabled,
            },
        ]}
    />
)

const SignInPageContainer = compose(
    passStore,
    withAsyncReducer('page', signInPageReducer),
    connect(
        (store: IStore & {
            page: SignInPageReducer,
        }): ISignInPageContainer => ({
            title: store.i18n.pages.signIn.title,
            cta: store.i18n.pages.signIn.cta,

            emailLabel: store.i18n.pages.signIn.form.email,
            passwordLabel: store.i18n.pages.signIn.form.password,
            forgotLabel: store.i18n.pages.signIn.form.forgot,
            signUpLabel: store.i18n.pages.signIn.form.signUp,
        }),
        {
            onSocialClick: openSocialAuthorization,
            onSubmit: submitSignInForm,
            navigateTo: navigate,
            onChangeField: setField,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(({onSocialClick}) => ({
        onSocialClick: (key: string) => () => {
            onSocialClick(key)
        },
    })),
    withProps(
        ({
            navigateTo,
        }: ISignInPageContainer): ISignInPageContainer => ({
            onForgotClick(): void {
                event && event.preventDefault()
                if (navigateTo) {
                    navigateTo('/restorePassword')
                }
            },
            onSignUpClick(): void {
                event && event.preventDefault()
                if (navigateTo) {
                    navigateTo('/signUp')
                }
            },
        }),
    ),
    withProps(({onChangeField}) => ({
        onChangeField: (key: string) => (value: string) => {
            onChangeField({ key, value })
        },
    })),
)(
    ({
        title,
        cta,
        isCTADisabled,
        onSubmit,
        onChangeField,

        emailLabel,
        passwordLabel,
        forgotLabel,
        signUpLabel,

        onSocialClick,

        onSignUpClick,
        onForgotClick,
    }: ISignInPageContainer | any) => (
        <SignInPageLayout
            renderTitle={<Title title={title}/>}
            renderCTA={<CTA text={cta} onClick={onSubmit} isDisabled={isCTADisabled}/>}
            renderForm={<SignInFormContainer
                onChangeField={onChangeField}
                onSubmit={onSubmit}
                emailLabel={emailLabel}
                passwordLabel={passwordLabel}
                forgotLabel={forgotLabel}
                signUpLabel={signUpLabel}
                onSignUpClick={onSignUpClick}
                onForgotClick={onForgotClick}
            />}
            renderSubActions={<SocialAuthorizationButtonsList
                networks={
                    [SOCIAL_VK, SOCIAL_FACEBOOK, SOCIAL_GOOGLE].map(item => ({
                        key: item,
                        type: item as any,
                        onClick: onSocialClick(item),
                    }))
                }
            />}
        />
    ),
)

export default SignInPageContainer