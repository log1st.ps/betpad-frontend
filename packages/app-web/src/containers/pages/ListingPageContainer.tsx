import {
    Datepicker,
    Field,
    Input,
    Slider,
    Switch,
    Toggle,
} from '@betpad/core-web/components/FormComponent'
import ListingPageLayout from '@betpad/core-web/components/layouts/ListingPageLayoutComponent'
import TabsList from '@betpad/core-web/components/TabsListComponent'
import {
    SecondaryCaption,
} from '@betpad/core-web/components/TypographyComponent'
import Accordion from '@betpad/core-web/src/assets/components/Accordion/AccordionComponent'
import Dropdown from '@betpad/core-web/src/assets/components/Dropdown/DropdownComponent'
import Table from '@betpad/core-web/src/assets/components/Table/TableComponent'
import TagsList from '@betpad/core-web/src/assets/components/TagsList/TagsListComponent'
import Button from '@betpad/core-web/src/assets/elements/Button/ButtonElement'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import withForwardedRef from '@betpad/helpers/hocs/withForwardedRef'
import withPopup from '@betpad/helpers/hocs/withPopup'
import { BUTTON_SIZE_MD } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_60,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { IButtonElement } from '@betpad/types/assets/elements/button/IButtonElement'
import { TOGGLE_LABEL_POSITION_AFTER } from '@betpad/types/assets/elements/controls/toggle/toggleLabelPositionsConstants'
import { TOGGLE_TYPE_CHECKBOX } from '@betpad/types/assets/elements/controls/toggle/toggleTypesConstants'
import { ICON_PLUS } from '@betpad/types/assets/elements/icon/iconsConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import { IRenderable } from '@betpad/types/core/IRenderable'
import {
    POPUP_ALIGN_END,
    POPUP_POSITION_BOTTOM,
} from '@betpad/types/hocs/IWithPopup'
import { utc } from 'moment'
import * as React from 'react'
import { ReactNode } from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import listingPageReducer, { IListingStorePage } from '../../reducers/pages/listingPageReducer'
import { availableBookmakers } from '../../services/bookmakers'
import { availableCategories } from '../../services/categories'
import { IStore, passStore, StoreContext } from '../../utilities/store'
import { LISTING_MODE_PREMATCH } from '../../constants/listingModesAndTypesConstants'

const ModeSelector = compose(
    withState('value', 'onChange', LISTING_MODE_PREMATCH),
    connect(
        (store: IStore & {
            page: IListingStorePage,
        }) => ({
            values: store.page.modes.map(mode => ({
                key: mode,
                text: store.i18n.bet.modes[mode],
            })),
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
)(Switch)

const DateSelector = compose(
    withState('value' , 'onChange', utc().unix()),
    withProps(({
        format: 'DD/MM/YYYY',
    })),
)(Datepicker)

const Filter = compose(
    withState('coefficientFrom', 'setCoefficientFrom', 0),
    withState('coefficientTo', 'setCoefficientTo', 100),
    withState('popularity', 'setPopularity', [0, 100]),
    withState('reliability', 'setReliability', [0, 100]),
    withState('bookmakersVisibility', 'setBookmakersVisibility', false),
    withState('selectedBookmakers', 'setSelectedBookmakers', []),
    connect(
        (store: IStore & {
            page: IListingStorePage,
        }) => ({
            texts: {
                coefficient: store.i18n.pages.listing.coefficient,
                from: store.i18n.pages.listing.from,
                to: store.i18n.pages.listing.to,
                popularity: store.i18n.pages.listing.popularity,
                reliability: store.i18n.pages.listing.reliability,
                bookmakers: store.i18n.pages.listing.bookmakers,
                types: store.i18n.pages.listing.types,
            },
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(
        ({
            bookmakersVisibility,
            setBookmakersVisibility,
        }) => ({
            onBookmakersTitleClick() {
                if (setBookmakersVisibility) {
                    setBookmakersVisibility(!bookmakersVisibility)
                }
            },
        }),
    ),
    withProps(
        ({
            selectedBookmakers,
            setSelectedBookmakers,
        }) => ({
            onBookmakerClick(value: any) {
                return () => {
                    if (setSelectedBookmakers) {
                        const index = selectedBookmakers.indexOf(value)
                        const newBookmakers = [...selectedBookmakers]
                        if (index === -1) {
                            newBookmakers.push(value)
                        }   else {
                            newBookmakers.splice(index, 1)
                        }
                        setSelectedBookmakers(newBookmakers)
                    }
                }
            },
        }),
    ),
)(
    ({
        texts: {
            coefficient: coefficientText,
            from: fromText,
            to: toText,
            popularity: popularityText,
            reliability: reliabilityText,
            bookmakers: bookmakersText,
            types: typesText,
        },

        coefficientFrom,
        coefficientTo,
        popularity,
        reliability,

        bookmakersVisibility,
        selectedBookmakers,

        setCoefficientFrom,
        setCoefficientTo,
        setPopularity,
        setReliability,
        
        onBookmakersTitleClick,
        onBookmakerClick,
    }: any) => (
        <React.Fragment>
            <Field
                label={(
                    <SecondaryCaption content={coefficientText}/>
                )}
            >
                <Input
                    hasInputAfter={true}
                    prefix={fromText}
                    onChange={setCoefficientFrom}
                    value={coefficientFrom}
                />
                <Input
                    hasInputBefore={true}
                    prefix={toText}
                    onChange={setCoefficientTo}
                    value={coefficientTo}
                />
            </Field>
            <Field
                label={(
                    <SecondaryCaption content={popularityText}/>
                )}
            >
                <Slider
                    min={0}
                    max={100}
                    step={1}
                    value={popularity}
                    isRange={true}
                    onChange={setPopularity}
                />
            </Field>
            <Field
                label={(
                    <SecondaryCaption content={reliabilityText}/>
                )}
            >
                <Slider
                    min={0}
                    max={100}
                    step={1}
                    value={reliability}
                    isRange={true}
                    onChange={setReliability}
                />
            </Field>
            <br/>
            <Accordion
                label={`${bookmakersText}${selectedBookmakers.length ? `: ${selectedBookmakers.length}` : ''}`}
                isActive={bookmakersVisibility}
                onClick={onBookmakersTitleClick}
            >
                {availableBookmakers.map(({
                    title, key,
                }) => (
                    <Field key={key}>
                        <Toggle
                            type={TOGGLE_TYPE_CHECKBOX}
                            label={title}
                            labelPosition={TOGGLE_LABEL_POSITION_AFTER}
                            onChange={onBookmakerClick(key)}
                            isChecked={selectedBookmakers.indexOf(key) > -1}
                        />
                    </Field>
                ))}
            </Accordion>
        </React.Fragment>
    ), 
)


const AddCategory: any = compose(
    withState('isActive', 'setIsActive', false),
    connect(
        (store: IStore) => ({
            categoriesTitles: store.i18n.categories,
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
    withPopup({
        position: POPUP_POSITION_BOTTOM,
        align: POPUP_ALIGN_END,
        isFixed: true,
        renderContent({ zIndex, ...props }: any): IRenderable {
            const {
                categoriesTitles,
                isActive,
                onChoose,
                setIsActive,
                selected,
            } = props

            const categories = availableCategories
                .filter(({key}) => selected.indexOf(key) === -1)
                .map(({key, icon}) => ({
                    key,
                    onClick: () => {
                        onChoose(key)()
                    },
                    renderLeft: icon,
                    text: categoriesTitles[key],
                }))

            const onOutsideClick = () => {
                if (isActive) {
                    setIsActive(false)
                }
            }

            return () => (
                <div
                    style={{
                        visibility: isActive ? undefined: 'hidden',
                        opacity: isActive ? 1 : 0,
                    }}
                >
                    <Dropdown
                        onOutsideClick={onOutsideClick}
                        items={categories}
                    />
                </div>
            )
        },
    }),
    withForwardedRef,
    withProps(
        ({isActive, setIsActive}): IButtonElement => ({
            onClick: () => {
                setIsActive(!isActive)
            },
            renderLeft: ICON_PLUS,
            isSquare: true,
            size: BUTTON_SIZE_MD,
            color: isActive ? COLOR_GRAY_900 : COLOR_GRAY_700,
            state: BUTTON_STATE_INACTIVE_OPACITY_60 as any,
        }),
    ),
)(Button)

const CategoriesControl = compose(
    withState('value', 'setValue', []),
    connect(
        (store: IStore) => ({
            categories: store.i18n.categories,
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(
        ({value, categories}) => ({
            items: value.map((item: any) => ({
                value: item,
                label: categories[item] || item,
                icon: `sport-${item}`,
            })),
        }),
    ),
    withProps(
        ({setValue, value}) => ({
            onClick(val: string) {
                if (setValue) {
                    setValue(
                        value.filter((item: string) => item !== val),
                    )
                }
            },
            onChoose(val: string) {
                return () => {
                    if (setValue) {
                        const newValues: any[] = [...value]
                        if (newValues.indexOf(val) > -1) {
                            setValue(newValues.filter((item: string) => item !== val))
                        }   else {
                            newValues.push(val)
                            setValue(newValues)
                        }
                    }
                }
            },
        }),
    ),
)(
    ({
        items, onClick, onChoose, value,
     }: any) => (
        <React.Fragment>
            <TagsList items={items} onClick={onClick} />
            <AddCategory selected={value} onChoose={onChoose}/>
        </React.Fragment>
    ),
)

const Tabs = compose(
    withState('value', 'onClick', 'profitable'),
    connect(
        (store: IStore) => ({
            tabsTitles: store.i18n.pages.listing.tabs,
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(
        (({tabsTitles}) => ({
            items: ['profitable', 'reliable', 'optimistic', 'popular'].map(item => ({
                value: item,
                label: tabsTitles[item],
            })),
        })),
    ),
)(TabsList)

const BetsTable = compose(
    connect(
        (store: IStore) => ({
            titles: store.i18n.pages.listing.titles,
        }),
        {},
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(
        ({titles}) => ({
            titles: ['event', 'bet', 'popularity', 'reliability', 'coefficient', null].map((item) => ({
                key: item || '_toggle',
                title: item && titles[item],
            })),
        }),
    ),
    withProps(
        ({}) => ({
            rows: [],
        }),
    ),
)(Table)

interface IListingPageContainer {
    children?: ReactNode,
}

const ListingPageContainer = compose(
    passStore,
    withAsyncReducer('page', listingPageReducer),
)(
    ({

     }: IListingPageContainer) => (
        <ListingPageLayout
            renderMode={ModeSelector}
            renderDatepicker={DateSelector}
            renderCategories={CategoriesControl}
            renderFilters={Filter}
            renderTabs={Tabs}
            renderTable={BetsTable}
        />
    ),
)

export default ListingPageContainer