import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import CardsList from '@betpad/core-web/components/CardsListComponent'
import BookmakersChoosingPageLayout from '@betpad/core-web/components/layouts/BookmakersChoosingPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/components/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import {
    CARD_TYPE_LOGO,
} from '@betpad/types/assets/components/card/cardTypesConstants'
import { CARDS_LIST_ALIGN_CENTER } from '@betpad/types/assets/components/cardsList/cardsListAlignsConstants'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    availableColors,
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import { lifecycle } from 'recompose'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { submitBookmakers } from '../../actions/pages/bookmakersChoosingPageActions'
import bookmakersChoosingPagesReducer, {
    IBookmakersChoosingStorePage, 
} from '../../reducers/pages/bookmakersChoosingPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'

const Title = ({title}: {title: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: any) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const Bookmakers = ({
    cards,
    onClick,
    selectedBookmakers,
}: {
    cards: IBookmakersChoosingPageContainer['bookmakers'],
    selectedBookmakers: string[],
    onClick(key: string): () => void,
}) => (
    <CardsList
        align={CARDS_LIST_ALIGN_CENTER}
        cards={
            cards ? cards.map(card => ({
                key: card.key,
                logo: selectedBookmakers.indexOf(card.key) > -1 ? card.logoActive : card.logo,
                type: CARD_TYPE_LOGO as any,
                isActive: card.isActive,
                onClick: onClick(card.key),
                logoBackground: card.background,
            })): []
        }
    />
)


const CTA = ({text, onClick, isDisabled}: any) => (
    <React.Fragment>
        <ButtonsList
            align={BUTTON_ALIGN_CENTER}
            buttons={[
                {
                    key: 'submit',
                    state: BUTTON_STATE_PRIMARY as any,
                    size: BUTTON_SIZE_XL as any,
                    isBlock: true,
                    text: () => (
                        <PrimarySubHeading content={text}/>
                    ),
                    onClick,
                    isDisabled,
                },
            ]}
        />
    </React.Fragment>
)

interface IBookmakersChoosingPageContainer {
    title?: string,
    subTitle?: string,
    cta?: string,
    selectedBookmakers?: string[],
    bookmakers?: Array<{
        key: string,
        logo: string,
        logoActive: string,
        isActive: boolean,
        background: availableColors,
    }>,
    preselectedBookmakers?: string[],
    onSubmit?(values: string[]): void,
    setSelectedBookmakers?(value: string[]): void,
    onBookmakerClick?(key: string): void,
}


const BookmakersChoosingPageContainer = compose(
    withState('selectedBookmakers', 'setSelectedBookmakers', []),
    withProps(
        ({
             setSelectedBookmakers,
             selectedBookmakers,
        }: IBookmakersChoosingPageContainer): IBookmakersChoosingPageContainer => ({
        onBookmakerClick: (value: string) => () => {
            if (selectedBookmakers && setSelectedBookmakers) {
                const index = selectedBookmakers.indexOf(value)

                const newBookmakers = [...selectedBookmakers]

                if (index > -1) {
                    newBookmakers.splice(index, 1)
                }   else {
                    newBookmakers.push(value)
                }

                setSelectedBookmakers(newBookmakers)
            }
        },
    })),

    passStore,
    withAsyncReducer('page', bookmakersChoosingPagesReducer),
    connect(
        (store: IStore & {
            page: IBookmakersChoosingStorePage,
        }): IBookmakersChoosingPageContainer => ({
            title: store.i18n.pages.bookmakersChoosing.title,
            subTitle: store.i18n.pages.bookmakersChoosing.subTitle,
            cta: store.i18n.pages.bookmakersChoosing.cta,
            bookmakers: store.page.bookmakers.map((bookmaker: any) => ({
                ...bookmaker,
            })),
            preselectedBookmakers: store.user.model.bookmakers,
        }),
        {
            onSubmit: submitBookmakers,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    lifecycle({
        componentDidMount() {
            const {
                preselectedBookmakers,
                setSelectedBookmakers,
            } = this.props as IBookmakersChoosingPageContainer

            if (setSelectedBookmakers) {
                setSelectedBookmakers(preselectedBookmakers || [])
            }
        },
    }),
    withProps(
        ({
             bookmakers,
             selectedBookmakers,
        }: IBookmakersChoosingPageContainer): IBookmakersChoosingPageContainer => ({
        bookmakers: bookmakers ? bookmakers.map((bookmaker: any) => ({
            ...bookmaker,
            isActive: selectedBookmakers ? selectedBookmakers.indexOf(bookmaker.key) > -1 : false,
        })) : [],
    })),
    withProps(
        ({
             onSubmit,
             selectedBookmakers,
        }: IBookmakersChoosingPageContainer): IBookmakersChoosingPageContainer => ({
            onSubmit: () => {
                if (selectedBookmakers && onSubmit) {
                    onSubmit(selectedBookmakers)
                }
            },
        }),
    ),
)(

    ({
         title,
         subTitle,
         bookmakers,
         cta,
         onBookmakerClick,
         selectedBookmakers,
         onSubmit,
    }: IBookmakersChoosingPageContainer | any) => (
        <BookmakersChoosingPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderBookmakers={<Bookmakers
                selectedBookmakers={selectedBookmakers}
                cards={bookmakers}
                onClick={onBookmakerClick}
            />}
            renderCTA={<CTA text={cta} onClick={onSubmit} isDisabled={selectedBookmakers.length === 0}/>}
            renderStep={<QuaternarySubHeading content={'1/3'} color={COLOR_GRAY_700}/>}
        />
    ),
)

export default BookmakersChoosingPageContainer