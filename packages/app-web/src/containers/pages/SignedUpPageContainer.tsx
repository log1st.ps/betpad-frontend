import ButtonsList from '@betpad/core-web/src/assets/components/ButtonsList/ButtonsListComponent'
import ResultPageLayout from '@betpad/core-web/src/assets/components/layouts/ResultPageLayout/ResultPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/src/assets/components/Typography/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import { navigate } from '../../actions/commonActions'
import signedUpPageReducer, { SignedUpPageReducer } from '../../reducers/pages/signedUpPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'

interface ISignedUpPageContainer {
    title?: string,
    subTitle?: string,
    cta?: string,
    onCTAClick?(): void,
}


const Title = ({title}: {title?: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: {subTitle?: string}) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const CTA = ({
    text,
    onClick,
}: {
    text?: string,
    onClick?(): void,
}) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
            },
        ]}
    />
)

const SignedUpPageContainer: React.ComponentClass<ISignedUpPageContainer> = compose<any, any>(
    passStore,
    withAsyncReducer('page', signedUpPageReducer),
    connect(
        (store: IStore & {
            page: SignedUpPageReducer,
        }): ISignedUpPageContainer => ({
            title: store.i18n.pages.signedUp.title,
            subTitle: store.i18n.pages.signedUp.subTitle,
            cta: store.i18n.pages.signedUp.cta,
        }),
        {
            onCTAClick: navigate,
        },
        null,
        {
            context: StoreContext,
        },
    ),
)(
    ({
        title,
        subTitle,
        onCTAClick,
        cta,
    }: ISignedUpPageContainer) => (
        <ResultPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderCTA={<CTA text={cta} onClick={onCTAClick}/>}
        />
    ),
)

export default SignedUpPageContainer