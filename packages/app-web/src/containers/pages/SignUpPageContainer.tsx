import SignUpPageLayout from '@betpad/core-web/components/layouts/SignUpPageLayoutComponent'
import ButtonsList from '@betpad/core-web/src/assets/components/ButtonsList/ButtonsListComponent'
import {
    PrimarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/src/assets/components/Typography/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { COLOR_GRAY_900 } from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import { lifecycle } from 'recompose'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import { openSocialAuthorization } from '../../actions/authActions'
import { setField } from '../../actions/forms/signUpFormActions'
import {
    setSignUpValidation,
    submitSignUpForm,
} from '../../actions/pages/signUpPageActions'
import signUpPageReducer, { SignUpPageReducer } from '../../reducers/pages/signUpPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'
import SocialAuthorizationButtonsList, {
    SOCIAL_FACEBOOK,
    SOCIAL_GOOGLE,
    SOCIAL_VK,
} from '../auth/SocialAuthorizationButtonsList'
import SignUpFormContainer from '../forms/SignUpFormContainer'

const Title = ({title}: {title: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const CTA = ({
    text,
    onClick,
    isDisabled,
}: {
    text: string,
    isDisabled?: boolean,
    onClick?(): void,
}) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
                isDisabled,
            },
        ]}
    />
)

interface ISignUpPageContainer {
    title?: string,
    cta?: string,

    firstNameLabel: string,
    lastNameLabel: string,
    emailLabel: string,
    passwordLabel: string,
    passwordConfirmationLabel: string,

    firstNameValue: string,
    lastNameValue: string,
    emailValue: string,
    passwordValue: string,
    passwordConfirmationValue: string,

    isCTADisabled?: boolean,

    validation?: {},

    onSubmit?(): void,
    onChangeField?(key: string): void,
    onSocialClick?(key: string): void,
}

const SignUpPageContainer = compose(
    passStore,
    withAsyncReducer('page', signUpPageReducer),
    connect(
        (store: IStore & {
            page: SignUpPageReducer,
        }): ISignUpPageContainer => ({
            title: store.i18n.pages.signUp.title,
            cta: store.i18n.pages.signUp.cta,
            firstNameLabel: store.i18n.pages.signUp.form.firstName,
            lastNameLabel: store.i18n.pages.signUp.form.lastName,
            emailLabel: store.i18n.pages.signUp.form.email,
            passwordLabel: store.i18n.pages.signUp.form.password,
            passwordConfirmationLabel: store.i18n.pages.signUp.form.passwordConfirmation,

            firstNameValue: store.user.model.info.firstName,
            lastNameValue: store.user.model.info.lastName,
            emailValue: store.user.model.info.email,
            passwordValue: store.user.model.info.password,
            passwordConfirmationValue: store.user.model.info.passwordConfirmation,

            validation: store.page.validation,
        }),
        {
            onChangeField: setField,
            onSocialClick: openSocialAuthorization,
            onSubmit: submitSignUpForm,
            setValidation: setSignUpValidation,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(({onChangeField, onSocialClick}) => ({
        onChangeField: (key: string) => (value: string) => {
            onChangeField({key, value})
        },
        onSocialClick: (key: string) => () => {
            onSocialClick(key)
        },
    })),
    lifecycle({
        componentDidMount() {
            const {
                setValidation,
            } = this.props as any
            setValidation({})
        },
    }),
)(
    ({
        title,
        cta,
        isCTADisabled,

        onChangeField,
        onSubmit,
        onSocialClick,

        firstNameLabel,
        lastNameLabel,
        emailLabel,
        passwordLabel,
        passwordConfirmationLabel,

        firstNameValue,
        lastNameValue,
        emailValue,
        passwordValue,
        passwordConfirmationValue,

        validation,
    }: ISignUpPageContainer | any) => (
        <SignUpPageLayout
            renderTitle={<Title title={title}/>}
            renderCTA={<CTA text={cta} isDisabled={isCTADisabled} onClick={onSubmit}/>}
            renderForm={<SignUpFormContainer
                onSubmit={onSubmit}

                validation={validation}

                firstNameLabel={firstNameLabel}
                lastNameLabel={lastNameLabel}
                emailLabel={emailLabel}
                passwordConfirmationLabel={passwordConfirmationLabel}
                passwordLabel={passwordLabel}

                firstNameValue={firstNameValue}
                lastNameValue={lastNameValue}
                emailValue={emailValue}
                passwordValue={passwordValue}
                passwordConfirmationValue={passwordConfirmationValue}

                onChangeFirstName={onChangeField('firstName')}
                onChangeLastName={onChangeField('lastName')}
                onChangeEmail={onChangeField('email')}
                onChangePassword={onChangeField('password')}
                onChangePasswordConfirmation={onChangeField('passwordConfirmation')}
            />}
            renderSubActions={<SocialAuthorizationButtonsList
                networks={
                    [SOCIAL_VK, SOCIAL_FACEBOOK, SOCIAL_GOOGLE].map(item => ({
                        key: item,
                        type: item as any,
                        onClick: onSocialClick(item),
                    }))
                }
            />}
        />
    ),
)

export default SignUpPageContainer