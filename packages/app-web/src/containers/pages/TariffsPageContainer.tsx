import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import CardsList from '@betpad/core-web/components/CardsListComponent'
import TariffsPageLayout from '@betpad/core-web/components/layouts/TariffsPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/components/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import {
    CARD_TYPE_TEXT,
} from '@betpad/types/assets/components/card/cardTypesConstants'
import { ICardComponent } from '@betpad/types/assets/components/card/ICardComponent'
import { CARDS_LIST_ALIGN_CENTER } from '@betpad/types/assets/components/cardsList/cardsListAlignsConstants'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { connect } from 'react-redux'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { submitSubscription } from '../../actions/pages/tariffsPageActions'
import tariffsPageReducer, { ITariffsStorePage } from '../../reducers/pages/tariffsPageReducer'
import formatMoney, { CURRENCY_RUR } from '../../utilities/formatMoney'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'

import Form, {
    Field,
    Select,
} from '@betpad/core-web/components/FormComponent'

const Title = ({title}: any) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: any) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const Tariffs = ({cards, onClick}: any) => (
    <CardsList
        align={CARDS_LIST_ALIGN_CENTER}
        cards={
            cards.map((card: any): ICardComponent & {key: string, width?: number} => ({
                key: card.key,
                value: card.value,
                type: CARD_TYPE_TEXT,
                label: card.label,
                isActive: card.isActive,
                onClick: onClick(card.key),
                width: 160,
            }))
        }
    />
)

const SelectPeriodForm = ({
    onSubmit,
    texts: {
        label,
        noData,
        placeholder,
    },
    value,
    values,
    onChange,
}: any) => (
    <Form
        onSubmit={onSubmit}
    >
        <Field label={label}>
            <Select
                texts={{
                    noData,
                }}
                value={value}
                values={values}
                onChange={onChange}
                placeholder={placeholder}
            />
        </Field>
    </Form>
)

const CTA = ({text, onClick, isDisabled}: any) => (
    <ButtonsList
        align={BUTTON_ALIGN_CENTER}
        buttons={[
            {
                key: 'submit',
                state: BUTTON_STATE_PRIMARY as any,
                size: BUTTON_SIZE_XL as any,
                isBlock: true,
                text: () => (
                    <PrimarySubHeading content={text}/>
                ),
                onClick,
                isDisabled,
            },
        ]}
    />
)

const TariffsPageContainer = compose(
    withState('selectedTariff', 'setSelectedTariff', null),
    withState('selectedPeriod', 'setSelectedPeriod', null),
    withProps(
        ({
            selectedTariff,
            setSelectedTariff,
        }: any) => ({
            onTariffClick: (value: string) => () => {
                if (setSelectedTariff) {
                    setSelectedTariff(value === selectedTariff ? null : value)
                }
            },
        }),
    ),
    passStore,
    withAsyncReducer('page', tariffsPageReducer),
    connect(
        (store: IStore & {
            page: ITariffsStorePage,
        }) => ({
            title: store.i18n.pages.tariffs.title,
            subTitle: store.i18n.pages.tariffs.subTitle,
            cta: store.i18n.pages.tariffs.cta,
            tariffs: store.page.tariffs.map(tariff => ({
                ...tariff,
                label: store.i18n.tariffs[tariff.key],
            })),
            periods: store.page.periods.map(period => ({
                ...period,
                label: store.i18n.periods[period.key],
            })),
            periodLabel: store.i18n.pages.tariffs.periodLabel,
            noDataLabel: store.i18n.form.select.noData,
            selectPlaceholder: store.i18n.form.select.placeholder,
        }),
        {
            onSubmit: submitSubscription,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    withProps(({tariffs, selectedTariff}) => ({
        tariffs: tariffs.map((tariff: any) => ({
            ...tariff,
            isActive: tariff.key === selectedTariff,
            value: formatMoney(tariff.value, CURRENCY_RUR),
        })),
    })),
    withProps(({onSubmit, selectedTariff, selectedPeriod}) => ({
        onSubmit: () => {
            onSubmit(selectedTariff, selectedPeriod)
        },
    })),
    withProps(({setSelectedPeriod}) => ({
        onChange: (value: string) => {
            if (setSelectedPeriod) {
                setSelectedPeriod(value)
            }
        },
    })),
    withProps(({selectedPeriod, selectedTariff}) => ({
        isCTADisabled: !selectedTariff || !selectedPeriod,
    })),
)(
    ({
         title,
         subTitle,
         selectedPeriod,
         tariffs,
         cta,
         isCTADisabled,
         onSubmit,
         periods,
         onTariffClick,
         periodLabel,
         noDataLabel,
         onChange,
         selectPlaceholder,
    }: any) => (
        <TariffsPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderTariffs={<Tariffs cards={tariffs} onClick={onTariffClick}/>}
            renderCTA={<CTA text={cta} isDisabled={isCTADisabled} onClick={onSubmit}/>}
            renderForm={<SelectPeriodForm
                onSubmit={onSubmit}
                values={periods.map((period: any) => ({
                    value: period.key,
                    text: period.label,
                }))}
                value={selectedPeriod}
                texts={{
                    label: periodLabel,
                    noData: noDataLabel,
                    placeholder: selectPlaceholder,
                }}
                onChange={onChange}
            />}
        />
    ),
)

export default TariffsPageContainer