import {
    Field,
    Toggle,
} from '@betpad/core-web/components/FormComponent'
import ButtonsList from '@betpad/core-web/src/assets/components/ButtonsList/ButtonsListComponent'
import SkillChoosingPageLayout
    from '@betpad/core-web/src/assets/components/layouts/SkillChoosingPageLayout/SkillChoosingPageLayoutComponent'
import {
    PrimarySubHeading,
    QuaternarySubHeading,
    TertiaryHeading,
} from '@betpad/core-web/src/assets/components/Typography/TypographyComponent'
import withAsyncReducer from '@betpad/helpers/hocs/withAsyncReducer'
import { TYPOGRAPHY_TEXT_ALIGN_CENTER } from '@betpad/types/assets/components/typography/typographyTextAlignsConstants'
import { BUTTON_ALIGN_CENTER } from '@betpad/types/assets/elements/button/buttonAlignsConstants'
import { BUTTON_SIZE_XL } from '@betpad/types/assets/elements/button/buttonSizesConstants'
import { BUTTON_STATE_PRIMARY } from '@betpad/types/assets/elements/button/buttonStatesConstants'
import { TOGGLE_LABEL_POSITION_AFTER } from '@betpad/types/assets/elements/controls/toggle/toggleLabelPositionsConstants'
import { TOGGLE_TYPE_RADIOBUTTON } from '@betpad/types/assets/elements/controls/toggle/toggleTypesConstants'
import {
    COLOR_GRAY_700,
    COLOR_GRAY_900,
} from '@betpad/types/core/colors'
import * as React from 'react'
import { ReactNode } from 'react'
import { connect } from 'react-redux'
import { lifecycle } from 'recompose'
import compose from 'recompose/compose'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import { submitSkill } from '../../actions/pages/skillChoosingPageActions'
import skillChoosingPageReducer, { ISkillChoosingStorePage } from '../../reducers/pages/skillChoosingPageReducer'
import {
    IStore,
    passStore,
    StoreContext,
} from '../../utilities/store'


const Title = ({title}: {title?: string}) => (
    <TertiaryHeading
        content={title}
        color={COLOR_GRAY_900}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const SubTitle = ({subTitle}: {subTitle?: string}) => (
    <QuaternarySubHeading
        content={subTitle}
        color={COLOR_GRAY_700}
        textAlign={TYPOGRAPHY_TEXT_ALIGN_CENTER}
    />
)

const CTA = ({
    text,
    onClick,
    isDisabled,
}: {
    text?: string,
    isDisabled?: boolean
    onClick?(): void,
}) => (
    <React.Fragment>
        <ButtonsList
            align={BUTTON_ALIGN_CENTER}
            buttons={[
                {
                    key: 'submit',
                    state: BUTTON_STATE_PRIMARY as any,
                    size: BUTTON_SIZE_XL as any,
                    isBlock: true,
                    text: () => (
                        <PrimarySubHeading content={text}/>
                    ),
                    onClick,
                    isDisabled,
                },
            ]}
        />
    </React.Fragment>
)

const Values = ({
    skills,
    selectedSkill,
    onChange,
}: {
    skills?: Array<{
        key: string,
        label: string,
    }>,
    selectedSkill?: string,
    onChange?(key: string): () => void,
}) => (
    <form>
        {skills && skills.map(({key, label}) => (
            <Field key={key}>
                <Toggle
                    type={TOGGLE_TYPE_RADIOBUTTON}
                    label={label}
                    labelPosition={TOGGLE_LABEL_POSITION_AFTER}
                    isChecked={selectedSkill === key}
                    onChange={onChange && onChange(key)}
                />
            </Field>
        ))}
    </form>
)

interface ISkillChoosingPageContainer {
    title?: string,
    subTitle?: string,
    cta?: string,
    skills?: Array<{
        key: string,
        label: string,
    }>,
    selectedSkill?: string,
    skill?: string,
    isCTADisabled?: boolean,
    children?: ReactNode,
    onSkillSelect?(key: string): () => void,
    onSubmit?(): void,
    setSelectedSkill?(skill: string): string,
}

const SkillChoosingPageContainer = compose(
    withState('selectedSkill', 'setSelectedSkill', null),
    passStore,
    withAsyncReducer('page', skillChoosingPageReducer),
    connect(
        (store: IStore & {
            page: ISkillChoosingStorePage,
        }): ISkillChoosingPageContainer => ({
            title: store.i18n.pages.skillChoosing.title,
            subTitle: store.i18n.pages.skillChoosing.subTitle,
            cta: store.i18n.pages.skillChoosing.cta,

            skills: store.page.skills.map(skill => ({
                key: skill,
                label: store.i18n.skills[skill],
            })),

            skill: store.user.model.skill,
        }),
        {
            onSubmit: submitSkill,
        },
        null,
        {
            context: StoreContext,
        },
    ),
    lifecycle(({
        componentDidMount() {
            const {
                setSelectedSkill,
                skill,
            } = this.props as ISkillChoosingPageContainer
            if (setSelectedSkill && skill) {
                setSelectedSkill(skill)
            }
        },
    })),
    withProps(
        ({
            onSubmit,
            selectedSkill,
        }): ISkillChoosingPageContainer => ({
            onSubmit() {
                if (onSubmit && selectedSkill)  {
                    onSubmit(selectedSkill)
                }
            },
        }),
    ),
    withProps(
        ({
            setSelectedSkill,
        }: ISkillChoosingPageContainer): ISkillChoosingPageContainer => ({
            onSkillSelect(key: string) {
                return () => {
                    if (setSelectedSkill) {
                        setSelectedSkill(key)
                    }
                }
            },
        }),
    ),
)(
    ({
        title,
        subTitle,
        cta,
        isCTADisabled,
        onSubmit,
        selectedSkill,
        skills,
        onSkillSelect,
    }: ISkillChoosingPageContainer) => (
        <SkillChoosingPageLayout
            renderTitle={<Title title={title}/>}
            renderSubTitle={<SubTitle subTitle={subTitle}/>}
            renderCTA={<CTA text={cta} isDisabled={isCTADisabled} onClick={onSubmit}/>}
            renderStep={'3/3'}
            renderValues={<Values skills={skills} selectedSkill={selectedSkill} onChange={onSkillSelect}/>}
        />
    ),
)

export default SkillChoosingPageContainer