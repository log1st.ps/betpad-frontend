import ButtonsList from '@betpad/core-web/components/ButtonsListComponent'
import { BUTTONS_LIST_JUSTIFY_CENTER } from '@betpad/types/assets/components/buttonsList/buttonsListAlignsConstants'
import {
    BUTTON_SIZE_XL,
} from '@betpad/types/assets/elements/button/buttonSizesConstants'
import {
    BUTTON_STATE_INACTIVE_OPACITY_40,
} from '@betpad/types/assets/elements/button/buttonStatesConstants'
import {
    ICON_SOCIAL_FACEBOOK,
    ICON_SOCIAL_GOOGLE,
    ICON_SOCIAL_VK,
} from '@betpad/types/assets/elements/icon/iconsConstants'
import {
    COLOR_GRAY_800,
} from '@betpad/types/core/colors'
import * as React from 'react'

export const SOCIAL_VK = 'vk'
export const SOCIAL_FACEBOOK = 'facebook'
export const SOCIAL_GOOGLE = 'google'

const SocialAuthorizationButtonsList = ({
    networks,
}: {
    networks?: Array<{
        key: string,
        type:
            typeof SOCIAL_VK
            | typeof SOCIAL_FACEBOOK
            | typeof SOCIAL_GOOGLE,
        onClick?(): void,
    }>,
}) => (
    <ButtonsList
        align={BUTTONS_LIST_JUSTIFY_CENTER}
        buttons={networks ? networks.map(({
            key,
            type,
            onClick,
        }) => ({
            key,
            renderLeft: {
                [SOCIAL_VK]: ICON_SOCIAL_VK,
                [SOCIAL_FACEBOOK]: ICON_SOCIAL_FACEBOOK,
                [SOCIAL_GOOGLE]: ICON_SOCIAL_GOOGLE,
            }[type],
            onClick,
            size: BUTTON_SIZE_XL as any,
            state: BUTTON_STATE_INACTIVE_OPACITY_40 as any,
            isSquare: true,
            color: COLOR_GRAY_800 as any,
        })) : []}
    />
)

export default SocialAuthorizationButtonsList