import * as React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import AppContainer from './containers/AppContainer'
import store, { StoreContext } from './utilities/store'

ReactDOM.render(
    <Provider context={StoreContext} store={store}>
        <AppContainer/>
    </Provider>,
    document.getElementById('root'),
)
