import {
    availableColors,
    COLOR_BLACK,
    COLOR_BLUE,
    COLOR_BLUE_DEEP,
    COLOR_GREEN_DARKER,
    COLOR_RED,
} from '@betpad/types/core/colors'


const onexbetLogo = require('../images/bookmakers/1xbet.png')
const onexbetLogoActive = require('../images/bookmakers/1xbet-active.png')
const tripleEightLogo = require('../images/bookmakers/888.png')
const tripleEightLogoActive = require('../images/bookmakers/888-active.png')
const bet365Logo = require('../images/bookmakers/bet365.png')
const bet365LogoActive = require('../images/bookmakers/bet365-active.gif')
const betAtHomeLogo = require('../images/bookmakers/bet-at-home.png')
const betAtHomeLogoActive = require('../images/bookmakers/bet-at-home-active.png')
const betCityLogo = require('../images/bookmakers/betcity.png')
const betCityLogoActive = require('../images/bookmakers/betcity-active.png')
const bwinLogo = require('../images/bookmakers/bwin.png')
const bwinLogoActive = require('../images/bookmakers/bwin-active.png')
const fonbetLogo = require('../images/bookmakers/fonbet.png')
const fonbetLogoActive = require('../images/bookmakers/fonbet-active.jpg')
const leonLogo = require('../images/bookmakers/leon.png')
const leonLogoActive = require('../images/bookmakers/leon-active.jpg')
const ligaStavokLogo = require('../images/bookmakers/liga-stavok.png')
const ligaStavokLogoActive = require('../images/bookmakers/liga-stavok-active.png')
const marathonBetLogo = require('../images/bookmakers/marathon-bet.jpg')
const marathonBetLogoActive = require('../images/bookmakers/marathon-bet-active.jpg')
const mostbetLogo = require('../images/bookmakers/mostbet.jpg')
const mostbetLogoActive = require('../images/bookmakers/mostbet-active.jpg')
const olympLogo = require('../images/bookmakers/olymp.png')
const olympLogoActive = require('../images/bookmakers/olymp-active.jpg')
const paddyPowerLogo = require('../images/bookmakers/paddypower.png')
const paddyPowerLogoActive = require('../images/bookmakers/paddypower-active.jpg')
const pariMatchLogo = require('../images/bookmakers/pari-match.jpg')
const pariMatchLogoActive = require('../images/bookmakers/pari-match-active.jpg')
const pinnacleLogo = require('../images/bookmakers/pinnacle.png')
const pinnacleLogoActive = require('../images/bookmakers/pinnacle-active.jpg')
const tennisiLogo = require('../images/bookmakers/tennisi.jpg')
const tennisiLogoActive = require('../images/bookmakers/tennisi-active.jpg')
const williamHillLogo = require('../images/bookmakers/william-hill.png')
const williamHillLogoActive = require('../images/bookmakers/william-hill-active.jpg')
const winlineLogo = require('../images/bookmakers/winline.png')
const winlineLogoActive = require('../images/bookmakers/winline-active.jpg')
const zenitLogo = require('../images/bookmakers/zenit.png')
const zenitLogoActive = require('../images/bookmakers/zenit-active.png')

export interface IBookmaker {
    key: string,
    icon: string,
    logo: string,
    logoActive: string,
    background: availableColors,
    title: string,
}

export const availableBookmakers: IBookmaker[] = [
    {
        key: '1xbet',
        title: '1XBET',
        logo: onexbetLogo,
        icon: onexbetLogo,
        logoActive: onexbetLogoActive,
        background: COLOR_BLUE_DEEP,
    },
    {
        key: '888',
        title: '888',
        logo: tripleEightLogo,
        icon: tripleEightLogo,
        logoActive: tripleEightLogoActive,
        background: COLOR_RED,
    },
    {
        key: 'bet365',
        title: 'Bet 365',
        logo: bet365Logo,
        icon: bet365Logo,
        logoActive: bet365LogoActive,
        background: COLOR_GREEN_DARKER,
    },
    {
        key: 'bet-at-home',
        title: 'Bet At Home',
        logo: betAtHomeLogo,
        icon: betAtHomeLogo,
        logoActive: betAtHomeLogoActive,
        background: COLOR_BLUE_DEEP,
    },
    {
        key: 'betcity',
        title: 'BetCity',
        logo: betCityLogo,
        icon: betCityLogo,
        logoActive: betCityLogoActive,
        background: COLOR_BLUE,
    },
    {
        key: 'bwin',
        title: 'BWin',
        logo: bwinLogo,
        icon: bwinLogo,
        logoActive: bwinLogoActive,
        background: COLOR_BLACK,
    },
    {
        key: 'fonbet',
        title: 'Fonbet',
        logo: fonbetLogo,
        icon: fonbetLogo,
        logoActive: fonbetLogoActive,
        background: COLOR_RED,
    },
    {
        key: 'leon',
        title: 'Leon',
        logo: leonLogo,
        icon: leonLogo,
        logoActive: leonLogoActive,
        background: COLOR_BLACK,
    },
    {
        key: 'liga-stavok',
        title: 'Лига Ставок',
        logo: ligaStavokLogo,
        icon: ligaStavokLogo,
        logoActive: ligaStavokLogoActive,
        background: COLOR_GREEN_DARKER,
    },
    {
        key: 'marathon-bet',
        title: 'Марафон-бет',
        logo: marathonBetLogo,
        icon: marathonBetLogo,
        logoActive: marathonBetLogoActive,
        background: COLOR_RED,
    },
    {
        key: 'mostbet',
        title: 'Мостбет',
        logo: mostbetLogo,
        icon: mostbetLogo,
        logoActive: mostbetLogoActive,
        background: COLOR_BLUE,
    },
    {
        key: 'olymp',
        title: 'Олимп',
        logo: olympLogo,
        icon: olympLogo,
        logoActive: olympLogoActive,
        background: COLOR_RED,
    },
    {
        key: 'paddypower',
        title: 'Paddy Power',
        logo: paddyPowerLogo,
        icon: paddyPowerLogo,
        logoActive: paddyPowerLogoActive,
        background: COLOR_GREEN_DARKER,
    },
    {
        key: 'pari-match',
        title: 'Пари-матч',
        logo: pariMatchLogo,
        icon: pariMatchLogo,
        logoActive: pariMatchLogoActive,
        background: COLOR_BLACK,
    },
    {
        key: 'pinnacle',
        title: 'Pinnacle',
        logo: pinnacleLogo,
        icon: pinnacleLogo,
        logoActive: pinnacleLogoActive,
        background: COLOR_BLACK,
    },
    {
        key: 'tennisi',
        title: 'Tennisi',
        logo: tennisiLogo,
        icon: tennisiLogo,
        logoActive: tennisiLogoActive,
        background: COLOR_RED,
    },
    {
        key: 'william-hill',
        title: 'William Hill',
        logo: williamHillLogo,
        icon: williamHillLogo,
        logoActive: williamHillLogoActive,
        background: COLOR_BLUE_DEEP,
    },
    {
        key: 'winline',
        title: 'Winline',
        logo: winlineLogo,
        icon: winlineLogo,
        logoActive: winlineLogoActive,
        background: COLOR_BLACK,
    },
    {
        key: 'zenit',
        title: 'Зенит',
        logo: zenitLogo,
        icon: zenitLogo,
        logoActive: zenitLogoActive,
        background: COLOR_BLUE,
    },
]