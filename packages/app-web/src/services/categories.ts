import {
    ICON_SPORT_BASKETBALL,
    ICON_SPORT_FIGHTING,
    ICON_SPORT_FOOTBALL,
    ICON_SPORT_GANDBALL,
    ICON_SPORT_HOCKEY,
    ICON_SPORT_TENNIS,
    ICON_SPORT_VOLLEYBALL,
} from '@betpad/types/assets/elements/icon/iconsConstants'
import { IIconElement } from '@betpad/types/assets/elements/icon/IIconElement'

export const availableCategories: Array<{
    key: string,
    icon: IIconElement['icon'],
}> = [
    {
        key: 'football',
        icon: ICON_SPORT_FOOTBALL,
    },
    {
        key: 'hockey',
        icon: ICON_SPORT_HOCKEY,
    },
    {
        key: 'volleyball',
        icon: ICON_SPORT_VOLLEYBALL,
    },
    {
        key: 'basketball',
        icon: ICON_SPORT_BASKETBALL,
    },
    {
        key: 'tennis',
        icon: ICON_SPORT_TENNIS,
    },
    {
        key: 'gandball',
        icon: ICON_SPORT_GANDBALL,
    },
    {
        key: 'fighting',
        icon: ICON_SPORT_FIGHTING,
    },
]