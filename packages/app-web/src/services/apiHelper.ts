import axios from 'axios'

const API_URL = 'http://backend.gobetpad.ru'

export const post = (url: string, payload: object) => {
    return new Promise((resolve, reject) => {
        axios.post(
            `${API_URL}${url}`, {
                payload,
            },
            {
                validateStatus: false,
            } as any,
        )
            .then(({data}) => {
                if (data.status === 200) {
                    resolve(data)
                }   else {
                    reject({...data})
                }
            })
            .catch((res) => {
                console.log(res)
                reject({
                    status: 500,
                    data: {
                        message: 'Error processing request',
                    },
                })
            })
    })
}