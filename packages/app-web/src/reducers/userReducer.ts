import createReducer from '@betpad/helpers/redux/createReducer'
import updateFieldWithPayload from '@betpad/helpers/redux/updateFieldWithPayload'
import { ACTION_BALANCES_SETTING_PAGE_SUBMIT } from '../constants/actions/balancesSettingPageActionsConstants'
import { ACTION_BOOKMAKERS_CHOOSING_PAGE_SUBMIT } from '../constants/actions/bookmakersChoosingPageActionsConstants'
import { ACTION_CC_FORM_SET_FIELD } from '../constants/actions/cCFormActionsConstants'
import { ACTION_SIGN_UP_FORM_SET_FIELD } from '../constants/actions/signUpFormActionsConstants'
import { ACTION_SKILL_CHOOSING_PAGE_SUBMIT } from '../constants/actions/skillChoosingPageActionsConstants'
import { ACTION_TARIFFS_PAGE_SUBMIT } from '../constants/actions/tariffsPageActionsConstants'
import {
    PERIOD_HALF_YEAR,
    PERIOD_MONTH,
    PERIOD_YEAR,
} from '../constants/periodsConstants'
import {
    TARIFF_FORK_LIVE,
    TARIFF_FORK_PREMATCH,
    TARIFF_FREE,
    TARIFF_RATING_LIVE,
    TARIFF_RATING_PREMATCH,
} from '../constants/tariffsConstants'

export type availableTariffs =
    typeof TARIFF_FREE
    | typeof TARIFF_RATING_PREMATCH
    | typeof TARIFF_RATING_LIVE
    | typeof TARIFF_FORK_PREMATCH
    | typeof TARIFF_FORK_LIVE

export type availablePeriods =
    typeof PERIOD_MONTH
    | typeof PERIOD_HALF_YEAR
    | typeof PERIOD_YEAR

export interface IUserReducer {
    isAuthorized: boolean,
    model: {
        tariff: availableTariffs | null,
        period: availablePeriods | null,
        cc: {
            name: string,
            number: string,
            expiration: {
                month: string,
                year: string,
            },
            cvc: string,
        },
        info: {
            firstName: string,
            lastName: string,
            email: string,
            password: string,
            passwordConfirmation: string,
        },
        bookmakers: string[],
        bookmakerBankValues: {
            [key: string]: string,
        },
        skill: string,
    }
}

const initialState: IUserReducer = {
    isAuthorized: false,
    model: {
        tariff: null,
        period: null,
        cc: {
            name: '',
            number: '',
            expiration: {
                month: '',
                year: '',
            },
            cvc: '',
        },
        info: {
            firstName: '',
            lastName: '',
            email: '',
            password: '',
            passwordConfirmation: '',
        },
        bookmakers: [],
        bookmakerBankValues: {},
        skill: '',
    },
}

export default createReducer({
    initialState,
    actions: {
        [ACTION_TARIFFS_PAGE_SUBMIT]: (state: IUserReducer, {tariff, period}: {
                tariff: availableTariffs,
                period: availablePeriods,
        }): IUserReducer => ({
            ...state,
            model: {
                ...state.model,
                tariff,
                period,
            },
        }),
        [ACTION_CC_FORM_SET_FIELD]: (state: IUserReducer, {key, value}: {
            key: string,
            value: any,
        }): IUserReducer => ({
            ...state,
            model: {
                ...state.model,
                cc: {
                    ...state.model.cc,
                    [['month', 'year'].indexOf(key) > -1 ? 'expiration': key]:
                        ['month', 'year'].indexOf(key) > -1 ? {
                        ...state.model.cc.expiration,
                        [key]: value,
                    } : value,
                },
            },
        }),
        [ACTION_SIGN_UP_FORM_SET_FIELD]: (state: IUserReducer, {key, value}: {
            key: string,
            value: string,
        }): IUserReducer => ({
            ...state,
            model: {
                ...state.model,
                info: {
                    ...state.model.info,
                    [key]: value,
                },
            },
        }),
        [ACTION_BOOKMAKERS_CHOOSING_PAGE_SUBMIT]: (state: IUserReducer, payload: any): IUserReducer => ({
            ...state,
            model: updateFieldWithPayload('bookmakers')(state.model, payload),
        }),
        [ACTION_BALANCES_SETTING_PAGE_SUBMIT]: (state: IUserReducer, payload: any): IUserReducer => ({
            ...state,
            model: updateFieldWithPayload('bookmakerBankValues')(state.model, payload),
        }),
        [ACTION_SKILL_CHOOSING_PAGE_SUBMIT]: (state: IUserReducer, payload: string): IUserReducer => ({
            ...state,
            model: updateFieldWithPayload('skill')(state.model, payload),
        }),
    },
})