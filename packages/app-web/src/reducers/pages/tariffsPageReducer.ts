import createReducer from '@betpad/helpers/redux/createReducer'
import {
    PERIOD_HALF_YEAR,
    PERIOD_MONTH,
    PERIOD_YEAR,
} from '../../constants/periodsConstants'
import {
    TARIFF_FORK_LIVE,
    TARIFF_FORK_PREMATCH,
    TARIFF_FREE,
    TARIFF_RATING_LIVE,
    TARIFF_RATING_PREMATCH,
} from '../../constants/tariffsConstants'
import { IStorePage } from '../../utilities/store'

export interface ITariffsStorePage extends IStorePage {
    tariffs: Array<{
        key: string,
        value: number,
    }>,
    periods: Array<{
        key: string,
    }>
}

const initialState: ITariffsStorePage = {
    key: 'tariffs',
    tariffs: [
        {
            key: TARIFF_FREE,
            value: 0,
        },
        {
            key: TARIFF_RATING_PREMATCH,
            value: 300,
        },
        {
            key: TARIFF_RATING_LIVE,
            value: 500,
        },
        {
            key: TARIFF_FORK_PREMATCH,
            value: 800,
        },
        {
            key: TARIFF_FORK_LIVE,
            value: 1000,
        },
    ],
    periods: [
        {
            key: PERIOD_MONTH,
        },
        {
            key: PERIOD_HALF_YEAR,
        },
        {
            key: PERIOD_YEAR,
        },
    ],
}

export default createReducer({
    initialState,
})