import createReducer from '@betpad/helpers/redux/createReducer'
import {
    availableBookmakers,
    IBookmaker,
} from '../../services/bookmakers'
import { IStorePage } from '../../utilities/store'

export interface IBookmakersChoosingStorePage extends IStorePage {
    bookmakers: IBookmaker[]
}

const initialState: IBookmakersChoosingStorePage = {
    key: 'bookmakersChoosing',
    bookmakers: availableBookmakers,
}

export default createReducer({
    initialState,
})