import createReducer from '@betpad/helpers/redux/createReducer'
import { IStorePage } from '../../utilities/store'

export interface IBalancesSettingStorePage extends IStorePage {

}

const initialState: IBalancesSettingStorePage = {
    key: 'balancesSetting',
}

export default createReducer({
    initialState,
})