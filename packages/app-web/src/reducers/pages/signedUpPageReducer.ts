import createReducer from '@betpad/helpers/redux/createReducer'
import { IStorePage } from '../../utilities/store'

export interface SignedUpPageReducer extends IStorePage {

}

const initialState: SignedUpPageReducer = {
    key: 'signedUp',
}

export default createReducer({
    initialState,
})