import createReducer from '@betpad/helpers/redux/createReducer'
import { ACTION_SIGN_IN_FORM_SET_FIELD } from '../../constants/actions/signInFormActionsConstants'
import { IStorePage } from '../../utilities/store'

export interface SignInPageReducer extends IStorePage {
    model: {
        email: string,
        password: string,
    }
}

const initialState: SignInPageReducer = {
    key: 'signIn',
    model: {
        email: '',
        password: '',
    },
}

export default createReducer({
    initialState,
    actions: {
        [ACTION_SIGN_IN_FORM_SET_FIELD]: (
            state: SignInPageReducer,
            {key, value}:{key: string, value: string},
        ): SignInPageReducer => ({
            ...state,
            model: {
                ...state.model,
                [key]: value,
            },
        }),
    },
})