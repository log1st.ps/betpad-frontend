import createReducer from '@betpad/helpers/redux/createReducer'
import { IIconElement } from '@betpad/types/assets/elements/icon/IIconElement'
import { IStorePage } from '../../utilities/store'
import { availableCategories } from '../../services/categories'

export interface IIndexStorePage extends IStorePage {
    categories: Array<{
        key: string,
        icon: IIconElement['icon'],
    }>
}

const initialState: IIndexStorePage = {
    key: 'index',
    categories: availableCategories,
}

export default createReducer({
    initialState,
})