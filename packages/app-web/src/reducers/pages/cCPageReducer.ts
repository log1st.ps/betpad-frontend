import createReducer from '@betpad/helpers/redux/createReducer'
import { IStorePage } from '../../utilities/store'

export interface CCPageReducer extends IStorePage {

}

const initialState: CCPageReducer = {
    key: 'cc',
}

export default createReducer({
    initialState,
})