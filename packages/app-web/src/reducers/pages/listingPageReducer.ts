import createReducer from '@betpad/helpers/redux/createReducer'
import {
    LISTING_MODE_LIVE,
    LISTING_MODE_PREMATCH,
} from '../../constants/listingModesAndTypesConstants'
import { IStorePage } from '../../utilities/store'

export interface IListingStorePage extends IStorePage {
    modes: Array<typeof LISTING_MODE_LIVE | typeof LISTING_MODE_PREMATCH>,
}

const initialState: IListingStorePage = {
    key: 'listing',
    modes: [
        LISTING_MODE_PREMATCH,
        LISTING_MODE_LIVE,
    ],
}

export default createReducer({
    initialState,
})