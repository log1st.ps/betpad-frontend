import createReducer from '@betpad/helpers/redux/createReducer'
import updateFieldWithPayload from '@betpad/helpers/redux/updateFieldWithPayload'
import { ACTION_SIGN_UP_PAGE_VALIDATION } from '../../constants/actions/signUpPageActionsConstants'
import { IStorePage } from '../../utilities/store'

export interface SignUpPageReducer extends IStorePage {
    validation: {
        [key: string]: string[],
    }
}

const initialState: SignUpPageReducer = {
    key: 'signUp',
    validation: {},
}

export default createReducer({
    initialState,
    actions: {
        [ACTION_SIGN_UP_PAGE_VALIDATION]: updateFieldWithPayload('validation'),
    },
})