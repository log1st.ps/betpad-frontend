import createReducer from '@betpad/helpers/redux/createReducer'
import {
    SKILL_NOVICE,
    SKILL_PRO,
    SKILL_RISKY,
    SKILL_SMALL_KNOWLEDGE,
} from '../../constants/skillsConstants'
import { IStorePage } from '../../utilities/store'

export interface ISkillChoosingStorePage extends IStorePage {
    skills: string[]
}

const initialState: ISkillChoosingStorePage = {
    key: 'skillChoosing',
    skills: [
        SKILL_NOVICE,
        SKILL_SMALL_KNOWLEDGE,
        SKILL_RISKY,
        SKILL_PRO,
    ],
}

export default createReducer({
    initialState,
})