import createReducer from '@betpad/helpers/redux/createReducer'
import updateFieldWithPayload from '@betpad/helpers/redux/updateFieldWithPayload'
import { ACTION_COMMON_SET_HISTORY } from '../constants/actions/commonActionsConstants'

export interface ICommonReducer {
    history: any,
}

const initialState: ICommonReducer = {
    history: null,
}

export default createReducer({
    initialState,
    actions: {
        [ACTION_COMMON_SET_HISTORY]: updateFieldWithPayload('history'),
    },
})