import createReducer from '@betpad/helpers/redux/createReducer'
import {
    LISTING_MODE_LIVE,
    LISTING_MODE_PREMATCH,
    LISTING_TYPE_FORK,
    LISTING_TYPE_RATING,
} from '../constants/listingModesAndTypesConstants'
import {
    PERIOD_HALF_YEAR,
    PERIOD_MONTH,
    PERIOD_YEAR,
} from '../constants/periodsConstants'
import {
    SKILL_NOVICE,
    SKILL_PRO,
    SKILL_RISKY,
    SKILL_SMALL_KNOWLEDGE,
} from '../constants/skillsConstants'
import {
    TARIFF_FORK_LIVE,
    TARIFF_FORK_PREMATCH,
    TARIFF_FREE,
    TARIFF_RATING_LIVE,
    TARIFF_RATING_PREMATCH,
} from '../constants/tariffsConstants'

export interface II18N {
    pages: {
        index: {
            title: string,
            subTitle: string,
            cta: string,
        },
        tariffs: {
            title: string,
            subTitle: string,
            cta: string,
            periodLabel: string,
        },
        cc: {
            title: string,
            cta: string,
            form: {
                name: string,
                number: string,
                expiration: string,
                month: string,
                year: string,
                cvc: string,
            },
        },
        signUp: {
            title: string,
            cta: string,
            form: {
                firstName: string,
                lastName: string,
                email: string,
                password: string,
                passwordConfirmation: string,
            },
        },
        signIn: {
            title: string,
            cta: string,
            form: {
                email: string,
                password: string,
                forgot: string,
                signUp: string,
            },
        },
        signedUp: {
            title: string,
            subTitle: string,
            cta: string,
        },
        bookmakersChoosing: {
            title: string,
            subTitle: string,
            cta: string,
        },
        balancesSetting: {
            title: string,
            subTitle: string,
            cta: string,
        },
        skillChoosing: {
            title: string,
            subTitle: string,
            cta: string,
        },
        listing: {
            coefficient: string,
            from: string,
            to: string,
            popularity: string,
            reliability: string,
            bookmakers: string,
            types: string,
            tabs: {
                profitable: string,
                reliable: string,
                optimistic: string,
                popular: string,
            },
            titles: {
                event: string,
                bet: string,
                popularity: string,
                reliability: string,
                coefficient: string,
            },
        },
    },
    categories: {
        [key: string]: string,
    },
    tariffs: {
        [key: string]: string,
    },
    periods: {
        [key: string]: string,
    },
    skills: {
        [key: string]: string,
    },
    header: {
        menu: {
            functionality: string,
            tariffs: string,
            blog: string,
            signUp: string,
        },
        primaryButton: string,
    },
    form: {
        select: {
            noData: string,
            placeholder: string,
        },
    },
    other: {
        currency: {
            [key: string]: string,
        },
    },
    bet: {
        modes: {
            [LISTING_MODE_PREMATCH]: string,
            [LISTING_MODE_LIVE]: string,
        },
        types: {
            [LISTING_TYPE_RATING]: string,
            [LISTING_TYPE_FORK]: string,
        },
    }

}

const initialState: II18N = {
    pages: {
        index: {
            title: 'Поиск и управление ставками',
            subTitle: 'Лучший способ улучшить свой банк',
            cta: 'Найти',
        },
        tariffs: {
            title: 'Тарифы',
            subTitle: 'Выберите подходящий тариф',
            cta: 'Выбрать',
            periodLabel: 'Срок подписки',
        },
        cc: {
            title: 'Подписка',
            cta: 'Оплатить',
            form: {
                name: 'Имя владельца карты',
                number: 'Номер карты',
                expiration: 'Срок действия',
                month: 'Месяц',
                year: 'Год',
                cvc: 'CVC/CVV',
            },
        },
        signUp: {
            title: 'Регистрация',
            cta: 'Зарегистрироваться',
            form: {
                firstName: 'Имя',
                lastName: 'Фамилия',
                email: 'Email',
                password: 'Пароль',
                passwordConfirmation: 'Подтверждение пароля',
            },
        },
        signIn: {
            title: 'Вход',
            cta: 'Войти',
            form: {
                email: 'Email',
                password: 'Пароль',
                signUp: 'Зарегистрироваться',
                forgot: 'Забыли пароль?',
            },
        },
        signedUp: {
            title: 'Вы успешно зарегистрировались!',
            subTitle: 'Для удобства работы с платформой предлагаем заполнить информацию, которая позволит' +
                ' наиболее эффективно пользоваться нашим функционалом.',
            cta: 'Далее',
        },
        bookmakersChoosing: {
            title: 'Настройка профиля',
            subTitle: 'Выберите БК, в которых Вы зарегистрированы',
            cta: 'Далее',
        },
        balancesSetting: {
            title: 'Настройка профиля',
            subTitle:
                'Пожалуйста, укажите состояние счета в выбранных БК. Это позволит вести аналитику по вашей' +
                'игровой деятельности. Вы сможете отслеживать какие ставки у Вас получаются лучше, сколько' +
                'Вы выиграли за период и т.д.',
            cta: 'Далее',
        },
        skillChoosing: {
            title: 'Настройка профиля',
            subTitle:
                'Укажите какого игрового сценария Вы планируете придерживаться? От этого зависит какие' +
                'рекомендации по ставкам вам будут предложены, а именно – суточное кол-во ставок в одну БК и' +
                'максимум, который можно ставить в рамках одной ставки каждой БК.',
            cta: 'Завершить',
        },
        listing: {
            coefficient: 'Коэффициент',
            from: 'от',
            to: 'до',
            popularity: 'Популярность',
            reliability: 'Надёжность',
            bookmakers: 'Букмекерские конторы',
            types: 'Типы ставок',
            tabs: {
                profitable: 'Доходный',
                reliable: 'Надёжный',
                optimistic: 'Оптимистичный',
                popular: 'Популярный',
            },
            titles: {
                event: 'Событие',
                bet: 'Ставка',
                popularity: 'Популярность',
                reliability: 'Надежность',
                coefficient: 'Коэффициент',
            },
        },
    },
    categories: {
        football: 'Футбол',
        hockey: 'Хоккей',
        volleyball: 'Воллейбол',
        basketball: 'Баскетбол',
        tennis: 'Теннис',
        gandball: 'Гадбол',
        fighting: 'Единоборства',
    },
    tariffs: {
        [TARIFF_FREE]: 'Бесплатный',
        [TARIFF_RATING_PREMATCH]: 'Рейтинг-Прематч',
        [TARIFF_RATING_LIVE]: 'Рейтинг-Лайв',
        [TARIFF_FORK_PREMATCH]: 'Вилка-Прематч',
        [TARIFF_FORK_LIVE]: 'Вилка-Лайв',
    },
    periods: {
        [PERIOD_MONTH]: '1 месяц',
        [PERIOD_HALF_YEAR]: '6 месяцев',
        [PERIOD_YEAR]: '1 год',
    },
    skills: {
        [SKILL_NOVICE]: 'Только начал разбираться',
        [SKILL_SMALL_KNOWLEDGE]: 'Уже знаю что и как',
        [SKILL_RISKY]: 'Готов рисковать',
        [SKILL_PRO]: 'Мне не нужны подсказки',
    },
    header: {
        menu: {
            functionality: 'Функционал',
            tariffs: 'Тарифы',
            blog: 'Блог',
            signUp: 'Регистрация',
        },
        primaryButton: 'Войти',
    },
    form: {
        select: {
            noData: 'Нет данных',
            placeholder: 'Выберите...',
        },
    },
    other: {
        currency: {
            rub: 'руб.',
        },
    },
    bet: {
        modes: {
            [LISTING_MODE_LIVE]: 'Лайв',
            [LISTING_MODE_PREMATCH]: 'Прематч',
        },
        types: {
            [LISTING_TYPE_RATING]: 'Рейтинг',
            [LISTING_TYPE_FORK]: 'Вилка',
        },
    },
}

export default createReducer({
    initialState,
})