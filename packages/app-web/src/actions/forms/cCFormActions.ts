import createAction from '@betpad/helpers/redux/createAction'
import { utc as moment } from 'moment'
import { Dispatch } from 'redux'
import { ACTION_CC_FORM_SET_FIELD } from '../../constants/actions/cCFormActionsConstants'
import { IStore } from '../../utilities/store'

export const setField = ({key, value}: {[key: string]: string}) => (dispatch: Dispatch, getState: () => IStore) => {
    if (key === 'year') {
        const {
            month,
        } = getState().user.model.cc.expiration

        const isCurrentYearSelected = +value === moment().year()
        const currentMonth = moment().month()

        if (isCurrentYearSelected && (currentMonth + 1 > (+month))) {
            dispatch(createAction(ACTION_CC_FORM_SET_FIELD)({key: 'month', value: ''}))
        }
    }

    dispatch(createAction(ACTION_CC_FORM_SET_FIELD)({key, value: String(value)}))
}