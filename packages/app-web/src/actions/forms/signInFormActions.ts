import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_SIGN_IN_FORM_SET_FIELD } from '../../constants/actions/signInFormActionsConstants'

export const setField = createAction(ACTION_SIGN_IN_FORM_SET_FIELD)