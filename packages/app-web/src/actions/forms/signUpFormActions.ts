import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_SIGN_UP_FORM_SET_FIELD } from '../../constants/actions/signUpFormActionsConstants'

export const setField = createAction(ACTION_SIGN_UP_FORM_SET_FIELD)