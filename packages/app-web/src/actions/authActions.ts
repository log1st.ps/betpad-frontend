import { IStore } from '../utilities/store'

export const openSocialAuthorization = (key: string) => (dispatch: any, getState: () => IStore) => {
    alert(`Открываем соц сеть "${key}" для авторизации`)
}