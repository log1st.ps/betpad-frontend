import createAction from '@betpad/helpers/redux/createAction'
import { Dispatch } from 'redux'
import { ACTION_COMMON_SET_HISTORY } from '../constants/actions/commonActionsConstants'
import { IStore } from '../utilities/store'

export const setHistory = createAction(ACTION_COMMON_SET_HISTORY)

export const navigate = (url: string) => (disptach: Dispatch, getState: () => IStore) => {
    const {
        common: {
            history,
        },
    } = getState()

    if (history) {
        history.push(url)
    }
}