import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_TARIFFS_PAGE_SUBMIT } from '../../constants/actions/tariffsPageActionsConstants'
import { IStore } from '../../utilities/store'
import { navigate } from '../commonActions'

export const submitSubscription = (tariff: string, period: string) => (dispatch: any, getState: () => IStore) => {
    dispatch(createAction(ACTION_TARIFFS_PAGE_SUBMIT)({
        tariff,
        period,
    }))

    dispatch(navigate('/cc'))
}
