import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_BALANCES_SETTING_PAGE_SUBMIT } from '../../constants/actions/balancesSettingPageActionsConstants'
import { navigate } from '../commonActions'

export const submitBalances = (balances: {
    [key: string]: string,
}) => (dispatch: any) => {

    dispatch(createAction(ACTION_BALANCES_SETTING_PAGE_SUBMIT)(balances))

    dispatch(navigate('/skillChoosing'))
}
