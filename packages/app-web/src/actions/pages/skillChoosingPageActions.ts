import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_SKILL_CHOOSING_PAGE_SUBMIT } from '../../constants/actions/skillChoosingPageActionsConstants'

export const submitSkill = (skill: string) => (dispatch: any) => {

    alert(
        `Введено: ${skill}
    `)

    dispatch(createAction(ACTION_SKILL_CHOOSING_PAGE_SUBMIT)(skill))
}
