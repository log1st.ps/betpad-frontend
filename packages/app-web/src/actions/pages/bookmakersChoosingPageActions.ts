import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_BOOKMAKERS_CHOOSING_PAGE_SUBMIT } from '../../constants/actions/bookmakersChoosingPageActionsConstants'
import { navigate } from '../commonActions'

export const submitBookmakers = (bookmakers: string[]) => (dispatch: any) => {

    dispatch(createAction(ACTION_BOOKMAKERS_CHOOSING_PAGE_SUBMIT)(bookmakers))

    dispatch(navigate('/balancesSetting'))
}
