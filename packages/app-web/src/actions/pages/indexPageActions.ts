import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_INDEX_PAGE_SUBMIT } from '../../constants/actions/indexPageActionsConstants'

export const submitCategories = (categories: string[]) => (dispatch: any) => {

    alert(`Выбрано: ${categories.join(', ')}`)

    dispatch(createAction(ACTION_INDEX_PAGE_SUBMIT)(categories))
}
