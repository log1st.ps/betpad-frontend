import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_SIGN_IN_PAGE_SUBMIT } from '../../constants/actions/signInPageActionsConstants'
import { SignInPageReducer } from '../../reducers/pages/signInPageReducer'
import { IStore } from '../../utilities/store'

export const submitSignInForm = () => (
    dispatch: any,
    getState: () => IStore & {page: SignInPageReducer},
)   => {
    const {
        email,
        password,
    } = getState().page.model


    alert(`
    Авторизация по данным: 
    Логин: ${email} 
    Пароль: ${password}
    `)

    dispatch(createAction(ACTION_SIGN_IN_PAGE_SUBMIT)({email, password}))
}