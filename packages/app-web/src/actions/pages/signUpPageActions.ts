import createAction from '@betpad/helpers/redux/createAction'
import {
    ACTION_SIGN_UP_PAGE_SUBMIT,
    ACTION_SIGN_UP_PAGE_VALIDATION,
} from '../../constants/actions/signUpPageActionsConstants'
import { post } from '../../services/apiHelper'
import { IStore } from '../../utilities/store'
import { navigate } from '../commonActions'

export const submitSignUpForm = () => (dispatch: any, getState: () => IStore) => {
    const {
        user: {
            model: {
                tariff,
                period,
                info: {
                    firstName, lastName, email, password, passwordConfirmation,
                },
                cc,
            },
        },
    } = getState()

    dispatch(createAction(ACTION_SIGN_UP_PAGE_SUBMIT)(cc))

    post('/auth/sign-up', {
        tariff,
        period,
        firstName,
        lastName,
        email,
        password,
        passwordConfirmation,
    })
        .then(({content}) => {
            dispatch(setSignUpValidation({}))
            dispatch(navigate('/signedUp'))
        })
        .catch(({data: {message, content: {errors}}}) => {
            dispatch(setSignUpValidation(errors))
        })
}

export const setSignUpValidation = (errors: any) => (dispatch: any) => {
    dispatch(createAction(ACTION_SIGN_UP_PAGE_VALIDATION)(errors))
}