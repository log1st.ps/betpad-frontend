import createAction from '@betpad/helpers/redux/createAction'
import { ACTION_CC_PAGE_SUBMIT } from '../../constants/actions/cCPageActionsConstants'
import { IStore } from '../../utilities/store'
import { navigate } from '../commonActions'

export const submitCCForm = () => (dispatch: any, getState: () => IStore) => {
    const {
        user: {
            model: {
                info: {
                    firstName, lastName, email, password, passwordConfirmation,
                },
                cc,
            },
        },
    } = getState()

    dispatch(createAction(ACTION_CC_PAGE_SUBMIT)(cc))

    if (
        !firstName
        || !lastName
        || !email
        || !password
        || !passwordConfirmation
    ) {
        dispatch(navigate('/signUp'))
    }   else {
        dispatch(navigate('/signedUp'))
    }
}