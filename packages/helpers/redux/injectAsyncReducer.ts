import {
    combineReducers,
    Reducer,
} from 'redux'

export default (store: any, name: string, asyncReducer: Reducer) => {
    store.asyncReducers[name] = asyncReducer
    store.replaceReducer(combineReducers({
        ...store.reducers,
        ...store.asyncReducers,
    }))
}