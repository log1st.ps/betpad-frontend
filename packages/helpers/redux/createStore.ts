import {
    applyMiddleware,
    combineReducers,
    compose,
    createStore,
    Reducer,
} from 'redux'
import thunk from 'redux-thunk'

export default (
    initialState: object,
    reducers: {[key: string]: Reducer},
) => {
    const middlewares = [
        applyMiddleware(thunk),
    ]

    if (window.hasOwnProperty('__REDUX_DEVTOOLS_EXTENSION__')) {
        middlewares.push(
            // tslint:disable-next-line:no-string-literal
            (window['__REDUX_DEVTOOLS_EXTENSION__'])(),
        )
    }

    const createStoreWithMiddleware = compose(...middlewares)(createStore) as any
    const store
        = createStoreWithMiddleware(combineReducers(reducers as any), initialState as any) as any
    store.reducers = reducers
    store.asyncReducers = {}

    return store
}
