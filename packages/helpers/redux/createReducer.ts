export default ({
    initialState = {},
    actions = {},
}) =>
    (
        state = initialState,
        { type, payload }: any,
    ) => {
        if (!actions[type]) { return state }

        return actions[type](state, payload)
    }