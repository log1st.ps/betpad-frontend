export default (
    state: any,
    payload: any,
) => ({
    ...state,
    ...payload,
})