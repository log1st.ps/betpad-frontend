export default
    (name: string) => (state: any) => ({
    ...state,
    [name]: !state[name],
})