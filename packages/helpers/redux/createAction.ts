export default (
    type: string,
    fn = (e: any) => e,
) => (payload?: any) => ({
    type,
    payload: fn(payload),
})