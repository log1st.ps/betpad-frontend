export default (name: string) => (
    state: any,
    payload: any,
) => ({
    ...state,
    [name]: payload,
})