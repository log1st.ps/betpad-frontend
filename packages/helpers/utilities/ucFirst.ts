import { IUcFirst } from '@betpad/types/utilities/IUcFirst'

const ucFirst: IUcFirst =
    (str) => str.length ? (str[0].toUpperCase() + str.slice(1)) : ''

export default ucFirst