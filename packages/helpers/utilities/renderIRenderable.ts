import { createElement } from 'react'
import { IRenderable } from '../../types/core/IRenderable'

export default (render: IRenderable) =>
    typeof render === 'function' ? createElement(render as any) : render