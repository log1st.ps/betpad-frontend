import { IWithForwardedRef } from '@betpad/types/hocs/IWithForwardedRef'
import * as React from 'react'
import {
    createElement,
    forwardRef,
    RefObject,
} from 'react'

const withForwardedRef: IWithForwardedRef
    = (base: React.ReactType<any>) =>
    forwardRef((props: any, ref: RefObject<any>) => {
        return createElement(
            base,
            {
                ...props,
                forwardedRef: ref,
            },
        )
    })

export default withForwardedRef