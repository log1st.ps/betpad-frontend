import {
    IWithPopup,
    IWithPopupProps,
} from '@betpad/types/hocs/IWithPopup'
import * as React from 'react'
import {
    createRef,
    Fragment,
    RefObject,
} from 'react'
import { createPortal } from 'react-dom'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import withHandlers from 'recompose/withHandlers'
import withProps from 'recompose/withProps'
import withState from 'recompose/withState'
import renderIRenderable from '../utilities/renderIRenderable'

interface IConnectedWithPopup extends IWithPopupProps {
    into: Element,
    popupRef: RefObject<HTMLDivElement>,
    targetRef: RefObject<HTMLDivElement>,
    targetZIndex: number,
    popupZIndex: number,
    popper: {
        destroy(): void,
    },

    initPopper(): void,

    deInitPopper(): void,

    setPopper(popper: any): void,
}

const Popper = require('popper.js/dist/popper').default

const zStartIndexesMap = {}

const getZIndex = (zStartIndex: number) => {
    if (!zStartIndexesMap.hasOwnProperty(zStartIndex)) {
        zStartIndexesMap[zStartIndex] = zStartIndex
    }

    zStartIndexesMap[zStartIndex]++

    return zStartIndexesMap[zStartIndex]
}

const withPopup: IWithPopup = ({
    renderContent,
    into,
    position,
    align,
    zStartIndex,
    zPopupOffset,
    isFixed,
    onArrowPositionUpdate,
    offsetPrimary,
    offsetSecondary,
    onCreate,
    onUpdate,
}) => (WrappedComponent: any) => compose(
    withProps(() => ({
        into: into instanceof Element ? into : document.querySelector(String(into || 'body')) as Element,
        popupRef: createRef(),
        targetRef: createRef(),
    })),
    withState('popper', 'setPopper', undefined),
    withProps(() => {
        const targetZIndex = getZIndex(zStartIndex || 0)
        const popupZIndex = Math.max(0, targetZIndex + (zPopupOffset || 0) - 2)

        return {
            targetZIndex: isNaN(targetZIndex) ? 0 : targetZIndex,
            popupZIndex: isNaN(popupZIndex) ? 0 : popupZIndex,
        }
    }),
    withHandlers({
        deInitPopper: ({
            popper,
        }: IConnectedWithPopup) => () => {
            if (popper) {
                popper.destroy()
            }
        },
        initPopper: ({
            targetRef: { current: target },
            popupRef: { current: popup },
            targetZIndex,
            setPopper,
        }: IConnectedWithPopup) => () => {
            let popperPosition = position
            if (align) {
                popperPosition += `-${align}`
            }

            if (!target) {
                return
            }

            target.style.zIndex = String(+targetZIndex)

            let placement: string
            setPopper(
                new Popper(
                    target,
                    popup,
                    {
                        positionFixed: isFixed,
                        placement: popperPosition,
                        modifiers: {
                            offset: {
                                enabled: true,
                                offset: `${offsetSecondary},${offsetPrimary}`,
                            },
                        },
                        onUpdate: (data: any) => {
                            if (onUpdate) {
                                onUpdate(data)
                            }

                            const {
                                attributes: {
                                    'x-placement': newPlacement,
                                },
                            } = data as any

                            if (newPlacement !== placement) {
                                placement = newPlacement
                                if (onArrowPositionUpdate) {
                                    onArrowPositionUpdate(placement)
                                }
                            }
                        },
                        onCreate: (data: any) => {
                            if (onCreate) {
                                onCreate(data)
                            }

                            const {
                                attributes: {
                                    'x-placement': newPlacement,
                                },
                            } = data as any

                            placement = newPlacement
                            if (onArrowPositionUpdate) {
                                onArrowPositionUpdate(placement)
                            }
                        },
                    },
                ),
            )
        },
    }),
    lifecycle({
        componentWillUnmount() {
            const {
                deInitPopper,
            } = this.props as IConnectedWithPopup

            deInitPopper()
        },
        componentDidMount() {
            const {
                initPopper,
            } = this.props as IConnectedWithPopup

            setTimeout(
                () => {
                    initPopper()
                },
                300)
        },
    }),
)(({
       into: portalInto,
       popupRef,
       targetRef,
       popupZIndex,
       ...props
   }: IConnectedWithPopup) => {
    return (
        <Fragment>
            {
                createPortal(
                    <div
                        ref={popupRef}
                        style={{ zIndex: popupZIndex }}
                    >
                        {renderIRenderable(renderContent({ zIndex: popupZIndex, ...props }))}
                    </div>,
                    portalInto,
                )}
            <WrappedComponent {...props} popupRef={popupRef} ref={targetRef}/>
        </Fragment>
    )
})

export default withPopup