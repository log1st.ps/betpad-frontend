import { IWithEvents } from '@betpad/types/hocs/IWithEvents'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import withState from 'recompose/withState'
import ucFirst from '../utilities/ucFirst'

interface IConnectedWithEvetnts extends IWithEvents {
    currentEvents: object,
    currentTarget: EventTarget,
    setCurrentEvents(object: object): void,
    setCurrentTarget(target: EventTarget): void,
}

export default ({
    targetKey,
    target,
    events,
    handlers,
}: IWithEvents) => compose(
    withState('currentEvents', 'setCurrentEvents', {}),
    withState('currentTarget', 'setCurrentTarget', {}),
    lifecycle({
        componentDidMount() {
            const {
                currentEvents,
                setCurrentEvents,
                setCurrentTarget,
            } = this.props as IConnectedWithEvetnts
            let eventsObject = {...currentEvents}
            events.map((event: string) => {
                const currentTarget = target instanceof EventTarget ? target : document.querySelector(target)
                const currentTargetKey = ucFirst(targetKey || (typeof target === 'string' ? target : ''))
                const currentEvent = ucFirst(event)
                const currentEventKey = `on${currentTargetKey}${currentEvent}`

                if (!handlers) {
                    return
                }
                const computedHandlers = handlers(() => this.props)
                const onEvent = computedHandlers && computedHandlers[currentEventKey]

                if (onEvent && currentTarget) {
                    setCurrentTarget(currentTarget)
                    eventsObject = {
                        ...eventsObject,
                        [event]: {
                            ...eventsObject[event],
                            [currentEventKey]: onEvent,
                        },
                    }
                    setCurrentEvents(eventsObject)
                    currentTarget.addEventListener(event, onEvent)
                }
            })
        },
        componentWillUnmount() {
            const {
                currentEvents,
                currentTarget,
            } = this.props as IConnectedWithEvetnts

            if (currentTarget) {
                Object.keys(currentEvents).map(key => {
                    Object.keys(currentEvents[key]).map(subKey => {
                        currentTarget.removeEventListener(key, currentEvents[key][subKey])
                    })
                })
            }
        },
    }),
)