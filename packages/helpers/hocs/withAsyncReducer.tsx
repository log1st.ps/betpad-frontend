import branch from 'recompose/branch'
import compose from 'recompose/compose'
import lifecycle from 'recompose/lifecycle'
import renderNothing from 'recompose/renderNothing'
import withState from 'recompose/withState'
import { Reducer } from 'redux'
import injectAsyncReducer from '../redux/injectAsyncReducer'

export default (key: string, reducer: Reducer) => compose(
    withState('isInjected', 'setIsInjected', false),
    lifecycle({
        componentDidMount() {
            const {
                asyncReducers,
                reducers,
                replaceReducer,
                setIsInjected,
            } = this.props as any

            injectAsyncReducer(
                {
                    asyncReducers,
                    reducers,
                    replaceReducer,
                },
                key,
                reducer,
            )
            setIsInjected(true)
        },
    }),
    branch(({isInjected}: any) => !isInjected, renderNothing),
)